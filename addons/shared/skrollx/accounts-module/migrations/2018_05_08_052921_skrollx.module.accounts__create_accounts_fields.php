<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleAccountsCreateAccountsFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'name' => 'anomaly.field_type.text',
        'slug' => [
            'type' => 'anomaly.field_type.text',
            'config' => [
                'slugify' => 'name',
                'type' => '_'
            ],
        ],
        'seller_name' => [
            'type' => 'anomaly.field_type.text'
        ],
        'date' => [
            'type' => 'anomaly.field_type.datetime'
        ],
        'country' => [
            'type' => 'anomaly.field_type.country',
            'config' => [
                'mode'          => 'dropdown',
            ]
        ],
        'identification_type' => [
            'type' => 'anomaly.field_type.text'
        ],
        'identification_number' => [
            'type' => 'anomaly.field_type.text'
        ],
        'mobile_number' => [
            'type' => 'anomaly.field_type.text'
        ],
        'vehicle' => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Cars\CarsCarsEntryModel',
            ]
        ],
        'vehicle_make' => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Cars\CarsMakesEntryModel',
            ]
        ],
        'vehicle_model' => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Cars\CarsModelsEntryModel',
            ]
        ],
        'purchase_order' => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Accounts\AccountsPurchaseOrdersEntryModel',
            ]
        ],
        'vehicle_year' => [
            'type'   => 'anomaly.field_type.text'
        ],
        'chassis_number' => [
            'type' => 'anomaly.field_type.text'
        ],
        'vehicle_color' => [
            'type' => 'anomaly.field_type.text'
        ],
        'plate_number' => [
            'type' => 'anomaly.field_type.text'
        ],
        'sales_approved' => [
            'type' => 'anomaly.field_type.boolean',
        ],
        'company_approved' => [
            'type' => 'anomaly.field_type.boolean',
        ],
        'price'       => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'address'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'approval' => [
            'type' => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Accounts\AccountsApprovalsEntryModel',
            ]
        ],
        'purchase_amount' => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'bank_clearance_amount' => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'deductions' => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'payable_to_customer' => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'additions' => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'paid_to_customer' => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'balance' => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'bank_loan_deposit_receipt' => [
            'type' => 'anomaly.field_type.file',
            'config' => [
                'folders' => []
            ]
        ],
        'sales_amount' => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'vat_on_sales' => [
            'type' => 'anomaly.field_type.integer'
        ],
        'extra' => [
            'type' => 'anomaly.field_type.text'
        ],
        'total_sales_payable' => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'po_id' => [
            'type' => 'anomaly.field_type.text'
        ],
        'po_status' => [
            'type' => 'anomaly.field_type.text'
        ]
    ];

}
