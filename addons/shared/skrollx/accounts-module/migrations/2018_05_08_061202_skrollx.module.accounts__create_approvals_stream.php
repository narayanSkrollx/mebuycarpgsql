<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleAccountsCreateApprovalsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'approvals',
        'title_column' => 'seller_name'
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'vehicle' => [
            'required' => true,
        ],
        'chassis_number',
        'seller_name' => [
            'required' => true
        ],
        'price' => [
            'required' => true
        ],
        'country',
        'address',
        'mobile_number',
        'identification_type',
        'identification_number',
        'date',
        'sales_approved'
    ];

}
