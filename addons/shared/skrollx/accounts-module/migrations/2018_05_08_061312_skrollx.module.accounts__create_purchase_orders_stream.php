<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleAccountsCreatePurchaseOrdersStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'purchase_orders',
        'title_column' => 'po_id'
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'seller_name' => [
            'required' => true
        ],
        'po_id' => [
            'required' => true,
            'unique' => true
        ],
        'approval',
        'price',
        'date',
        'country',
        'identification_type',
        'identification_number',
        'mobile_number',
        'vehicle',
        'vehicle_make',
        'vehicle_model',
        'vehicle_year',
        'chassis_number',
        'vehicle_color',
        'plate_number',
        'company_approved',
        'po_status'
    ];

}
