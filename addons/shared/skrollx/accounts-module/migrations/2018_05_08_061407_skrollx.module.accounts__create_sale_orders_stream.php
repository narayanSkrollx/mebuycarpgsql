<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleAccountsCreateSaleOrdersStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'sale_orders',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
    ];

}
