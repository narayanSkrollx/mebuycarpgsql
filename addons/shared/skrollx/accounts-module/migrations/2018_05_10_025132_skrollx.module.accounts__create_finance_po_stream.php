<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleAccountsCreateFinancePoStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'finance_po',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'purchase_order' => [ 'required'=>true ],
        'purchase_amount' => [ 'required' => true ],
        'bank_clearance_amount',
        'deductions',
        'payable_to_customer',
        'additions',
        'paid_to_customer',
        'balance',
        'bank_loan_deposit_receipt'
    ];

}
