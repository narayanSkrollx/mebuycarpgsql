<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleAccountsCreateFinanceSoStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'finance_so',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'sales_amount',
        'vat_on_sales',
        'extra',
        'total_sales_payable'
    ];

}
