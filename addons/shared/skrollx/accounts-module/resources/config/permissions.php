<?php 
	
	return [
		'approvals' => [
			'manage',
			'read',
	        'write',
	        'delete',
		],
		'purchase_orders' => [
			'manage',
			'read',
	        'write',
	        'delete',
		],
		'finance_po' => [
			'manage',
			'read',
	        'write',
	        'delete',
		],
		'finance_so' => [
			'manage',
			'read',
	        'write',
	        'delete',
		],

	];