$(document).ready(function(){
	$('.controls .card-block').hide()
	$('input[name="purchase_amount"]').attr('min',0);
	$('input[name="balance"]').attr('disabled','disabled');
	var purchase_order = $('select[name=purchase_order]').val();

	$.ajax({
		url:"/ajax/po_detail",
		dataType: "JSON",
		data:{id:purchase_order,type:'po',_token:"{{ csrf_token() }}"},
		method: "POST",
		success:function(data){
			if(data){
				$('input[name="purchase_amount"]').val(data.price);
			}
		}

	});

	$('input[name="bank_clearance_amount"]').on('change',function(){
		var purchase_amount = $('input[name="purchase_amount"]').val();
		var bank_clearance_amount = $('input[name="bank_clearance_amount"]').val();

		var payable_amount = purchase_amount - bank_clearance_amount;

		$('input[name="payable_to_customer"]').val(payable_amount);

		updateBalance();
	});

	$('input[name="additions"]').on('change',function(){
		updateBalance();
	});

	$('input[name="paid_to_customer"]').on('change',function(){
		updateBalance();
	});

	function updateBalance(){
		var payable_amount = $('input[name="payable_to_customer"]').val();
		var additions = $('input[name="additions"]').val();
		var paid_to_customer = $('input[name="paid_to_customer"]').val();

		var payable_amount = payable_amount == '' ? 0 : payable_amount;
		var additions = additions == '' ? 0 : additions;
		var paid_to_customer = paid_to_customer == '' ? 0 : paid_to_customer;

		var balance = parseFloat(payable_amount) + parseFloat(additions) - parseFloat(paid_to_customer);

		$('input[name="balance"]').val(balance);

		if(balance == 0){
			$('.card-block .actions').find('button[name="action"]').show();
		}
	}

});