$(document).ready(function(){
	var approval = $('select[name=approval]').val();

	$.ajax({
		url:"/ajax/po_detail",
		dataType: "JSON",
		data:{id:approval,type:'approval',_token:"{{ csrf_token() }}"},
		method: "POST",
		success:function(data){
			if(data){
				$('input[name="price"]').val(data.price);
			}
		}

	});
});