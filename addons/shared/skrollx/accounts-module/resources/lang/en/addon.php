<?php

return [
    'title'       => 'Accounts',
    'name'        => 'Accounts Module',
    'description' => '',
    'section'	  => [
    	'approvals' => 'Approvals',
    	'purchase_orders' => 'Purchase orders',
    	'sale_orders' => 'Sales Orders',
    	'finance_po' => 'Finance PO',
    	'finance_so' => 'Finance SO',
    ],
];
