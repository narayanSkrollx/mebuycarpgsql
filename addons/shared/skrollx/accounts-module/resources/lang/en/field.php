<?php 
	return [
		'vehicle' => [
			'name' => 'Vehicle'
		],
		'seller_name' => [
			'name' => 'Seller name'
		],
		'mobile_number' => [
			'name' => 'Mobile number'
		],
		'identification_type' => [
			'name' => 'Identification type'
		],
		'identification_number' => [
			'name' => 'Identification number'
		],
		'date' => [
			'name' => 'Date'
		],
		'sales_approved' => [
			'name' => 'Sales approved'
		],
		'company_approved' => [
			'name' => 'Admin approved'
		],
		'price' => [
			'name' => 'Price'
		],
		'chassis_number' => [
			'name' => 'Chassis Number'
		],
		'country' => [
			'name' => 'Country'
		],
		'address' => [
			'name' => 'Address'
		],
		'approval' => [
			'name' => 'Approval'
		],
		'po_id' => [
			'name' => 'ID'
		],
		'vehicle_make' => [
			'name' => 'Vehicle Make'
		],
		'vehicle_model' => [
			'name' => 'Vehicle Model'
		],
		'vehicle_year' => [
			'name' => 'Vehicle year'
		],
		'vehicle_color' => [
			'name' => 'Vehicle color'
		],
		'plate_number' => [
			'name' => 'Plate number'
		],
		'purchase_order' => [
			'name' => 'Purchase Order'
		],
		'purchase_amount' => [
			'name' => 'Purchase Amount'
		],
		'bank_clearance_amount' => [
			'name' => 'Bank Clearance Amount'
		],
		'deductions' => [
			'name' => 'Deductions'
		],
		'payable_to_customer' => [
			'name' => 'Payable To Costomer'
		],
		'additions' => [
			'name' => 'Additions'
		],
		'paid_to_customer' => [
			'name' => 'Paid To Costumer'
		],
		'balance' => [
			'name' => 'Balance'
		],
		'bank_loan_deposit_receipt' => [
			'name' => 'Bank Loan Deposit Receipt'
		],
		'sales_amount' => [
			'name' => 'Sales amount'
		],
		'vat_on_sales' => [
			'name' => 'Vat on sales'
		],
		'extra' => [
			'name' => 'Extra'
		],
		'total_sales_payable' => [
			'name' => 'Total Payable'
		],
	];

?>