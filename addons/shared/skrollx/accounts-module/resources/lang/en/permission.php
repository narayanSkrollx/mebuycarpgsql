<?php 
	return [

		'approvals' => [
			'name' => 'Approvals',
			'option' => [
				'read' => 'Can read approvals',
				'manage' => 'Can manage approvals',
				'write' => 'Can create approvals',
				'delete' => 'Can delete approvals',
			]
		],
		'purchase_orders' => [
			'name' => 'Purchase Orders',
			'option' => [
				'read' => 'Can read purchase orders',
				'manage' => 'Can manage purchase orders',
				'write' => 'Can create purchase orders',
				'delete' => 'Can delete purchase orders',
			]
		],
		'finance_po' => [
			'name' => 'Finance PO',
			'option' => [
				'read' => 'Can read Finance PO',
				'manage' => 'Can manage Finance PO',
				'write' => 'Can create Finance PO',
				'delete' => 'Can delete Finance PO',
			]
		],
		'finance_so' => [
			'name' => 'Finance SO',
			'option' => [
				'read' => 'Can read Finance SO',
				'manage' => 'Can manage Finance SO',
				'write' => 'Can create Finance SO',
				'delete' => 'Can delete Finance SO',
			]
		],

	];
?>