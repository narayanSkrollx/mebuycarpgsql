<?php 
	return [
		'admin/accounts/approval/verify/{id}'           => 'Skrollx\AccountsModule\Http\Controller\Admin\ApprovalsController@approve',
		
		'admin/accounts/purchaseorders/verify/{id}'           => 'Skrollx\AccountsModule\Http\Controller\Admin\PurchaseOrdersController@approve',
		'admin/accounts/purchaseorders/docs/{id}'           => 'Skrollx\AccountsModule\Http\Controller\Admin\PurchaseOrdersController@viewDocs',
		'admin/accounts/purchaseorders/pay/{id}'           => 'Skrollx\AccountsModule\Http\Controller\Admin\PurchaseOrdersController@makePayment',

		'ajax/po_detail'	=>  'Skrollx\AccountsModule\Http\Controller\Admin\AjaxController@getPoDetail',
	];
?>