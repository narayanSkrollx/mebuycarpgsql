<?php namespace Skrollx\AccountsModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class AccountsModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-puzzle-piece';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'approvals' => [
            'buttons' => [
                'new_approval',
            ],
        ],
        'purchase_orders',
        'finance_po',
        'finance_so' => [
            'buttons' => [
                'new_finance_so',
            ],
        ],
    ];

}
