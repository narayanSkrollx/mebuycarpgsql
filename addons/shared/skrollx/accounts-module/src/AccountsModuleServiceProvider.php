<?php namespace Skrollx\AccountsModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Skrollx\AccountsModule\SaleOrder\Contract\SaleOrderRepositoryInterface;
use Skrollx\AccountsModule\SaleOrder\SaleOrderRepository;
use Anomaly\Streams\Platform\Model\Accounts\AccountsSaleOrdersEntryModel;
use Skrollx\AccountsModule\SaleOrder\SaleOrderModel;
use Skrollx\AccountsModule\PurchaseOrder\Contract\PurchaseOrderRepositoryInterface;
use Skrollx\AccountsModule\PurchaseOrder\PurchaseOrderRepository;
use Anomaly\Streams\Platform\Model\Accounts\AccountsPurchaseOrdersEntryModel;
use Skrollx\AccountsModule\PurchaseOrder\PurchaseOrderModel;
use Skrollx\AccountsModule\Approval\Contract\ApprovalRepositoryInterface;
use Skrollx\AccountsModule\Approval\ApprovalRepository;
use Anomaly\Streams\Platform\Model\Accounts\AccountsApprovalsEntryModel;
use Skrollx\AccountsModule\Approval\ApprovalModel;
use Skrollx\AccountsModule\FinancePo\FinancePoObserver;
use Skrollx\AccountsModule\FinancePo\FinancePoModel;
use Illuminate\Routing\Router;

class AccountsModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/accounts/finance_so'           => 'Skrollx\AccountsModule\Http\Controller\Admin\FinanceSoController@index',
        'admin/accounts/finance_so/create'    => 'Skrollx\AccountsModule\Http\Controller\Admin\FinanceSoController@create',
        'admin/accounts/finance_so/edit/{id}' => 'Skrollx\AccountsModule\Http\Controller\Admin\FinanceSoController@edit',
        'admin/accounts/finance_po'           => 'Skrollx\AccountsModule\Http\Controller\Admin\FinancePoController@index',
        'admin/accounts/finance_po/create'    => 'Skrollx\AccountsModule\Http\Controller\Admin\FinancePoController@create',
        'admin/accounts/finance_po/edit/{id}' => 'Skrollx\AccountsModule\Http\Controller\Admin\FinancePoController@edit',
        'admin/accounts/sale_orders'           => 'Skrollx\AccountsModule\Http\Controller\Admin\SaleOrdersController@index',
        'admin/accounts/sale_orders/create'    => 'Skrollx\AccountsModule\Http\Controller\Admin\SaleOrdersController@create',
        'admin/accounts/sale_orders/edit/{id}' => 'Skrollx\AccountsModule\Http\Controller\Admin\SaleOrdersController@edit',
        'admin/accounts/purchase_orders'           => 'Skrollx\AccountsModule\Http\Controller\Admin\PurchaseOrdersController@index',
        'admin/accounts/purchase_orders/create'    => 'Skrollx\AccountsModule\Http\Controller\Admin\PurchaseOrdersController@create',
        'admin/accounts/purchase_orders/edit/{id}' => 'Skrollx\AccountsModule\Http\Controller\Admin\PurchaseOrdersController@edit',
        'admin/accounts'           => 'Skrollx\AccountsModule\Http\Controller\Admin\ApprovalsController@index',
        'admin/accounts/create'    => 'Skrollx\AccountsModule\Http\Controller\Admin\ApprovalsController@create',
        'admin/accounts/edit/{id}' => 'Skrollx\AccountsModule\Http\Controller\Admin\ApprovalsController@edit',
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Skrollx\AccountsModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * The addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Skrollx\AccountsModule\Event\ExampleEvent::class => [
        //    Skrollx\AccountsModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Skrollx\AccountsModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        AccountsSaleOrdersEntryModel::class => SaleOrderModel::class,
        AccountsPurchaseOrdersEntryModel::class => PurchaseOrderModel::class,
        AccountsApprovalsEntryModel::class => ApprovalModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        SaleOrderRepositoryInterface::class => SaleOrderRepository::class,
        PurchaseOrderRepositoryInterface::class => PurchaseOrderRepository::class,
        ApprovalRepositoryInterface::class => ApprovalRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        FinancePoModel::observe(FinancePoObserver::class);
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }

}
