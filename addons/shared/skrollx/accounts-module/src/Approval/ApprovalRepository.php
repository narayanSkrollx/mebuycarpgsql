<?php namespace Skrollx\AccountsModule\Approval;

use Skrollx\AccountsModule\Approval\Contract\ApprovalRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class ApprovalRepository extends EntryRepository implements ApprovalRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ApprovalModel
     */
    protected $model;

    /**
     * Create a new ApprovalRepository instance.
     *
     * @param ApprovalModel $model
     */
    public function __construct(ApprovalModel $model)
    {
        $this->model = $model;
    }
}
