<?php namespace Skrollx\AccountsModule\Approval\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ApprovalRepositoryInterface extends EntryRepositoryInterface
{

}
