<?php namespace Skrollx\AccountsModule\Approval\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class ApprovalTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        'vehicle',
        'seller_name',
        'sales_approved'
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'vehicle',
        'seller_name',
        'entry.sales_approved.label'
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit',
        'view',
        'verify' => [
            'icon' => 'fa fa-check',
            'type' => 'success',
            // 'disabled' => 'entry.sales_approved',
            'href' => 'admin/accounts/approval/verify/{entry.id}'
        ]
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}
