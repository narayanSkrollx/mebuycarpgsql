<?php namespace Skrollx\AccountsModule\FinancePo\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface FinancePoRepositoryInterface extends EntryRepositoryInterface
{

}
