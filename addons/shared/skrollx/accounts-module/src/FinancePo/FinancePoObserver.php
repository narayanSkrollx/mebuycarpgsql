<?php namespace Skrollx\AccountsModule\FinancePo;

use Anomaly\Streams\Platform\Entry\EntryObserver;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Skrollx\AccountsModule\PurchaseOrder\PurchaseOrderModel;

class FinancePoObserver extends EntryObserver
{
	public function created(EntryInterface $entry)
	{  
		PurchaseOrderModel::where('id',$entry->purchase_order_id)->update(['po_status'=>'closed']);
	}
}