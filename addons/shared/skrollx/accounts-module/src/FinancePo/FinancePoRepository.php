<?php namespace Skrollx\AccountsModule\FinancePo;

use Skrollx\AccountsModule\FinancePo\Contract\FinancePoRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class FinancePoRepository extends EntryRepository implements FinancePoRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var FinancePoModel
     */
    protected $model;

    /**
     * Create a new FinancePoRepository instance.
     *
     * @param FinancePoModel $model
     */
    public function __construct(FinancePoModel $model)
    {
        $this->model = $model;
    }
}
