<?php namespace Skrollx\AccountsModule\FinanceSo\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface FinanceSoRepositoryInterface extends EntryRepositoryInterface
{

}
