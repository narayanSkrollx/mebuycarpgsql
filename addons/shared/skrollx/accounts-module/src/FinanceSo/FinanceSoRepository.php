<?php namespace Skrollx\AccountsModule\FinanceSo;

use Skrollx\AccountsModule\FinanceSo\Contract\FinanceSoRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class FinanceSoRepository extends EntryRepository implements FinanceSoRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var FinanceSoModel
     */
    protected $model;

    /**
     * Create a new FinanceSoRepository instance.
     *
     * @param FinanceSoModel $model
     */
    public function __construct(FinanceSoModel $model)
    {
        $this->model = $model;
    }
}
