<?php namespace Skrollx\AccountsModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Skrollx\AccountsModule\PurchaseOrder\PurchaseOrderModel;
use Skrollx\AccountsModule\Approval\ApprovalModel;

class AjaxController
{
    public function getPoDetail()
    {
        $id = \Request::input('id');

    	if(\Request::input('type') == 'approval'){
    		$approval = ApprovalModel::find($id);
        	echo json_encode($approval);
    	}

    	if(\Request::input('type') == 'po'){
        	$po = PurchaseOrderModel::find($id);
        	$po->price = $po->approval->price;
        	echo json_encode($po);
    	}

    }
}
