<?php namespace Skrollx\AccountsModule\Http\Controller\Admin;

use Skrollx\AccountsModule\Approval\Form\ApprovalFormBuilder;
use Skrollx\AccountsModule\Approval\Table\ApprovalTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Skrollx\AccountsModule\Approval\ApprovalModel;
use Skrollx\DealersModule\File\FileUploader;
use Anomaly\FilesModule\Folder\Contract\FolderRepositoryInterface;
use Skrollx\AccountsModule\PurchaseOrder\PurchaseOrderModel;
use PDF;
use Auth;

class ApprovalsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ApprovalTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ApprovalTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ApprovalFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ApprovalFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ApprovalFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ApprovalFormBuilder $form, $id)
    {
        return $form->render($id);
    }

    public function approve($id,FileUploader $uploader, FolderRepositoryInterface $folders)
    {
        if(Auth::user()->hasRole('sales') == false){
            return redirect()->back()->with('warning',['Permission denied']);
        }

        ApprovalModel::where('id',$id)->update(['sales_approved' => 1]);

        $max_po = PurchaseOrderModel::max('id');

        $po = 'PO_';

        $po_id = 'PO_001';

        if($max_po == null or !$max_po){
            $po_id = $po.'001';
        }else{
            $po_id = $po.'00'.$max_po;
        }

        $po_exist = true;

        do {
            
            $existing_po = PurchaseOrderModel::where('po_id',$po_id)->first();

            if(!$existing_po){
                $po_exist = false;
            }else{
                $po_id = $po.'00'.($max_po+1);                
            }

        } while ( $po_exist == true);


        $approval = ApprovalModel::find($id);

        $po = [];

        $po['date'] = date('Y-m-d');
        $po['approval_id'] = $id;
        $po['po_id'] = $po_id;
        $po['seller_name'] = $approval->seller_name;
        $po['country'] = $approval->country;
        // $po['address'] = $approval->address;
        $po['identification_type'] = $approval->identification_type;
        $po['identification_number'] = $approval->identification_number;
        $po['mobile_number'] = $approval->mobile_number;
        $po['vehicle_make'] = $approval->vehicle->make->id;
        $po['vehicle_model'] = $approval->vehicle->model->id;
        $po['vehicle_year'] = $approval->vehicle->year;
        $po['chassis_number'] = $approval->chassis_number;
        $po['vehicle_color'] = $approval->vehicle->vehicle_color;
        $po['plate_number'] = $approval->vehicle->car_number;
        $po['company_approved'] = false;

        $new_po = new PurchaseOrderModel($po);

        $new_po->save();

        return redirect()->back()->with('success',['Approved successfully!!']);
        
    }
}