<?php namespace Skrollx\AccountsModule\Http\Controller\Admin;

use Skrollx\AccountsModule\FinancePo\Form\FinancePoFormBuilder;
use Skrollx\AccountsModule\FinancePo\Table\FinancePoTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class FinancePoController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param FinancePoTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(FinancePoTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param FinancePoFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(FinancePoFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param FinancePoFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(FinancePoFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
