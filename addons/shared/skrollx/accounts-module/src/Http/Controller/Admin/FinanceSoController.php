<?php namespace Skrollx\AccountsModule\Http\Controller\Admin;

use Skrollx\AccountsModule\FinanceSo\Form\FinanceSoFormBuilder;
use Skrollx\AccountsModule\FinanceSo\Table\FinanceSoTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class FinanceSoController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param FinanceSoTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(FinanceSoTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param FinanceSoFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(FinanceSoFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param FinanceSoFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(FinanceSoFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
