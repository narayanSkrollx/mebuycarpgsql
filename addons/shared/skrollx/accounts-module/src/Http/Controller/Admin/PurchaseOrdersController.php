<?php namespace Skrollx\AccountsModule\Http\Controller\Admin;

use Skrollx\AccountsModule\PurchaseOrder\Form\PurchaseOrderFormBuilder;
use Skrollx\AccountsModule\PurchaseOrder\Table\PurchaseOrderTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Skrollx\AccountsModule\PurchaseOrder\PurchaseOrderModel;
use Skrollx\CarsModule\VehicleDetail\VehicleDetailModel;
use Skrollx\CarsModule\VehicleSpec\VehicleSpecModel;
use Skrollx\CarsModule\VehicleGeneral\VehicleGeneralModel;
use Skrollx\CarsModule\VehicleBodyReport\VehicleBodyReportModel;
use Skrollx\CarsModule\EngineCondition\EngineConditionModel;
use Skrollx\CarsModule\EngineNoise\EngineNoiseModel;
use Skrollx\CarsModule\GearboxCondition\GearboxConditionModel;
use Skrollx\CarsModule\AcCondition\AcConditionModel;
use Skrollx\CarsModule\AccessoryPart\AccessoryPartModel;
use Skrollx\CarsModule\Fluid\FluidModel;
use Skrollx\CarsModule\TestdriveGeneral\TestdriveGeneralModel;
use Skrollx\CarsModule\ElectricalSystem\ElectricalSystemModel;
use Skrollx\CarsModule\TyresWheel\TyresWheelModel;
use Skrollx\CarsModule\Interior\InteriorModel;
use Skrollx\CarsModule\Brake\BrakeModel;
use Auth;

class PurchaseOrdersController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param PurchaseOrderTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PurchaseOrderTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param PurchaseOrderFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(PurchaseOrderFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param PurchaseOrderFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(PurchaseOrderFormBuilder $form, $id)
    {
        return $form->render($id);
    }

    public function approve($id)
    {
        if(Auth::user()->hasRole('admin')){
            PurchaseOrderModel::where('id',$id)->update(['company_approved'=>1]);
            return redirect()->back()->with('success',['PO verified']);
        }else{
            return redirect()->back()->with('warning',['Permission denied!!']);
        }
    }
    
    public function viewDocs($id)
    {
        $po = PurchaseOrderModel::find($id);
        
        $car = $po->approval->vehicle;

        $tabs = [];

        $VehicleDetail = VehicleDetailModel::where('car_id',$car->id)->first();
        
        if($VehicleDetail){
            $tabs[trans('module::site.VehicleDetail')] = [
                'id' => 'VehicleDetail',
                'data' => $VehicleDetail,
            ];
        }

        $VehicleSpec = VehicleSpecModel::where('car_id',$car->id)->first();
        
        if($VehicleSpec){
            $tabs[trans('module::site.VehicleSpec')] = [
                'data' => $VehicleSpec,
                'id' => 'VehicleSpec' 
            ];
        }

        $VehicleGeneral = VehicleGeneralModel::where('car_id',$car->id)->first();
        
        if($VehicleGeneral){
            $tabs[trans('module::site.VehicleGeneral')] = [
                'data' => $VehicleGeneral,
                'id' => 'VehicleGeneral' 
            ];
        }

        $VehicleBodyReport = VehicleBodyReportModel::where('car_id',$car->id)->first();
        
        if($VehicleBodyReport){
            $tabs[trans('module::site.VehicleBodyReport')] = [
                'data' => $VehicleBodyReport,
                'id' => 'VehicleBodyReport' 
            ];
        }

        $EngineCondition = EngineConditionModel::where('car_id',$car->id)->first();
        
        if($EngineCondition){
            $tabs[trans('module::site.EngineCondition')] = [
                'data' => $EngineCondition,
                'id' => 'EngineCondition' 
            ];
        }

        $EngineNoise = EngineNoiseModel::where('car_id',$car->id)->first();
        
        if($EngineNoise){
            $tabs[trans('module::site.EngineNoise')] = [
                'data' => $EngineNoise,
                'id' => 'EngineNoise' 
            ];
        }

        $GearboxCondition = GearboxConditionModel::where('car_id',$car->id)->first();
        
        if($GearboxCondition){
            $tabs[trans('module::site.GearboxCondition')] = [
                'data' => $GearboxCondition,
                'id' => 'GearboxCondition' 
            ];
        }

        $AcCondition = AcConditionModel::where('car_id',$car->id)->first();
        
        if($AcCondition){
            $tabs[trans('module::site.AcCondition')] = [
                'data' => $AcCondition,
                'id' => 'AcCondition' 
            ];
        }

        $AccessoryPart = AccessoryPartModel::where('car_id',$car->id)->first();
        
        if($AccessoryPart){
            $tabs[trans('module::site.AccessoryPart')] = [
                'data' => $AccessoryPart,
                'id' => 'AccessoryPart' 
            ];
        }

        $Fluid = FluidModel::where('car_id',$car->id)->first();
        
        if($Fluid){
            $tabs[trans('module::site.Fluid')] = [
                'data' => $Fluid,
                'id' => 'Fluid' 
            ];
        }

        $TestdriveGeneral = TestdriveGeneralModel::where('car_id',$car->id)->first();
        
        if($TestdriveGeneral){
            $tabs[trans('module::site.TestdriveGeneral')] = [
                'data' => $TestdriveGeneral,
                'id' => 'TestdriveGeneral' 
            ];
        }

        $ElectricalSystem = ElectricalSystemModel::where('car_id',$car->id)->first();
        
        if($ElectricalSystem){
            $tabs[trans('module::site.ElectricalSystem')] = [
                'data' => $ElectricalSystem,
                'id' => 'ElectricalSystem' 
            ];
        }

        $TyresWheel = TyresWheelModel::where('car_id',$car->id)->first();
        
        if($TyresWheel){
            $tabs[trans('module::site.TyresWheel')] = [
                'data' => $TyresWheel,
                'id' => 'TyresWheel' 
            ];
        }

        $Interior = InteriorModel::where('car_id',$car->id)->first();
        
        if($Interior){
            $tabs[trans('module::site.Interior')] = [
                'data' => $Interior,
                'id' => 'Interior' 
            ];
        }

        $Brake = BrakeModel::where('car_id',$car->id)->first();

        if($Brake){
            $tabs[trans('module::site.Brake')] = [
                'data' => $Brake,
                'id' => 'Brake' 
            ];
        }


        return view('skrollx.module.accounts::view_po',compact('car','VehicleDetail','VehicleSpec','VehicleGeneral','VehicleBodyReport','EngineCondition','EngineNoise','GearboxConition','AcCondition','AccessoryPart','Fluid','TestdriveGeneral','ElectricalSystem','TyresWheel','Interior','Brake','tabs'));
    }

    public function makePayment($id)
    {
        if(Auth::user()->hasRole('finance')){
            $price = PurchaseOrderModel::find($id)->approval->price;
            return redirect('admin/accounts/finance_po/create?purchase_order_id='.$id);
        }else{
            return redirect()->back()->with('warning',['Permission denied!!']);
        }
    }
}
