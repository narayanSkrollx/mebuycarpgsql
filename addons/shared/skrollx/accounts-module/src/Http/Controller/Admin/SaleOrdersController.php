<?php namespace Skrollx\AccountsModule\Http\Controller\Admin;

use Skrollx\AccountsModule\SaleOrder\Form\SaleOrderFormBuilder;
use Skrollx\AccountsModule\SaleOrder\Table\SaleOrderTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class SaleOrdersController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param SaleOrderTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(SaleOrderTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param SaleOrderFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(SaleOrderFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param SaleOrderFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(SaleOrderFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
