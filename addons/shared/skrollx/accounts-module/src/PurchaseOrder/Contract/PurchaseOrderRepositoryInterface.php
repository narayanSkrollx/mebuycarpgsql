<?php namespace Skrollx\AccountsModule\PurchaseOrder\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface PurchaseOrderRepositoryInterface extends EntryRepositoryInterface
{

}
