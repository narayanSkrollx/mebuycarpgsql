<?php namespace Skrollx\AccountsModule\PurchaseOrder;

use Skrollx\AccountsModule\PurchaseOrder\Contract\PurchaseOrderInterface;
use Anomaly\Streams\Platform\Model\Accounts\AccountsPurchaseOrdersEntryModel;

class PurchaseOrderModel extends AccountsPurchaseOrdersEntryModel implements PurchaseOrderInterface
{

}
