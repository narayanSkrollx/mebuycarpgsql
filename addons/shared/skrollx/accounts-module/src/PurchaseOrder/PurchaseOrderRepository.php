<?php namespace Skrollx\AccountsModule\PurchaseOrder;

use Skrollx\AccountsModule\PurchaseOrder\Contract\PurchaseOrderRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class PurchaseOrderRepository extends EntryRepository implements PurchaseOrderRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var PurchaseOrderModel
     */
    protected $model;

    /**
     * Create a new PurchaseOrderRepository instance.
     *
     * @param PurchaseOrderModel $model
     */
    public function __construct(PurchaseOrderModel $model)
    {
        $this->model = $model;
    }
}
