<?php namespace Skrollx\AccountsModule\PurchaseOrder\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;
use Auth;

class PurchaseOrderTableBuilder extends TableBuilder
{
    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        'approval'
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'po_id',
        'seller_name',
        'entry.company_approved.label'
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'verify' => [
            'icon' => 'fa fa-check',
            'type' => 'success',
            'href' => 'admin/accounts/purchaseorders/verify/{entry.id}'
        ],
        'view_docs' => [
            'icon' => 'fa fa-eye',
            'type' => 'primary',
            'href' => 'admin/accounts/purchaseorders/docs/{entry.id}'
        ],
        'make_payment' => [
            'icon' => 'fa fa-money',
            'type' => 'primary',
            'enabled' => 'entry.company_approved',
            'href' => 'admin/accounts/purchaseorders/pay/{entry.id}'
        ],
        'edit',
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}
