<?php namespace Skrollx\AccountsModule\SaleOrder\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface SaleOrderRepositoryInterface extends EntryRepositoryInterface
{

}
