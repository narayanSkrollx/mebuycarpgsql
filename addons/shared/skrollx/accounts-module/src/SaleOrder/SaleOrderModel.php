<?php namespace Skrollx\AccountsModule\SaleOrder;

use Skrollx\AccountsModule\SaleOrder\Contract\SaleOrderInterface;
use Anomaly\Streams\Platform\Model\Accounts\AccountsSaleOrdersEntryModel;

class SaleOrderModel extends AccountsSaleOrdersEntryModel implements SaleOrderInterface
{

}
