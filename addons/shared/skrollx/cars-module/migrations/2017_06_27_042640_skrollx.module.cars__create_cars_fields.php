<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateCarsFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'title'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'name'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'config_value'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'depreciation'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'slug'  => [
            'type' => 'anomaly.field_type.slug',
            'config' => [
                'type'    => '-',
                'slugify' => 'title'
            ]
        ],
        'car' => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Cars\CarsCarsEntryModel',
            ]
        ],
        'image'        => [
            'type' => 'anomaly.field_type.file',
            'config' => [
                'folders' => []
            ]
        ],
        'images'        => [
            'type' => 'anomaly.field_type.files',
            'config' => [
                'folders' => []
            ]
        ],
        'user' => [ 
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Users\UsersUsersEntryModel',
            ]
        ],
        'make' => [ 
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Cars\CarsMakesEntryModel',
            ]
        ],
        'model' => [ 
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'   => 'Anomaly\Streams\Platform\Model\Cars\CarsModelsEntryModel',
                'handler'   => 'Skrollx\CarsModule\Model\Handler\ModelFieldHandler@handle'
            ]
        ],
        'transmission' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.transmission.options.1',
                    '2'  => 'skrollx.module.cars::field.transmission.options.2',
                ],
                'max' => 1
            ]
        ],
        'specs' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.specs.options.1',
                    '2'  => 'skrollx.module.cars::field.specs.options.2',
                    '3'  => 'skrollx.module.cars::field.specs.options.3',
                ],
                'max' => 1
            ]
        ],
        'cylinders' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '2'  => '2',
                    '3'  => '3',
                    '4'  => '4',
                    '5'  => '5',
                    '6'  => '6',
                    '7'  => '7',
                    '8'  => '8',
                    '9'  => '9',
                    '10'  => '10',
                    '11'  => '11',
                    '12'  => '12',
                    '13'  => '13',
                    '14'  => '14',
                ],
                'max' => 2
            ]
        ],
        'odometer_reading'       => [
            'type' => 'anomaly.field_type.integer'
        ],
        'booking_count'       => [
            'type' => 'anomaly.field_type.integer'
        ],
        'option' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.option.options.1',
                    '2'  => 'skrollx.module.cars::field.option.options.2',
                    '3'  => 'skrollx.module.cars::field.option.options.3',
                    '4'  => 'skrollx.module.cars::field.option.options.4',
                ],
                'max' => 1
            ]
        ],
        'paint' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.paint.options.1',
                    '2'  => 'skrollx.module.cars::field.paint.options.2',
                    '3'  => 'skrollx.module.cars::field.paint.options.3',
                ],
                'max' => 1
            ]
        ],
        'history_report' => [
            'type' => 'anomaly.field_type.checkboxes',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.history_report.options.1',
                    '2'  => 'skrollx.module.cars::field.history_report.options.2',
                    '3'  => 'skrollx.module.cars::field.history_report.options.3',
                ],
                'mode' => 'checkboxes'
            ]
        ],
        'accident_history'       => [
            'type' => 'anomaly.field_type.boolean'
        ],
        'service_history' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.service_history.options.1',
                    '2'  => 'skrollx.module.cars::field.service_history.options.2',
                    '3'  => 'skrollx.module.cars::field.service_history.options.3',
                ],
                'max' => 1
            ]
        ],
        'body_type' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.body_type.options.1',
                    '2'  => 'skrollx.module.cars::field.body_type.options.2',
                    '3'  => 'skrollx.module.cars::field.body_type.options.3',
                    '4'  => 'skrollx.module.cars::field.body_type.options.4',
                    '5'  => 'skrollx.module.cars::field.body_type.options.5',
                    '6'  => 'skrollx.module.cars::field.body_type.options.6',
                    '7'  => 'skrollx.module.cars::field.body_type.options.7',
                    '8'  => 'skrollx.module.cars::field.body_type.options.8',
                    '9'  => 'skrollx.module.cars::field.body_type.options.9',
                    '10'  => 'skrollx.module.cars::field.body_type.options.10',
                    '11'  => 'skrollx.module.cars::field.body_type.options.11',
                    '12'  => 'skrollx.module.cars::field.body_type.options.12',
                ],
                'max' => 1
            ]
        ],
        'drive' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.drive.options.1',
                    '2'  => 'skrollx.module.cars::field.drive.options.2',
                    '3'  => 'skrollx.module.cars::field.drive.options.3',
                ],
                'max' => 1
            ]
        ],
        'color' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.color.options.1',
                    '2'  => 'skrollx.module.cars::field.color.options.2',
                    '3'  => 'skrollx.module.cars::field.color.options.3',
                    '4'  => 'skrollx.module.cars::field.color.options.4',
                    '5'  => 'skrollx.module.cars::field.color.options.5',
                    '6'  => 'skrollx.module.cars::field.color.options.6',
                    '7'  => 'skrollx.module.cars::field.color.options.7',
                    '8'  => 'skrollx.module.cars::field.color.options.8',
                    '9'  => 'skrollx.module.cars::field.color.options.9',
                ],
                'max' => 1
            ]
        ],
        'fuel_type' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.fuel_type.options.1',
                    '2'  => 'skrollx.module.cars::field.fuel_type.options.2',
                    '3'  => 'skrollx.module.cars::field.fuel_type.options.3',
                    '4'  => 'skrollx.module.cars::field.fuel_type.options.4',
                    '5'  => 'skrollx.module.cars::field.fuel_type.options.5',
                ],
                'max' => 1
            ]
        ],
        'car_number'       => [
            'type' => 'anomaly.field_type.integer'
        ],
        'cylinder_number'       => [
            'type' => 'anomaly.field_type.integer'
        ],
        'engine_size'       => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'   => 'Anomaly\Streams\Platform\Model\Cars\CarsEngineSizesEntryModel',
                'handler'   => 'Skrollx\CarsModule\EngineSize\Handler\EngineSizeFieldHandler@handle',
            ]
        ],
        'engine_cylinder'       => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'   => 'Anomaly\Streams\Platform\Model\Cars\CarsEngineCylindersEntryModel',
            ]
        ],
        'engine_sizes'       => [
            'type'   => 'anomaly.field_type.multiple',
            'config' => [
                'related'   => 'Anomaly\Streams\Platform\Model\Cars\CarsEngineSizesEntryModel',
            ]
        ],
        'chassis_damage' => [
            'type' => 'anomaly.field_type.boolean'
        ],
        'chassis_repaired' => [
            'type' => 'anomaly.field_type.boolean'
        ],
        'registered_in' => [
            'type' => 'anomaly.field_type.country',
            'config' => [
                'top_options' => ['AE'],
                'mode' => 'dropdown'
            ],
        ],
        'bank_loan' => [
            'type' => 'anomaly.field_type.boolean'
        ],
        'navigation_system' => [
            'type' => 'anomaly.field_type.boolean'
        ],
        'no_keys' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => '1',
                    '2'  => '2',
                    '3'  => '3',
                    '4'  => '4',
                    '5'  => '5',
                ],
                'max' => 1
            ]
        ],
        'roof' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.roof.options.1',
                    '2'  => 'skrollx.module.cars::field.roof.options.2',
                    '3'  => 'skrollx.module.cars::field.roof.options.3',
                ],
                'max' => 1
            ]
        ],
        'rim_type' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.rim_type.options.1',
                    '2'  => 'skrollx.module.cars::field.rim_type.options.2',
                ],
                'max' => 1
            ]
        ],
        'rim_condition' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.rim_condition.options.1',
                    '2'  => 'skrollx.module.cars::field.rim_condition.options.2',
                    '3'  => 'skrollx.module.cars::field.rim_condition.options.3',
                    '4'  => 'skrollx.module.cars::field.rim_condition.options.4',
                ],
                'max' => 1
            ]
        ],
        'seat_color' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1' => 'skrollx.module.cars::field.seat_color.options.1',
                    '2' => 'skrollx.module.cars::field.seat_color.options.2',
                    '3' => 'skrollx.module.cars::field.seat_color.options.3',
                    '4' => 'skrollx.module.cars::field.seat_color.options.4',
                    '5' => 'skrollx.module.cars::field.seat_color.options.5',
                    '6' => 'skrollx.module.cars::field.seat_color.options.6',
                    '7' => 'skrollx.module.cars::field.seat_color.options.7',
                    '8' => 'skrollx.module.cars::field.seat_color.options.8',
                    '9' => 'skrollx.module.cars::field.seat_color.options.9',
                ],
                'max' => 1
            ]
        ],
        'description'       => [
            'type' => 'anomaly.field_type.textarea'
        ],
        'min_bid_amount'       => [
            'type' => 'anomaly.field_type.integer'
        ],
        'max_bid_amount'       => [
            'type' => 'anomaly.field_type.integer'
        ],
        'rejected_amount'       => [
            'type' => 'anomaly.field_type.integer'
        ],
        'expire_date' => [
            'type'   => 'anomaly.field_type.datetime',
            'config' => [
                'mode'          => 'datetime',
                'date_format'   => 'Y-m-d',
                'year_range'    => '0:+1',
                'time_format'   => 'g:i A',
                'timezone'      => null,
                'step'          => 15
            ]
        ],
        'mileage' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.mileage.options.1',
                    '2'  => 'skrollx.module.cars::field.mileage.options.2',
                    '3'  => 'skrollx.module.cars::field.mileage.options.3',
                    '4'  => 'skrollx.module.cars::field.mileage.options.4',
                    '5'  => 'skrollx.module.cars::field.mileage.options.5',
                    '6'  => 'skrollx.module.cars::field.mileage.options.6',
                    '7'  => 'skrollx.module.cars::field.mileage.options.7',
                    '8'  => 'skrollx.module.cars::field.mileage.options.8',
                    '9'  => 'skrollx.module.cars::field.mileage.options.9',
                    '10' => 'skrollx.module.cars::field.mileage.options.10',
                    '11' => 'skrollx.module.cars::field.mileage.options.11',
                    '12' => 'skrollx.module.cars::field.mileage.options.12',
                    '13' => 'skrollx.module.cars::field.mileage.options.13',
                    '14' => 'skrollx.module.cars::field.mileage.options.14',
                    '15' => 'skrollx.module.cars::field.mileage.options.15',
                    '16' => 'skrollx.module.cars::field.mileage.options.16',
                ],
                'max' => 2
            ]
        ],
        'year'       => [
            'type' => 'anomaly.field_type.integer'
        ],
        'year_from'       => [
            'type' => 'anomaly.field_type.integer'
        ],
        'year_to'       => [
            'type' => 'anomaly.field_type.integer'
        ],
        'evaluation_price'       => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'price'       => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'seller_price'       => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'live' => [
            'type' => 'anomaly.field_type.boolean'
        ],
        'email' => [
            'type' => 'anomaly.field_type.email'
        ],
        'location' => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Cars\CarsLocationsEntryModel',
            ]
        ],
        'first_name' => [
            'type' => 'anomaly.field_type.text'
        ],
        'last_name' => [
            'type' => 'anomaly.field_type.text'
        ],
        'date' => [
            'type'   => 'anomaly.field_type.datetime',
            'config' => [
                'mode'          => 'datetime',
                'date_format'   => 'Y-m-d',
                'time_format'   => 'g:i A',
                'year_range'    => '0:+1',
                'timezone'      => null,
                'step'          => 15
            ]
        ],
        'mobile' => [
            'type' => 'anomaly.field_type.text'
        ],
        'alt_contact' => [
            'type' => 'anomaly.field_type.text'
        ],
        'status' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.status.options.1',
                    '2'  => 'skrollx.module.cars::field.status.options.2',
                    '3'  => 'skrollx.module.cars::field.status.options.3',
                    '4'  => 'skrollx.module.cars::field.status.options.4',
                    '5'  => 'skrollx.module.cars::field.status.options.5',
                ],
                'max' => 1
            ]
        ],
        'checked_in' => [
            'type' => 'anomaly.field_type.boolean',
        ],
        'deal_status' => [
            'type' => 'anomaly.field_type.text',
        ],
        'country' => [
            'type' => 'anomaly.field_type.country',
            'config' => [
                'mode'          => 'dropdown',
            ]
        ],
        'city' => [ 
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Dealers\DealersCitiesEntryModel',
            ]
        ],
        'reserved'  => [
            'type'  => 'anomaly.field_type.boolean'
        ],



        /* ---------------------- later added ---------------------*/

        'plate_number' => [
            'type' => 'anomaly.field_type.text',
        ],
        'model_type' => [
            'type' => 'anomaly.field_type.text'
        ],
        'engine' => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'   => 'Anomaly\Streams\Platform\Model\Cars\CarsEngineSizesEntryModel'
            ]
        ],
        'body' => [
            'type' => 'anomaly.field_type.text'
        ],

        // Vehicle specifications

        'power_options' => [
            'type' => 'anomaly.field_type.boolean',
        ],
        'keys' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.keys.options.1',
                    '2'  => 'skrollx.module.cars::field.keys.options.2',
                ]
            ]
        ],
        'iterior' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.iterior.options.1',
                    '2'  => 'skrollx.module.cars::field.iterior.options.2',
                    '3'  => 'skrollx.module.cars::field.iterior.options.3',
                ]
            ]
        ],
        'sun_para_roof' => [
            'type' => 'anomaly.field_type.boolean'
        ],
        'wheel' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.wheel.options.1',
                    '2'  => 'skrollx.module.cars::field.wheel.options.2',
                    '3'  => 'skrollx.module.cars::field.wheel.options.3',
                ]
            ]
        ],
        'fog_lamp' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'zenon_lights' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'cruise_control' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'blue_tooth' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'cd_dvd_player' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'fog_lamp' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'rear_camera' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'air_bag' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'parking_sensor' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'differential_lock' => [
            'type' => 'anomaly.field_type.boolean' 
        ],

        // General

        'car_registered_in' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.car_registered_in.options.1',
                    '2'  => 'skrollx.module.cars::field.car_registered_in.options.2',
                    '3'  => 'skrollx.module.cars::field.car_registered_in.options.3',
                    '4'  => 'skrollx.module.cars::field.car_registered_in.options.4',
                    '5'  => 'skrollx.module.cars::field.car_registered_in.options.5',
                    '6'  => 'skrollx.module.cars::field.car_registered_in.options.6',
                    '7'  => 'skrollx.module.cars::field.car_registered_in.options.7',
                ]
            ]
        ],
        'dealers_warranty' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'dealers_extended_warranty' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'warranty_service_booklet' => [
            'type' => 'anomaly.field_type.boolean' 
        ],

        // Car body detailed check up reports

        'vehicle_history' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.vehicle_history.options.1',
                    '2'  => 'skrollx.module.cars::field.vehicle_history.options.2',
                    '3'  => 'skrollx.module.cars::field.vehicle_history.options.3',
                    '4'  => 'skrollx.module.cars::field.vehicle_history.options.4',
                ]
            ]
        ],
        'accident_reported' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'front_bumper' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],
        'front_bonnete' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],
        'front_left_fender' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],
        'front_left_door' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],
        'rear_bumper' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],
        'rear_left_door' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],
        'rear_left_fender' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],
        'front_right_fender' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],
        'front_right_door' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],
        'rear_right_door' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],
        'rear_right_fender' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],
        'rear_trunk' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],
        'running_board_right' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],
        'running_board_left' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.front_bumper.options.1',
                    '2'  => 'skrollx.module.cars::field.front_bumper.options.2',
                    '3'  => 'skrollx.module.cars::field.front_bumper.options.3',
                    '4'  => 'skrollx.module.cars::field.front_bumper.options.4',
                    '5'  => 'skrollx.module.cars::field.front_bumper.options.5',
                    '6'  => 'skrollx.module.cars::field.front_bumper.options.6',
                    '7'  => 'skrollx.module.cars::field.front_bumper.options.7',
                    '8'  => 'skrollx.module.cars::field.front_bumper.options.8',
                ]
            ]
        ],

        //Engine over all condition

        'head_gasket_leak' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'oil_sump_leak' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'value_cover_oil_leak' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'coolant_leak' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'warning_light_on' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'power_steering_oil_leak' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'engine_misfiring' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'need_sparkplug_replace' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'engine_lack_powers' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'engine_overheating' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'engine_vibration_in_acc' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'battery_weak' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.battery_weak.options.1',
                    '2'  => 'skrollx.module.cars::field.battery_weak.options.2',
                    '3'  => 'skrollx.module.cars::field.battery_weak.options.3',
                ]
            ]
        ],
        'engine_mounting' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.fluids_otions_1.options.1',
                    '2'  => 'skrollx.module.cars::field.fluids_otions_1.options.2',
                    '3'  => 'skrollx.module.cars::field.fluids_otions_1.options.3',
                ]
            ]
        ],
        'battery_dead' => [
            'type' => 'anomaly.field_type.boolean' 
        ],

        //Engine Noises

        'engine_valve_noise' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'tappet_noise' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'timebelt_noise' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'pulley_noise' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'engine_bearing_noise' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'exhaust_manifold_noise' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'engine_noise' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'drive_belt_noise' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.wheel_covers.options.1',
                    '2'  => 'skrollx.module.cars::field.wheel_covers.options.2',
                    '3'  => 'skrollx.module.cars::field.wheel_covers.options.3',
                ]
            ]
        ],
        'silencer_condition' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.wheel_covers.options.1',
                    '2'  => 'skrollx.module.cars::field.wheel_covers.options.2',
                    '3'  => 'skrollx.module.cars::field.wheel_covers.options.3',
                ]
            ]
        ],
        'silencer_smoke_condition' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.silencer_smoke_condition.options.1',
                    '2'  => 'skrollx.module.cars::field.silencer_smoke_condition.options.2',
                    '3'  => 'skrollx.module.cars::field.silencer_smoke_condition.options.3',
                    '4'  => 'skrollx.module.cars::field.silencer_smoke_condition.options.4',
                ]
            ]
        ],
        'engine_turbo_condition' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.engine_turbo_condition.options.1',
                    '2'  => 'skrollx.module.cars::field.engine_turbo_condition.options.2',
                    '3'  => 'skrollx.module.cars::field.engine_turbo_condition.options.3',
                ]
            ]
        ],

        //Gear box overall condition

        'delay_in_shift' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'transmission_whining_sound' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'shift_shock' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'gear_slipping' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'noise_differential_gear' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'transmission_oil_leak' => [
            'type' => 'anomaly.field_type.boolean' 
        ],
        'hard_clutch' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.hard_clutch.options.1',
                    '2'  => 'skrollx.module.cars::field.hard_clutch.options.2',
                    '3'  => 'skrollx.module.cars::field.hard_clutch.options.3',
                ]
            ]
        ],
        'gear_shifting' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.gear_shifting.options.1',
                    '2'  => 'skrollx.module.cars::field.gear_shifting.options.2',
                ]
            ]
        ],
        'transmission_mounting' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.fluids_otions_1.options.1',
                    '2'  => 'skrollx.module.cars::field.fluids_otions_1.options.2',
                    '3'  => 'skrollx.module.cars::field.fluids_otions_1.options.3',
                ]
            ]
        ],

        //AC Condition

        'air_condition_working' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.air_condition_working.options.1',
                    '2'  => 'skrollx.module.cars::field.air_condition_working.options.2',
                    '3'  => 'skrollx.module.cars::field.air_condition_working.options.3',
                ]
            ]
        ],
        'ac_compresor' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.ac_compresor.options.1',
                    '2'  => 'skrollx.module.cars::field.ac_compresor.options.2',
                    '3'  => 'skrollx.module.cars::field.ac_compresor.options.3',
                ]
            ]
        ],
        'heating_system' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.heating_system.options.1',
                    '2'  => 'skrollx.module.cars::field.heating_system.options.2',
                    '3'  => 'skrollx.module.cars::field.heating_system.options.3',
                ]
            ]
        ],

        //Test drive sensor camera glass and mirrors

        'right_mirror_camera' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.fluids_otions_1.options.1',
                    '2'  => 'skrollx.module.cars::field.fluids_otions_1.options.2',
                    '3'  => 'skrollx.module.cars::field.fluids_otions_1.options.3',
                ]
            ]
        ],
        'left_mirror_camera' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.fluids_otions_1.options.1',
                    '2'  => 'skrollx.module.cars::field.fluids_otions_1.options.2',
                    '3'  => 'skrollx.module.cars::field.fluids_otions_1.options.3',
                ]
            ]
        ],
        'front_parking_sensor' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.fluids_otions_1.options.1',
                    '2'  => 'skrollx.module.cars::field.fluids_otions_1.options.2',
                    '3'  => 'skrollx.module.cars::field.fluids_otions_1.options.3',
                ]
            ]
        ],
        'rear_parking_sensor' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.fluids_otions_1.options.1',
                    '2'  => 'skrollx.module.cars::field.fluids_otions_1.options.2',
                    '3'  => 'skrollx.module.cars::field.fluids_otions_1.options.3',
                ]
            ]
        ],
        'sun_para_roof_condition' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.light_options.options.1',
                    '2'  => 'skrollx.module.cars::field.light_options.options.2',
                    '3'  => 'skrollx.module.cars::field.light_options.options.3',
                ]
            ]
        ],
        'right_side_mirror' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.light_options.options.1',
                    '2'  => 'skrollx.module.cars::field.light_options.options.2',
                    '3'  => 'skrollx.module.cars::field.light_options.options.3',
                ]
            ]
        ],
        'left_side_mirror' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.light_options.options.1',
                    '2'  => 'skrollx.module.cars::field.light_options.options.2',
                    '3'  => 'skrollx.module.cars::field.light_options.options.3',
                ]
            ]
        ],
        'front_wind_screen' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.average_glassdamage.options.1',
                    '2'  => 'skrollx.module.cars::field.average_glassdamage.options.2',
                ]
            ]
        ],
        'rear_wind_screen' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.average_glassdamage.options.1',
                    '2'  => 'skrollx.module.cars::field.average_glassdamage.options.2',
                ]
            ]
        ],
        'wiper_blades' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.fluids_otions_1.options.1',
                    '2'  => 'skrollx.module.cars::field.fluids_otions_1.options.2',
                    '3'  => 'skrollx.module.cars::field.fluids_otions_1.options.3',
                ]
            ]
        ],

        //Fluids

        'engine_oil_filter' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.fluids_otions_1.options.1',
                    '2'  => 'skrollx.module.cars::field.fluids_otions_1.options.2',
                    '3'  => 'skrollx.module.cars::field.fluids_otions_1.options.3',
                ]
            ]
        ],        
        'break_fluid' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.fluids_otions_1.options.1',
                    '2'  => 'skrollx.module.cars::field.fluids_otions_1.options.2',
                    '3'  => 'skrollx.module.cars::field.fluids_otions_1.options.3',
                ]
            ]
        ],        
        'driver_axle_fluid' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.fluids_otions_1.options.1',
                    '2'  => 'skrollx.module.cars::field.fluids_otions_1.options.2',
                    '3'  => 'skrollx.module.cars::field.fluids_otions_1.options.3',
                ]
            ]
        ],        
        'collant' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.fluids_otions_1.options.1',
                    '2'  => 'skrollx.module.cars::field.fluids_otions_1.options.2',
                    '3'  => 'skrollx.module.cars::field.fluids_otions_1.options.3',
                ]
            ]
        ],        
        'transmission_fluid' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.fluids_otions_1.options.1',
                    '2'  => 'skrollx.module.cars::field.fluids_otions_1.options.2',
                    '3'  => 'skrollx.module.cars::field.fluids_otions_1.options.3',
                ]
            ]
        ],        
        'power_steering_fluid' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.fluids_otions_2.options.1',
                    '2'  => 'skrollx.module.cars::field.fluids_otions_2.options.2',
                    '3'  => 'skrollx.module.cars::field.fluids_otions_2.options.3',
                    '4'  => 'skrollx.module.cars::field.fluids_otions_2.options.4',
                ]
            ]
        ],        
        'clutch_fluid' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                   '1'  => 'skrollx.module.cars::field.fluids_otions_2.options.1',
                   '2'  => 'skrollx.module.cars::field.fluids_otions_2.options.2',
                   '3'  => 'skrollx.module.cars::field.fluids_otions_2.options.3',
                   '4'  => 'skrollx.module.cars::field.fluids_otions_2.options.4',
                ]
            ]
        ],

        //Test drive general

        'engine_startup' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.test_drive_options_1.options.1',
                    '2'  => 'skrollx.module.cars::field.test_drive_options_1.options.2',
                    '3'  => 'skrollx.module.cars::field.test_drive_options_1.options.3',
                ]
            ]
        ],               
        'engine_exhaust' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.test_drive_options_1.options.1',
                    '2'  => 'skrollx.module.cars::field.test_drive_options_1.options.2',
                    '3'  => 'skrollx.module.cars::field.test_drive_options_1.options.3',
                ]
            ]
        ],               
        'shift_shock' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.test_drive_options_1.options.1',
                    '2'  => 'skrollx.module.cars::field.test_drive_options_1.options.2',
                    '3'  => 'skrollx.module.cars::field.test_drive_options_1.options.3',
                ]
            ]
        ],               
        'shift_interlock' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.test_drive_options_1.options.1',
                    '2'  => 'skrollx.module.cars::field.test_drive_options_1.options.2',
                    '3'  => 'skrollx.module.cars::field.test_drive_options_1.options.3',
                ]
            ]
        ],               
        'steer_wheel' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.test_drive_options_1.options.1',
                    '2'  => 'skrollx.module.cars::field.test_drive_options_1.options.2',
                    '3'  => 'skrollx.module.cars::field.test_drive_options_1.options.3',
                ]
            ]
        ],               
        'suspension' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.test_drive_options_1.options.1',
                    '2'  => 'skrollx.module.cars::field.test_drive_options_1.options.2',
                    '3'  => 'skrollx.module.cars::field.test_drive_options_1.options.3',
                ]
            ]
        ],               
        'electric_seats' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.electric_seats.options.1',
                    '2'  => 'skrollx.module.cars::field.electric_seats.options.2',
                    '3'  => 'skrollx.module.cars::field.electric_seats.options.3',
                ]
            ]
        ],               
        'diff_lock' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.test_drive_options_2.options.1',
                    '2'  => 'skrollx.module.cars::field.test_drive_options_2.options.2',
                    '3'  => 'skrollx.module.cars::field.test_drive_options_2.options.3',
                    '4'  => 'skrollx.module.cars::field.test_drive_options_2.options.4',
                ]
            ]
        ],               
        'cruise_contorl_condition' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.test_drive_options_3.options.1',
                    '2'  => 'skrollx.module.cars::field.test_drive_options_3.options.2',
                    '3'  => 'skrollx.module.cars::field.test_drive_options_3.options.3',
                ]
            ]
        ],               
        'manual_gear' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.test_drive_options_2.options.1',
                    '2'  => 'skrollx.module.cars::field.test_drive_options_2.options.2',
                    '3'  => 'skrollx.module.cars::field.test_drive_options_2.options.3',
                    '4'  => 'skrollx.module.cars::field.test_drive_options_2.options.4',
                ]
            ]
        ],               
        'differentail_drive_axle' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.test_drive_options_2.options.1',
                    '2'  => 'skrollx.module.cars::field.test_drive_options_2.options.2',
                    '3'  => 'skrollx.module.cars::field.test_drive_options_2.options.3',
                    '4'  => 'skrollx.module.cars::field.test_drive_options_2.options.4',
                ]
            ]
        ],               
        'bonnete_shocks' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.test_drive_options_3.options.1',
                    '2'  => 'skrollx.module.cars::field.test_drive_options_3.options.2',
                    '3'  => 'skrollx.module.cars::field.test_drive_options_3.options.3',
                ]
            ]
        ],               
        'power_lift_gate_electrical' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.test_drive_options_3.options.1',
                    '2'  => 'skrollx.module.cars::field.test_drive_options_3.options.2',
                    '3'  => 'skrollx.module.cars::field.test_drive_options_3.options.3',
                ]
            ]
        ],

        //Electrical sytem
                       
        'front_head_lights' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.light_options.options.1',
                    '2'  => 'skrollx.module.cars::field.light_options.options.2',
                    '3'  => 'skrollx.module.cars::field.light_options.options.3',
                ]
            ]
        ],                       
        'tail_lamps' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.light_options.options.1',
                    '2'  => 'skrollx.module.cars::field.light_options.options.2',
                    '3'  => 'skrollx.module.cars::field.light_options.options.3',
                ]
            ]
        ],                       
        'indicators' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.light_options.options.1',
                    '2'  => 'skrollx.module.cars::field.light_options.options.2',
                    '3'  => 'skrollx.module.cars::field.light_options.options.3',
                ]
            ]
        ],                       
        'front_right_door_power_switches' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.power_switches.options.1',
                    '2'  => 'skrollx.module.cars::field.power_switches.options.2',
                ]
            ]
        ],                       
        'front_left_door_power_switches' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.power_switches.options.1',
                    '2'  => 'skrollx.module.cars::field.power_switches.options.2',
                ]
            ]
        ],                       
        'rear_right_door_power_switches' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.power_switches.options.1',
                    '2'  => 'skrollx.module.cars::field.power_switches.options.2',
                ]
            ]
        ],                       
        'rear_left_door_power_switches' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.power_switches.options.1',
                    '2'  => 'skrollx.module.cars::field.power_switches.options.2',
                ]
            ]
        ],                       
        'cd_radio_electricals' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.working_notworking.options.1',
                    '2'  => 'skrollx.module.cars::field.working_notworking.options.2'
                ]
            ]
        ],                       
        'door_centeral_looks_system' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.working_notworking.options.1',
                    '2'  => 'skrollx.module.cars::field.working_notworking.options.2'
                ]
            ]
        ],                       
        'arms_rest_console' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.average_damage.options.1',
                    '2'  => 'skrollx.module.cars::field.average_damage.options.2',
                ]
            ]
        ],                       
        'glove_box' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.average_damage.options.1',
                    '2'  => 'skrollx.module.cars::field.average_damage.options.2',
                ]
            ]
        ],
        'warning_signal_active' => [
            'type' => 'anomaly.field_type.boolean'
        ],
        'horn_working' => [
            'type' => 'anomaly.field_type.boolean'
        ],

        //Tyres and wheels
        
        'tyres_matches' => [
            'type' => 'anomaly.field_type.boolean'
        ],        
        'wheel_rim_matches' => [
            'type' => 'anomaly.field_type.boolean'
        ],        
        'spare_tyre_available' => [
            'type' => 'anomaly.field_type.boolean'
        ],        
        'jack_tool_available' => [
            'type' => 'anomaly.field_type.boolean'
        ],
        'front_right_tyre_condition' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.type_conditions.options.1',
                    '2'  => 'skrollx.module.cars::field.type_conditions.options.2',
                    '3'  => 'skrollx.module.cars::field.type_conditions.options.3',
                ]
            ]
        ],
        'front_left_tyre_condition' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.type_conditions.options.1',
                    '2'  => 'skrollx.module.cars::field.type_conditions.options.2',
                    '3'  => 'skrollx.module.cars::field.type_conditions.options.3',
                ]
            ]
        ],
        'rear_right_tyre_condition' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.type_conditions.options.1',
                    '2'  => 'skrollx.module.cars::field.type_conditions.options.2',
                    '3'  => 'skrollx.module.cars::field.type_conditions.options.3',
                ]
            ]
        ],
        'rear_left_tyre_condition' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.type_conditions.options.1',
                    '2'  => 'skrollx.module.cars::field.type_conditions.options.2',
                    '3'  => 'skrollx.module.cars::field.type_conditions.options.3',
                ]
            ]
        ],
        'wheel_covers' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.wheel_covers.options.1',
                    '2'  => 'skrollx.module.cars::field.wheel_covers.options.2',
                    '3'  => 'skrollx.module.cars::field.wheel_covers.options.3',
                ]
            ]
        ],
        'alloy_wheel_condition' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.alloy_wheel_condition.options.1',
                    '2'  => 'skrollx.module.cars::field.alloy_wheel_condition.options.2',
                    '3'  => 'skrollx.module.cars::field.alloy_wheel_condition.options.3',
                ]
            ]
        ],

        //Seats trims carpets and interior

        'seat_belts' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.good_damage.options.1',
                    '2'  => 'skrollx.module.cars::field.good_damage.options.2',
                ]
            ]
        ],        
        'folding_seat_system' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.good_damage.options.1',
                    '2'  => 'skrollx.module.cars::field.good_damage.options.2'
                ]
            ]
        ],        
        'cool_heat_seat_system' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.good_damage.options.1',
                    '2'  => 'skrollx.module.cars::field.good_damage.options.2'
                ]
            ]
        ],        
        'head_liner' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.good_damage.options.1',
                    '2'  => 'skrollx.module.cars::field.good_damage.options.2',
                ]
            ]
        ],        
        'door_trim_panels' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.good_damage.options.1',
                    '2'  => 'skrollx.module.cars::field.good_damage.options.2',
                ]
            ]
        ],        
        'floor_carpet' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.floor_carpet.options.1',
                    '2'  => 'skrollx.module.cars::field.floor_carpet.options.2',
                    '3'  => 'skrollx.module.cars::field.floor_carpet.options.3',
                ]
            ]
        ],

        //Brakes

        'brake_pad_shoe' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.brakes.options.1',
                    '2'  => 'skrollx.module.cars::field.brakes.options.2',
                    '3'  => 'skrollx.module.cars::field.brakes.options.3',
                ]
            ]
        ],        
        'master_cylender_booster' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.brakes.options.1',
                    '2'  => 'skrollx.module.cars::field.brakes.options.2',
                    '3'  => 'skrollx.module.cars::field.brakes.options.3',
                ]
            ]
        ],        
        'parking_brake' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.brakes.options.1',
                    '2'  => 'skrollx.module.cars::field.brakes.options.2',
                    '3'  => 'skrollx.module.cars::field.brakes.options.3',
                ]
            ]
        ],        
        'hand_foot_brake' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' =>[
                    '1'  => 'skrollx.module.cars::field.brakes.options.1',
                    '2'  => 'skrollx.module.cars::field.brakes.options.2',
                    '3'  => 'skrollx.module.cars::field.brakes.options.3',
                ]
            ]
        ],
    ];
}
