<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateModelsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'models',
        'title_column' => 'title'
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'title' => [
            'required' => true
        ],
        'make',
        'year_from',
        'year_to',
        'engine_sizes',
        'engine_cylinder',
        'live'
    ];

}
