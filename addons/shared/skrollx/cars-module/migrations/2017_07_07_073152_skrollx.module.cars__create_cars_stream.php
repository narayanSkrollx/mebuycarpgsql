<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateCarsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'cars',
        'title_column' => 'title',
        'translatable' => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'title' => [
            'required' => true,
            'translatable' => true
        ],
        'slug' => [
            'required' => true,
            'unique' => true
        ],
        'description' => [
            'translatable' => true
        ],
        'image',
        'images',
        'make',
        'model',
        'year',
        'transmission',
        'specs',
        'cylinders',
        'mileage',
        'odometer_reading',
        'option',
        'paint',
        'history_report',
        'accident_history',
        'service_history',
        'body_type',
        'drive',
        'color',
        'fuel_type',
        'car_number',
        'engine_size',
        'chassis_damage',
        'chassis_repaired',
        'registered_in',
        'bank_loan',
        'navigation_system',
        'no_keys',
        'roof',
        'rim_type',
        'rim_condition',
        'seat_color',
        'price',
        'evaluation_price',
        'seller_price',
        'min_bid_amount',
        'max_bid_amount',
        'rejected_amount',
        'expire_date',
        'status',
        'location',
        'first_name',
        'last_name',
        'date',
        'mobile',
        'alt_contact',
        'email',
        'checked_in',
        'deal_status'
    ];

}
