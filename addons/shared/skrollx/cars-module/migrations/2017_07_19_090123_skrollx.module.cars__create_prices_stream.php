<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreatePricesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'prices'
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'make' => [
            'required' => true
        ],
        'model' => [
            'required' => true,
        ],
        'year_from' => [
            'required' => true
        ],
        'year_to' => [
            'required' => true
        ],
        'price' => [
            'required' => true
        ],
        'engine_size',
        'depreciation',
        'option'
    ];

}
