<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateDatesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'dates',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'location' => ['required' => true],
        'date',
        'reserved',
        'booking_count'
    ];

}
