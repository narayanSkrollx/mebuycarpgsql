<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateVehicleDetailStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'vehicle_detail',
        'title_column' => 'plate_number',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car' => ['required' => true],
        'plate_number',
        'model_type',
        'engine',
        'body',
    ];

}
