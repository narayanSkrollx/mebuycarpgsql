<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateVehicleSpecsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'vehicle_specs',
         'title_column' => 'power_options',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car'  => ['required' => true],
        'power_options',
        'keys',
        'iterior',
        'sun_para_roof',
        'wheel',
        'fog_lamp',
        'zenon_lights',
        'cruise_control',
        'blue_tooth',
        'cd_dvd_player',
        'fog_lamp',
        'rear_camera',
        'air_bag',
        'parking_sensor',
        'differential_lock',
    ];

}
