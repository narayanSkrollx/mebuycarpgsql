<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateVehicleGeneralStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'vehicle_general',
         'title_column' => 'car_registered_in',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car'  => ['required' => true],
        'car_registered_in',
        'dealers_warranty',
        'dealers_extended_warranty',
        'warranty_service_booklet',
    ];

}
