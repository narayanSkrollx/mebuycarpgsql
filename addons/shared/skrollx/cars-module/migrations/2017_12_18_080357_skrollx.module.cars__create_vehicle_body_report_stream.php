<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateVehicleBodyReportStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'vehicle_body_report',
         'title_column' => 'vehicle_history',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car'  => ['required' => true],
        'vehicle_history',
        'accident_reported',
        'front_bumper',
        'front_bonnete',
        'front_left_fender',
        'front_left_door',
        'rear_bumper',
        'rear_left_door',
        'rear_left_fender',
        'front_right_fender',
        'front_right_door',
        'rear_right_door',
        'rear_right_fender',
        'rear_trunk',
        'running_board_right',
        'running_board_left',
    ];

}
