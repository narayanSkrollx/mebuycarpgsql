<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateEngineConditionStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'engine_condition',
         'title_column' => 'head_gasket_leak',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car'  => ['required' => true],
        'head_gasket_leak',
        'oil_sump_leak',
        'value_cover_oil_leak',
        'coolant_leak',
        'warning_light_on',
        'power_steering_oil_leak',
        'engine_misfiring',
        'need_sparkplug_replace',
        'engine_lack_powers',
        'engine_overheating',
        'engine_vibration_in_acc',
        'battery_weak',
        'engine_mounting',
        'battery_dead',
    ];

}
