<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateEngineNoiseStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'engine_noise',
         'title_column' => 'engine_valve_noise',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car'  => ['required' => true],
        'engine_valve_noise',
        'tappet_noise',
        'timebelt_noise',
        'pulley_noise', 
        'engine_bearing_noise',
        'exhaust_manifold_noise',
        'engine_noise',
        'drive_belt_noise',
        'silencer_condition', 
        'silencer_smoke_condition', 
        'engine_turbo_condition',
    ];

}
