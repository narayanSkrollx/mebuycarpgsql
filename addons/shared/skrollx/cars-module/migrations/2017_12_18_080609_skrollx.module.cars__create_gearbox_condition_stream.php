<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateGearboxConditionStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'gearbox_condition',
         'title_column' => 'delay_in_shift',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car'  => ['required' => true],
        'delay_in_shift', 
        'transmission_whining_sound', 
        'shift_shock',
        'gear_slipping',
        'noise_differential_gear',
        'transmission_oil_leak',
        'hard_clutch',
        'gear_shifting',
        'transmission_mounting',
    ];

}
