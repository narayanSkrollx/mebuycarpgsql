<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateFluidsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'fluids',
         'title_column' => 'engine_oil_filter',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car'  => ['required' => true],
        'engine_oil_filter',
        'break_fluid',
        'driver_axle_fluid',
        'collant',
        'transmission_fluid',
        'power_steering_fluid',
        'clutch_fluid',
    ];

}
