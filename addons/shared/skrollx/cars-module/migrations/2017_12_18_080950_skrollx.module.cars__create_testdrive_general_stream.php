<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateTestdriveGeneralStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'testdrive_general',
         'title_column' => 'engine_startup',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car'  => ['required' => true],
        'engine_startup',
        'engine_exhaust',
        'shift_shock',
        'shift_interlock',
        'steer_wheel',
        'suspension',
        'electric_seats',
        'diff_lock',
        'cruise_contorl_condition',
        'manual_gear',
        'differentail_drive_axle',
        'bonnete_shocks',
        'power_lift_gate_electrical',
    ];

}
