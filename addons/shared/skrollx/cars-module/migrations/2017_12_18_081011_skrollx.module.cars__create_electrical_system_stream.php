<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateElectricalSystemStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'electrical_system',
         'title_column' => 'front_head_lights',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car'  => ['required' => true],
        'front_head_lights',
        'tail_lamps',
        'indicators',
        'front_right_door_power_switches',
        'front_left_door_power_switches',
        'rear_right_door_power_switches',
        'rear_left_door_power_switches',
        'cd_radio_electricals',
        'door_centeral_looks_system',
        'arms_rest_console',
        'glove_box',
        'warning_signal_active',
        'horn_working',
    ];

}
