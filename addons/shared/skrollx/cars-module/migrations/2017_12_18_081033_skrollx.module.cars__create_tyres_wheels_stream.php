<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateTyresWheelsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'tyres_wheels',
         'title_column' => 'tyres_matches',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car'  => ['required' => true],
        'tyres_matches',
        'wheel_rim_matches',
        'spare_tyre_available',
        'jack_tool_available',
        'front_right_tyre_condition',
        'front_left_tyre_condition',
        'rear_right_tyre_condition',
        'rear_left_tyre_condition',
        'wheel_covers',
        'alloy_wheel_condition',
    ];

}
