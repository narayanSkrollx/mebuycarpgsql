<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateBrakesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'brakes',
         'title_column' => 'brake_pad_shoe',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car'  => ['required' => true],
        'brake_pad_shoe',
        'master_cylender_booster',
        'parking_brake',
        'hand_foot_brake',
    ];

}
