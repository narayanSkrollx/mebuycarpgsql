<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateInteriorsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'interiors',
         'title_column' => 'seat_belts',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car'  => ['required' => true],
        'seat_belts',
        'folding_seat_system',
        'cool_heat_seat_system',
        'head_liner',
        'door_trim_panels',
        'floor_carpet',
    ];

}
