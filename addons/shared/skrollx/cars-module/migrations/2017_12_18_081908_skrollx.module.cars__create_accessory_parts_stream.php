<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateAccessoryPartsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'accessory_parts',
         'title_column' => 'right_mirror_camera',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'car'  => ['required' => true],
        'right_mirror_camera',
        'left_mirror_camera',
        'front_parking_sensor',
        'rear_parking_sensor',
        'sun_para_roof_condition',
        'right_side_mirror',
        'left_side_mirror',
        'front_wind_screen',
        'rear_wind_screen',
        'wiper_blades',
    ];

}
