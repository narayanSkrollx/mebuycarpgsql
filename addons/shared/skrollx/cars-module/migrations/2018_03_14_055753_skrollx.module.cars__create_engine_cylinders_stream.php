<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateEngineCylindersStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'engine_cylinders',
         'title_column' => 'cylinder_number',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'cylinder_number' => [
            'required' => true,
        ],
        'live'
    ];

}
