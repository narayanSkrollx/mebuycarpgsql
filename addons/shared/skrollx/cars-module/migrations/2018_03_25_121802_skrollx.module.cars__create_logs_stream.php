<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleCarsCreateLogsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'logs',
        'title_column' => 'title',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'title' => [
            'required' => true,
        ],
        'car' => [
            'required' => true,
        ],
        'description' => [
            'required' => true
        ]
    ];

}
