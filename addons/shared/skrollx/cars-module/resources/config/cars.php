<?php

return [
    'status' => [
        '1'  => 'skrollx.module.cars::field.status.options.1',
        '2'  => 'skrollx.module.cars::field.status.options.2',
        '3'  => 'skrollx.module.cars::field.status.options.3',
        '4'  => 'skrollx.module.cars::field.status.options.4',
        '5'  => 'skrollx.module.cars::field.status.options.5',
    ],
    'status_options' => [
        '1' => ['2', '3', '4', '5'],
        '2' => ['3', '4', '5'],
        '3' => ['4', '5'],
        '4' => ['5'],
        '5' => [],
    ],
    'depreciation' => [
        'year' => [
            '0-1'   => 20,
            '2-5'   => 15,
            '5-200' => 10,
        ],
        'mileage' => [
            '1-5'   => 2,
            '6-10'  => 4,
            '10-15' => 6,
            '16-20' => 6,
        ],
        'paint' => [
            1 => 0,
            2 => 7,
            3 => 7,
        ],
        'specs' => [
            1 => 0,
            2 => 15,
            3 => 15,
        ],
        'option' => [
            1 => 15,
            2 => 7.5,
            3 => 0
        ],
        'litre' => 10
    ],
    'bidding_limit' => [
        '5000' => '75000',
        '10000' => '100000',
        '15000' => '175000',
        '20000' => '250000',
        '25000' => '350000',
        '30000' => '450000',
        '40000' => '550000',
        '50000' => '700000',
        '60000' => '800000',
        '75000' => '1000000',
        '100000' => '10000000',
    ]
];
