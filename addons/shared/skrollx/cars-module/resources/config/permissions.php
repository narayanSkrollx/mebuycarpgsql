<?php 
	
	return [
		'cars' => [
			'manage',
			'read',
	        'write',
	        'delete',
		],
		'makes' => [
			'manage',
			'read',
	        'write',
	        'delete',
		],
		'models' => [
			'manage',
			'read',
	        'write',
	        'delete',
		],
		'locations' => [
			'manage',
			'read',
	        'write',
	        'delete',
		],
		'dates' => [
			'manage',
			'read',
	        'write',
	        'delete',
		],
		'engine_sizes' => [
			'manage',
			'read',
	        'write',
	        'delete',
		],
		'prices' => [
			'manage',
			'read',
	        'write',
	        'delete',
		],
		'evaluation_configure' => [
			'manage',
			'read',
	        'write',
	        'delete',
		],

	];