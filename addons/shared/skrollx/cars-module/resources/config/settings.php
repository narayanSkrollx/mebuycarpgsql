<?php

return [
    'admin_fee' => [
        'type'     => 'anomaly.field_type.integer',
        'config'   => [
            'default_value' => '229',
        ],
    ],
    'odometer_check_fee' => [
        'type'     => 'anomaly.field_type.integer',
        'config'   => [
            'default_value' => '49',
        ],
    ],
    'bid_increment_amount' => [
        'type'     => 'anomaly.field_type.integer',
        'config'   => [
            'default_value' => '500',
        ],
    ],
];
