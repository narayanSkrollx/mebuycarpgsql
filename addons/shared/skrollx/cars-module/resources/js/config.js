$('.add_more_year').click(function(){
  var count = $(this).data('count');
      new_count = count+1;
      that = $(this);
  $.ajax({
    url: '/evaluation_config/year_row',
    dataType: 'JSON',
    mothod: 'POST',
    data: {count:new_count},
  }).done(function(data){
    that.closest('.card-block').find('.section-wrapper').append(data);
    that.data('count',new_count);

    $('.remove_row').click(function(){
       $(this).closest('.form-group').remove();
    })

  });
  
});

$('.add_more_mileage').click(function(){
	var count = $(this).data('count');

	var new_row = '<div class="form-group"><div class="row">'
                        +'<div class="col-md-5">'
                                +'<select name="mileage['+count+'][from]" class="form-control selectpicker">'
                                  +'<option selected="selected" disabled="disabled">Mileage From</option>'
                                  +'<option value="1">Up to 5,000 KM</option>'
                                  +'<option value="2">Up to 10,000 KM</option>'
                                  +'<option value="3">Up to 20,000 KM</option>'
                                  +'<option value="4">Up to 30,000 KM</option>'
                                  +'<option value="5">Up to 40,000 KM</option>'
                                  +'<option value="6">Up to 60,000 KM</option>'
                                  +'<option value="7">Up to 80,000 KM</option>'
                                  +'<option value="8">Up to 100,000 KM</option>'
                                  +'<option value="9">Up to 125,000 KM</option>'
                                  +'<option value="10">Up to 150,000 KM</option>'
                                  +'<option value="11">Up to 175,000 KM</option>'
                                  +'<option value="12">Up to 200,000 KM</option>'
                                  +'<option value="13">Up to 225,000 KM</option>'
                                  +'<option value="14">Up to 250,000 KM</option>'
                                  +'<option value="15">More than 250,000 KM</option>'
                                  +'<option value="16">Don’t know, assume normal usage</option>'
                                +'</select>'
                        +'</div>'
                        +'<div class="col-md-5">'
                                +'<select name="mileage['+count+'][to]" class="form-control selectpicker">'
                                  +'<option selected="selected" disabled="disabled">Mileage To</option>'
                                  +'<option value="1">Up to 5,000 KM</option>'
                                  +'<option value="2">Up to 10,000 KM</option>'
                                  +'<option value="3">Up to 20,000 KM</option>'
                                  +'<option value="4">Up to 30,000 KM</option>'
                                  +'<option value="5">Up to 40,000 KM</option>'
                                  +'<option value="6">Up to 60,000 KM</option>'
                                  +'<option value="7">Up to 80,000 KM</option>'
                                  +'<option value="8">Up to 100,000 KM</option>'
                                  +'<option value="9">Up to 125,000 KM</option>'
                                  +'<option value="10">Up to 150,000 KM</option>'
                                  +'<option value="11">Up to 175,000 KM</option>'
                                  +'<option value="12">Up to 200,000 KM</option>'
                                  +'<option value="13">Up to 225,000 KM</option>'
                                  +'<option value="14">Up to 250,000 KM</option>'
                                  +'<option value="15">More than 250,000 KM</option>'
                                  +'<option value="16">Don’t know, assume normal usage</option>'
                                +'</select>'
                        +'</div>'
                        +'<div class="col-md-5">'
                            +'<input type="number" class="form-control" name="mileage['+count+'][depr]" placeholder="Depreciation %">'
                        +'</div><button type="button" class="remove_row"><i class="fa fa-minus"></i></button>'
                    +'</div></div>';

    $(this).closest('.card-block').find('.section-wrapper').append(new_row);
	
	$(this).data('count',count+1);

    $('.remove_row').click(function(){
       $(this).closest('.form-group').remove();
    })
});


$('.remove_row').click(function(){

})