$(function(){



  $('[name="make"]').on('change', function(){
    makeInputChanged('[name="make"]', '[name="model"]');
  }).change();

  // $('[name="make"]').on('change', function(){

  //   related_field_changed('[name="make"]', '[name="model"]', '/ajax/model-dropdown');

  // }).change();


  $('[name="filter_make"]').on('change', function(){

    related_field_changed('[name="filter_make"]', '[name="filter_model"]', '/ajax/model-dropdown');

  }).change();



  $('[name="model"]').on('change', function(){

    related_field_changed('[name="model"]', '[name="engine_size"]', '/ajax/engine-size-dropdown');

  }).change();



});



function related_field_changed(elem, related_elem, ajax_url){

  if(!ajax_url) return false;



  var elem_id = $(elem).val();

  var $related = $(elem).closest('form').find(related_elem);

  var related_id = $related.val();

  $.ajax({

    url: ajax_url,

    data: {elem:elem_id},

    dataType: 'json',

    method: 'POST',

  }).done(function( d ) {

    var options = "";

    if(d.length != '0'){

      for(key in d){

        if(key == related_id){

          selected = 'selected';

        }else{

          selected = '';

        }

        options += '<option value="'+key+'" '+selected+'>'+d[key]+'</option>';

        }

      $related.closest('.form-group').show();

      $related.html(options);

      $related.trigger('change');

      }else{

        $related.html(options);

      }

    });

}

function makeInputChanged(elem, targetElem){

        var targetElem = targetElem || '[name="model"]';

        var make_id = $(elem).val();

        var model_id = $(targetElem).attr('data-value');

        if(!make_id){

          return;

        }

        var $modelElem = $(elem).closest('form').find(targetElem);

        $.ajax({

          url: '/ajax/model-dropdown',

          data: {make:make_id},

          dataType: 'json',

          method: 'POST',

        }).done(function( d ) {



          var options = "";

          if(d.length != '0'){

            for(key in d){

              if(d[key]['id'] == model_id){

                selected = 'selected';

              }else{

                selected = '';

              }

              options += '<option value="'+d[key]['id']+'" '+selected+'>'+d[key]['title']+'</option>';

              }

            $modelElem.closest('.form-group').show();

            $modelElem.html(options);

            $modelElem.trigger('change');

            }else{

              $modelElem.html(options);

            }

          });

    }