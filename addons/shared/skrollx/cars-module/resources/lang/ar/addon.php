<?php

return [
    'title'       => 'سيارات',
    'name'        => 'السيارات وحدة',
    'description' => '',
    'section' => [
        'cars'      => 'سيارات',
        'models'    => 'عارضات ازياء',
        'makes'     => 'يصنع',
        'locations' => 'مواقع',
        'prices'    => 'الأسعار',
        'biddings'  => 'المناقصات',
    ]
];
