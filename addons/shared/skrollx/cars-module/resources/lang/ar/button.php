<?php

return [
    'new_car'       => 'سيارة جديدة',
    'new_model'     => 'نموذج جديد',
    'new_make'      => 'جعل جديد',
    'new_location'  => 'موقع جديد',
    'new_price'     => 'سعر جديد',
    'save_this_car' => 'حفظ هذه السيارة',
    'view_details'  => 'عرض التفاصيل',
    'bid_now'       => 'عرض الأسعار الآن',
    'start_bidding'       => 'بدء عرض التسعير',
    'biddings'       => 'المناقصات',
    'prices'        => 'الأسعار',
];
