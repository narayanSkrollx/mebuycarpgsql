<?php

return [
    'title' => [
        'name' => 'عنوان',
        'placeholder' => 'أكتب هنا',
    ],
    'name' => [
        'name' => 'اسم'
    ],
    'slug' => [
        'name' => 'سبيكة'
    ],
    'make' => [
        'name' => 'يصنع',
        'placeholder' => 'حدد السيارة'
    ],
    'model' => [
        'name' => 'نموذج',
        'placeholder' => 'حدد الموديل '
    ],
    'image' => [
        'name' => 'صورة'
    ],
    'images' => [
        'name' => 'صور'
    ],
    'transmission' => [
        'name' => 'انتقال',
        'placeholder' => 'حدد انتقال',
        'options' => [
            '1'  => 'كتيب',
            '2'  => 'أوتوماتيكي',
        ]
    ],
    'specs' => [
        'name' => 'المواصفات',
        'placeholder' => 'حدد المواصفات ',
        'options' => [
            '1'  => 'خليجية',
            '2'  => 'غير خليجية ',
            '3'  => 'لا أعلم',
        ]
    ],
    'cylinders' => [
        'name' => 'اسطوانات المحرك ',
        'placeholder' => 'حدد اسطوانات المحرك'
    ],
    'odometer_reading' => [
        'name' => 'عداد المسافات (كم)'
    ],
    'paint' => [
        'name' => 'رسم',
        'placeholder' => 'حدد بينت ',
        'options' => [
            '1'  => 'أصلي',
            '2'  => 'جزئي',
            '3'  => 'مجموع إعادة رسم',
        ]
    ],
    'option' => [
        'name' => 'الخيارات',
        'placeholder' => 'حدد الخيار ',
        'options' => [
            '1'  => 'عادية',
            '2'  => 'خيارات متوسطة',
            '3'  => 'خيارات كاملة',
            '4'  => 'انا لا اعرف',
        ]
    ],
    'history_report' => [
        'name' => 'تقرير تاريخ المركبات',
        'placeholder' => 'حدد تقرير محفوظات المركبات',
        'options' => [
            '1'  => 'وذكرت الحوادث',
            '2'  => 'سيارة شخصية',
            '3'  => 'سيارة العمل',
        ]
    ],
    'accident_history' => [
        'name' => 'الحوادث التاريخ'
    ],
    'service_history' => [
        'name' => 'سجل الخدمة',
        'placeholder' => 'حدد سجل الخدمة',
        'options' => [
            '1'  => 'لا يوجد سجل خدمة',
            '2'  => 'خدمة روتينية',
            '3'  => 'تحقق منتظم',
        ]
    ],
    'body_type' => [
        'name' => 'نوع الجسم',
        'placeholder' => 'اختر نوع الجسم ',
        'options' => [
            '1'  => 'سيارة',
            '2'  => 'كوبيه',
            '3'  => 'عبور',
            '4'  => 'الثابت الأعلى للتحويل',
            '5'  => 'هاتشباك',
            '6'  => 'شاحنة بيك اب',
            '7'  => 'لينة الأعلى للتحويل',
            '8'  => 'سيارة رياضية',
            '9'  => 'SUV',
            '10'  => 'شاحنة فائدة',
            '11'  => 'سيارة نقل',
            '12'  => 'عربة',
        ]
    ],
    'drive' => [
        'name' => 'قيادة',
        'placeholder' => 'حدد دريف ',
        'options' => [
            '1'  => 'دفع على جميع العجلات',
            '2'  => 'نظام دفع بالعجلات الأمامية',
            '3'  => 'العجلات الخلفية',
        ]
    ],
    'color' => [
        'name' => 'اللون',
        'placeholder' => 'إختر لون',
        'options' => [
            '1'  => 'أبيض',
            '2'  => 'فضة',
            '3'  => 'أسود',
            '4'  => 'اللون الرمادي',
            '5'  => 'أزرق',
            '6'  => 'أحمر',
            '7'  => 'بنى',
            '8'  => 'أخضر',
            '9'  => 'آخر',
        ]
    ],
    'fuel_type' => [
        'name' => 'نوع الوقود',
        'placeholder' => 'حدد نوع الوقود ',
        'options' => [
            '1'  => 'بنزين',
            '2'  => 'ديزل',
            '3'  => 'CNG',
            '4'  => 'LPG',
            '5'  => 'كهربائي',
        ]
    ],
    'car_number' => [
        'name' => 'رقم السياره'
    ],
    'car' => [
        'name' => 'سيارة'
    ],
    'engine_size' => [
        'name' => 'حجم المحرك ',
        'placeholder' => 'حجم المحرك في لتر',
    ],
    'chassis_damage' => [
        'name' => 'الشاسيه الأضرار'
    ],
    'chassis_repaired' => [
        'name' => 'إصلاح الهيكل'
    ],
    'registered_in' => [
        'name' => 'مسجل في',
        'placeholder' => 'حدد مسجل في'
    ],
    'bank_loan' => [
        'name' => 'قرض مصرفي'
    ],
    'navigation_system' => [
        'name' => 'نظام ملاحة'
    ],
    'no_keys' => [
        'name' => 'عدد مفاتيح ',
        'placeholder' => 'حدد عدد المفاتيح'
    ],
    'roof' => [
        'name' => 'سقف',
        'placeholder' => 'سيليكت روف ',
        'options' => [
            '1'  => 'منتظم',
            '2'  => 'فتح سقف',
            '3'  => 'سقف مهواة',
        ]
    ],
    'rim_type' => [
        'name' => 'ريم تايب',
        'placeholder' => 'حدد ريم تايب ',
        'options' => [
            '1'  => 'صلب',
            '2'  => 'عجلة معدنية'
        ]
    ],
    'rim_condition' => [
        'name' => 'حالة الحافة',
        'placeholder' => 'حدد ريم كونديتيون ',
        'options' => [
            '1'  => 'خدوش صغيرة',
            '2'  => 'علامة تجارية جديدة',
            '3'  => 'كسر',
            '4'  => 'في حالة العمل',
        ]
    ],
    'seat_color' => [
        'name' => 'لون المقعد',
        'placeholder' => 'حدد لون المقعد ',
        'options' => [
            '1'  => 'أبيض',
            '2'  => 'فضة',
            '3'  => 'أسود',
            '4'  => 'اللون الرمادي',
            '5'  => 'أزرق',
            '6'  => 'أحمر',
            '7'  => 'بنى',
            '8'  => 'أخضر',
            '9'  => 'آخر',
        ]
    ],
    'description' => [
        'name' => 'وصف'
    ],
    'min_bid_amount' => [
        'name' => 'الحد الأدنى لمبلغ العرض'
    ],
    'max_bid_amount' => [
        'name' => 'الحد الأقصى لعرض السعر'
    ],
    'rejected_amount' => [
        'name' => 'المبلغ المرفوض',
        'instructions' => 'أذكر أعلى مبلغ رفض البائع إن وجدت.',
    ],
    'expire_date' => [
        'name' => 'المزايدة تاريخ انتهاء الصلاحية ',
        'instructions' => 'سيتم إغلاق عرض الأسعار في هذا التاريخ والوقت.',
    ],
    'year' => [
        'name' => 'عام',
        'placeholder' => 'حدد السنة'
    ],
    'price' => [
        'name' => 'السعر'
    ],
    'evaluation_price' => [
        'name' => 'تقييم الأسعار'
    ],
    'seller_price' => [
        'name' => 'سعر البائع'
    ],
    'year_from' => [
        'name' => 'سنة من'
    ],
    'year_to' => [
        'name' => 'السنة إلى'
    ],
    'mileage' => [
        'name' => 'الأميال',
        'placeholder' => 'حدد الأميال',
        'options' => [
            '1'  => 'حتى 5000 كم',
            '2'  => 'ما يصل إلى 10،000 كم',
            '3'  => 'تصل إلى 20،000 كم',
            '4'  => 'تصل إلى 30،000 كم',
            '5'  => 'تصل إلى 40،000 كم',
            '6'  => 'تصل إلى 60،000 كم',
            '7'  => 'تصل إلى 80،000 كم',
            '8'  => 'ما يصل إلى 100،000 كم',
            '9'  => 'تصل إلى 125،000 كم',
            '10' => 'تصل إلى 150،000 كم',
            '11' => 'تصل إلى 175،000 كم',
            '12' => 'تصل إلى 200،000 كم',
            '13' => 'تصل إلى 225،000 كم',
            '14' => 'تصل إلى 250،000 كم',
            '15' => 'أكثر من 250،000 كم',
            '16' => 'لا أعرف، تفترض الاستخدام العادي',
        ]
    ],
    'live' => [
        'name' => 'حي'
    ],
    'email' => [
        'name' => 'البريد الإلكتروني',
        'placeholder' => 'البريد الإلكتروني'
    ],
    'location' => [
        'name' => 'موقعك',
        'placeholder' => 'اختر موقعا'
    ],
    'first_name' => [
        'name' => 'الاسم الاول',
        'placeholder' => 'الاسم الاول'
    ],
    'last_name' => [
        'name' => 'الكنية',
        'placeholder' => 'الكنية'
    ],
    'requested_by' => [
        'name' => 'بتوصية من'
    ],
    'date' => [
        'name' => 'تاريخ',
        'evaluation' => 'تاريخ التقييم',
        'placeholder' => 'حدد تاريخ'
    ],
    'time' => [
        'name' => 'زمن',
        'placeholder' => 'حدد الوقت'
    ],
    'mobile' => [
        'name' => 'رقم الهاتف المحمول',
        'placeholder' => 'رقم الهاتف المحمول'
    ],
    'alt_contact' => [
        'name' => 'الاتصال البديل',
        'placeholder' => 'الاتصال البديل'
    ],
    'status' => [
        'name' => 'الحالة',
        'placeholder' => 'حدد الحالة',
        'options' => [
            '1'  => 'تقييم',
            '2'  => 'عرض',
            '3'  => 'شراء',
            '4'  => 'مزايدة',
            '5'  => 'تم البيع',
        ]
    ],
    'country' => [
        'name' => 'بلد'
    ],
    'city' => [
        'name' => 'مدينة'
    ],
];
