<?php

return [
    'evaluation_requested' => [
        'subject'  => 'وقد طلب تقييم سيارة جديدة من قبل :name',
        'line1'  => 'وقد طلب تقييم السيارة من قبل :name على :date',
    ],
    'winner_selected' => [
        'greeting'  => 'العزيز :name,',
        'subject'  => 'يتم اختيارك الفائز :name',
        'line1'  => 'يمكنك الحصول على سيارتك الآن. أحد أعضاء فريقنا سيتصل بك قريبا.',
    ],
    'bidding_placed' => [
        'subject'  => 'تم تقديم عرض تسعير جديد :name.',
        'line1'  => 'يمكنك الآن مراجعة العروض من المشرف واختيار الفائز.',
    ]
];
