<?php

return [
    'admin_fee' => [
        'name'  => 'رسوم المشرف',
    ],
    'odometer_check_fee' => [
        'name'  => 'رسوم فحص عداد المسافات',
    ],
    'bid_increment_amount' => [
        'name'  => 'مبلغ زيادة عروض الأسعار',
        'instructions'  => 'سيتم زيادة مبلغ العطاء بمقدار معين',
    ],
];
