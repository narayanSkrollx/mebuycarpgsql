<?php

return [
    'title'       => 'Cars',
    'name'        => 'Cars Module',
    'description' => '',
    'section' => [
        'cars'      => 'Cars',
        'models'    => 'Models',
        'makes'     => 'Makes',
        'locations' => 'Locations',
        'prices'    => 'Prices',
        'biddings'  => 'Biddings',
        'dates'  => 'Dates',
        'engine_sizes'  => 'Engine Sizes',
        'engine_cylinders'  => 'Engine Cyliders',
        'vehicle_detail'    => 'Vehicle Detail',
        'vehicle_specs' => 'Vehicle Specs',
        'vehicle_general'   => 'General',
        'vehicle_body_report'   => 'Vehicle body report',
        'engine_condition'  => 'Engine Condition',
        'engine_noise' => 'Engine Noises',
        'gearbox_condition'    => 'Gear box condition',
        'ac_condition'  => 'AC condition',
        'accessory_parts'   => 'Accessory parts',
        'fluids'    => 'Fluids',
        'testdrive_general'    => 'Test Drive General',
        'electrical_system' => 'Electrical System',
        'tyres_wheels'  => 'Tyres and Wheels',
        'interiors'    => 'Interiors',
        'brakes'    => 'Brakes ',
        'evaluation_configure' => 'Evaluation Config'
    ]
];
