<?php

return [
    'title' => [
        'name' => 'Title',
        'placeholder' => 'Type here',
    ],
    'name' => [
        'name' => 'Name'
    ],
    'slug' => [
        'name' => 'Slug'
    ],
    'make' => [
        'name' => 'Make',
        'placeholder' => 'Select Make'
    ],
    'model' => [
        'name' => 'Model',
        'placeholder' => 'Select Model'
    ],
    'image' => [
        'name' => 'Image'
    ],
    'images' => [
        'name' => 'Images'
    ],
    'transmission' => [
        'name' => 'Transmission',
        'placeholder' => 'Select Transmission',
        'options' => [
            '1'  => 'Manual',
            '2'  => 'Automatic',
        ]
    ],
    'specs' => [
        'name' => 'Specs',
        'placeholder' => 'Select Specs',
        'options' => [
            '1'  => 'GCC',
            '2'  => 'Non GCC',
            '3'  => 'I don\'t know',
        ]
    ],
    'cylinders' => [
        'name' => 'Engine Cylinders',
        'placeholder' => 'Select Engine Cylinders'
    ],
    'odometer_reading' => [
        'name' => 'Odometer Reading (km)'
    ],
    'paint' => [
        'name' => 'Paint',
        'placeholder' => 'Select Paint',
        'options' => [
            '1'  => 'Original',
            '2'  => 'Partial',
            '3'  => 'Total Repaint',
        ]
    ],
    'option' => [
        'name' => 'Option',
        'placeholder' => 'Select Option',
        'options' => [
            '1'  => 'Basic',
            '2'  => 'Mid',
            '3'  => 'Full',
            '4'  => 'I don\'t know',
        ]
    ],
    'history_report' => [
        'name' => 'Vehicle History Report',
        'placeholder' => 'Select Vehicle History Report',
        'options' => [
            '1'  => 'Accident reported',
            '2'  => 'Personal Car',
            '3'  => 'Business Car',
        ]
    ],
    'accident_history' => [
        'name' => 'Accident History'
    ],
    'service_history' => [
        'name' => 'Service History',
        'placeholder' => 'Select Service History',
        'options' => [
            '1'  => 'No Service History',
            '2'  => 'Routine Servicing',
            '3'  => 'Regular Check',
        ]
    ],
    'body_type' => [
        'name' => 'Body Type',
        'placeholder' => 'Select Body Type',
        'options' => [
            '1'  => 'Sedan',
            '2'  => 'Coupe',
            '3'  => 'Crossover',
            '4'  => 'Hard Top Convertible',
            '5'  => 'Hatchback',
            '6'  => 'Pick Up Truck',
            '7'  => 'Soft Top Convertible',
            '8'  => 'Sports Car',
            '9'  => 'SUV',
            '10'  => 'Utility Truck',
            '11'  => 'Van',
            '12'  => 'Wagon',
        ]
    ],
    'drive' => [
        'name' => 'Drive',
        'placeholder' => 'Select Drive',
        'options' => [
            '1'  => 'All Wheel Drive',
            '2'  => 'Front Wheel Drive',
            '3'  => 'Rear Wheel Drive',
        ]
    ],
    'color' => [
        'name' => 'Color',
        'placeholder' => 'Select Color',
        'options' => [
            '1'  => 'White',
            '2'  => 'Silver',
            '3'  => 'Black',
            '4'  => 'Grey',
            '5'  => 'Blue',
            '6'  => 'Red',
            '7'  => 'Brown',
            '8'  => 'Green',
            '9'  => 'Other',
        ]
    ],
    'fuel_type' => [
        'name' => 'Fuel Type',
        'placeholder' => 'Select Fuel Type',
        'options' => [
            '1'  => 'Petrol',
            '2'  => 'Diesel',
            '3'  => 'CNG',
            '4'  => 'LPG',
            '5'  => 'Electric',
        ]
    ],
    'car_number' => [
        'name' => 'Car Number'
    ],
    'car' => [
        'name' => 'Car'
    ],
    'engine_size' => [
        'name' => 'Engine Size',
        'placeholder' => 'Select Engine Size',
    ],
    'engine_sizes' => [
        'name' => 'Engine Sizes',
        'placeholder' => 'Select Engine Sizes',
    ],
    'chassis_damage' => [
        'name' => 'Chassis Damage'
    ],
    'chassis_repaired' => [
        'name' => 'Chassis Repaired'
    ],
    'registered_in' => [
        'name' => 'Registered In',
        'placeholder' => 'Select Registered In'
    ],
    'bank_loan' => [
        'name' => 'Bank Loan'
    ],
    'navigation_system' => [
        'name' => 'Navigation System'
    ],
    'no_keys' => [
        'name' => 'Number Of Keys',
        'placeholder' => 'Select Number Of Keys'
    ],
    'roof' => [
        'name' => 'Roof',
        'placeholder' => 'Select Roof',
        'options' => [
            '1'  => 'Regular',
            '2'  => 'Open Roof',
            '3'  => 'Ventilated Roof',
        ]
    ],
    'rim_type' => [
        'name' => 'Rim Tipe',
        'placeholder' => 'Select Rim Tipe',
        'options' => [
            '1'  => 'Steel',
            '2'  => 'Alloy Wheel'
        ]
    ],
    'rim_condition' => [
        'name' => 'Rim Condition',
        'placeholder' => 'Select Rim Condition',
        'options' => [
            '1'  => 'Small Scratches',
            '2'  => 'Brand New',
            '3'  => 'Broken',
            '4'  => 'In Working Condition',
        ]
    ],
    'seat_color' => [
        'name' => 'Seat Color',
        'placeholder' => 'Select Seat Color',
        'options' => [
            '1'  => 'White',
            '2'  => 'Silver',
            '3'  => 'Black',
            '4'  => 'Grey',
            '5'  => 'Blue',
            '6'  => 'Red',
            '7'  => 'Brown',
            '8'  => 'Green',
            '9'  => 'Other',
        ]
    ],
    'description' => [
        'name' => 'Description'
    ],
    'min_bid_amount' => [
        'name' => 'Min Bid Amount'
    ],
    'max_bid_amount' => [
        'name' => 'Max Bid Amount'
    ],
    'rejected_amount' => [
        'name' => 'Rejected Amount',
        'instructions' => 'Mention highest amount seller rejected if any.',
    ],
    'expire_date' => [
        'name' => 'Bid Expire Date',
        'instructions' => 'Bidding will be closed at this date and time.',
    ],
    'year' => [
        'name' => 'Year',
        'placeholder' => 'Select Year'
    ],
    'price' => [
        'name' => 'Price'
    ],
    'evaluation_price' => [
        'name' => 'Evaluation Price'
    ],
    'seller_price' => [
        'name' => 'Seller Price'
    ],
    'year_from' => [
        'name' => 'Year From'
    ],
    'year_to' => [
        'name' => 'Year To'
    ],
    'mileage' => [
        'name' => 'Mileage',
        'placeholder' => 'Select Mileage',
        'options' => [
            '1'  => 'Up to 5,000 KM',
            '2'  => 'Up to 10,000 KM',
            '3'  => 'Up to 20,000 KM',
            '4'  => 'Up to 30,000 KM',
            '5'  => 'Up to 40,000 KM',
            '6'  => 'Up to 60,000 KM',
            '7'  => 'Up to 80,000 KM',
            '8'  => 'Up to 100,000 KM',
            '9'  => 'Up to 125,000 KM',
            '10' => 'Up to 150,000 KM',
            '11' => 'Up to 175,000 KM',
            '12' => 'Up to 200,000 KM',
            '13' => 'Up to 225,000 KM',
            '14' => 'Up to 250,000 KM',
            '15' => 'More than 250,000 KM',
            '16' => 'Don’t know, assume normal usage',
        ]
    ],
    'live' => [
        'name' => 'Live'
    ],
    'email' => [
        'name' => 'Email',
        'placeholder' => 'Your email address',
    ],
    'location' => [
        'name' => 'Location',
        'placeholder' => 'Select Location'
    ],
    'first_name' => [
        'name' => 'First Name',
        'placeholder' => 'First Name'
    ],
    'last_name' => [
        'name' => 'Last Name',
        'placeholder' => 'Last Name'
    ],
    'requested_by' => [
        'name' => 'Requested by'
    ],
    'date' => [
        'name' => 'Date',
        'evaluation' => 'Evaluation Date',
        'placeholder' => 'Select Date'
    ],
    'time' => [
        'name' => 'Time',
        'placeholder' => 'Select Time'
    ],
    'mobile' => [
        'name' => 'Mobile Number',
        'placeholder' => 'Mobile Number'
    ],
    'alt_contact' => [
        'name' => 'Alternative Contact',
        'placeholder' => 'Alternative Contact'
    ],
    'status' => [
        'name' => 'Status',
        'placeholder' => 'Select Status',
        'options' => [
            '1'  => 'Evaluation',
            '2'  => 'Offer',
            '3'  => 'Purchase',
            '4'  => 'Bidding',
            '5'  => 'Sold',
        ]
    ],
    'country' => [
        'name' => 'Country'
    ],
    'city' => [
        'name' => 'City'
    ],
    'reserved' => [
        'name' => 'Reserved'
    ],
    'from_date' => [
        'name' => 'From Date'
    ],
    'to_date' => [
        'name' => 'To Date'
    ],
    'times' => [
        'name' => 'Times'
    ],
    'engine' => [
        'options' => [
            '1' => '4.0L',
            '2' => '2.7'
        ]
    ],
    'engine' => [
        'options' => [
            '1' => 'Remote Key',
            '2' => 'Manual Key'
        ]
    ],
    'wheel' => [
        'options' => [
            '1' => 'Alloy wheels',
            '2' => 'Steel wheels',
            '3' => 'Wheel caps'
        ]
    ],
    'interior' => [
        'name' => 'Interior',
        'options' => [
            '1' => 'Alloy wheels',
            '2' => 'Steel wheels',
            '3' => 'Wheel caps'
        ]
    ],
    'brakes' => [
        'name' => 'Brakes',
        'options' => [
            '1' => 'Good',
            '2' => 'Average',
            '3' => 'Need to change immidiately',
        ]
    ],
    'type_conditions' => [
        'options' => [
            '1' => 'Good',
            '2' => 'Average',
            '3' => 'Need to change immidiately',
        ]
    ],
    'good_damage' => [
        'options' => [
            '1' => 'Good',
            '2' => 'Damage',
        ]
    ],
    'average_damage' => [
        'options' => [
            '1' => 'Average',
            '2' => 'Damage',
        ]
    ],
    'power_switches' => [
        'options' => [
            '1' => 'Average',
            '2' => 'Not working',
        ]
    ],
    'working_notworking' => [
        'options' => [
            '1' => 'Working',
            '2' => 'Not working',
        ]
    ],
    'light_options' => [
        'options' => [
            '1' => 'Average',
            '2' => 'Not working',
            '3' => 'Glass Damage',
        ]
    ],
    'average_glassdamage' => [
        'options' => [
            '1' => 'Average',
            '2' => 'Glass Damage',
        ]
    ],
    'test_drive_options_1' => [
        'options' => [
            '1' => 'Smooth',
            '2' => 'Average',
            '3' => 'Noisy',
        ]
    ],
    'test_drive_options_2' => [
        'options' => [
            '1' => 'Smooth',
            '2' => 'Average',
            '3' => 'Noisy',
            '4' => 'Not Available',
        ]
    ],
    'test_drive_options_3' => [
        'options' => [
            '1' => 'Smooth',
            '2' => 'Damage',
            '3' => 'Not Available',
        ]
    ],
    'fluids_otions_1' => [
        'options' => [
            '1' => 'Good',
            '2' => 'Average',
            '3' => 'Need to change immediately',
        ]
    ],
    'fluids_otions_2' => [
        'options' => [
            '1' => 'Good',
            '2' => 'Average',
            '3' => 'Need to change immediately',
            '4' => 'Not Available',
        ]
    ],
    'plate_number' => [
        'name' => 'Plate Number'
    ],
    'model_type' => [
        'name' => 'Model Type'
    ],
    'engine' => [
        'name' => 'Engine',
        'options' => [
            '1' => '4.0L',
            '2' => '2.7L',
        ]
    ],
    'body' => [
        'name' => 'Body'
    ],
    'power_options' => [
        'name' => 'Power Options'
    ],
    'keys'  => [
        'name' => 'Keys',
        'options' => [
            '1' => 'Remote key',
            '2' => 'Manual Key',
        ]
    ],
    'iterior'   => [
        'name' => 'Iterior',
        'options' => [
            '1' => 'L/S',
            '2' => 'F/S',
            '3' => 'Semi Leather'
        ]
    ],
    'sun_para_roof' => [
        'name' => 'SunRoof / Panaromic Roof'
    ],
    'wheel' => [
        'name' => 'Wheel',
        'options' => [
            '1' => 'Alloy Wheels',
            '2' => 'steel wheel',
            '3' => 'wheel Caps'
        ]
    ],
    'fog_lamp'  => [
        'name' => 'Fog Lamps'
    ],
    'zenon_lights'  => [
        'name' => 'Zenon Lights'
    ],
    'cruise_control'    => [
        'name' => 'Cruise Control'
    ],
    'blue_tooth'    => [
        'name' => 'Bluetooth'
    ],
    'cd_dvd_player' => [
        'name' => 'CD DVD player'
    ],
    'rear_camera'   => [
        'name' => 'Rear Camera'
    ],
    'air_bag'   => [
        'name' => 'Air Bag'
    ],
    'parking_sensor'    => [
        'name' => 'Parking Sensor'
    ],
    'differential_lock' => [
        'name' => 'Differential Lock'
    ],
    'car_registered_in' => [
        'name' => 'Car Registered In',
        'options' => [
            '1' => 'Dubai',
            '2' => 'Abu Dhabi',
            '3' => 'Sharjah',
            '4' => 'Ajman',
            '5' => 'RAK ',
            '6' => 'Fujairah ',
            '7' => 'ULQ',
        ]
    ],
    'dealers_warranty' => [
        'name' => 'Dealers Warranty'
    ],
    'dealers_extended_warranty' => [
        'name' => 'Dealers Extended Warranty'
    ],
    'warranty_service_booklet' => [
        'name' => 'Warranty Serive Booklets'
    ],
    'vehicle_history' => [
        'name' => 'Vehicle History',
        'options' => [
            '1' => 'Personal',
            '2' => 'Dealer',
            '3' => 'Rent a car',
            '4' => 'Private company Vehicle'
        ]
    ],
    'accident_reported' => [
        'name' => 'Accident Reported'
    ],
    'front_bumper' => [
        'name' => 'Front Bumper',
        'options' => [
            '1' => 'Original Paint',
            '2' => 'Painted',
            '3' => 'portion repainted',
            '4' => 'Sechres',
            '5' => 'Multipal scraches',
            '6' => 'Dents',
            '7' => 'Multiple Dents',
            '8' => 'Hair Line Secraches Rust',
        ]
    ],
    'front_bonnete' => [
        'name' => 'Front Bonnete'
    ],
    'front_left_fender' => [
        'name' => 'Front Left Fender'
    ],
    'front_left_door' => [
        'name' => 'Front Left Door'
    ],
    'rear_bumper' => [
        'name' => 'Rear Bumber'
    ],
    'rear_left_door' => [
        'name' => 'Rear Left Door'
    ],
    'rear_left_fender' => [
        'name' => 'Rear Left Fender'
    ],
    'front_right_fender' => [
        'name' => 'Front Right Fender'
    ],
    'front_right_door' => [
        'name' => 'Front Right Door'
    ],
    'rear_right_door' => [
        'name' => 'Rear Right Door'
    ],
    'rear_right_fender' => [
        'name' => 'Rear Right Fender'
    ],
    'rear_trunk' => [
        'name' => 'Rear Trunk'
    ],
    'running_board_right' => [
        'name' => 'Running Board Right'
    ],
    'running_board_left' => [
        'name' => 'Running Board Left'
    ],
    'head_gasket_leak' => [
        'name' => 'Head Gasket Leak'
    ],
    'oil_sump_leak' => [
        'name' => 'Oil Sump Leak'
    ],
    'value_cover_oil_leak' => [
        'name' => 'Value Cover Oil Leak'
    ],
    'coolant_leak' => [
        'name' => 'Coolant Leak'
    ],
    'warning_light_on' => [
        'name' => 'Warning Light On'
    ],
    'power_steering_oil_leak' => [
        'name' => 'Power Steering Oil Leak'
    ],
    'engine_misfiring' => [
        'name' => 'Engine Misfiring'
    ],
    'need_sparkplug_replace' => [
        'name' => 'Need Sparkplug Replace'
    ],
    'engine_lack_powers' => [
        'name' => 'Engine lak powers'
    ],
    'engine_overheating' => [
        'name' => 'Engine Overheating'
    ],
    'engine_vibration_in_acc' => [
        'name' => 'Engine Vibration In Accelaration'
    ],
    'battery_weak' => [
        'name' => 'Battery Weak',
        'options' => [
            '1' => 'Yes',
            '2' => 'No',
            '3' => 'Average',
        ]
    ],
    'engine_mounting' => [
        'name' => 'Engine Mounting',
        'options' => [
            '1' => 'Good',
            '2' => 'Average',
            '3' => 'Need to change immidiately',
        ]
    ],
    'battery_dead' => [
        'name' => 'Battery Dead'
    ],
    'air_condition_working' => [
        'name' => 'Air Condition Working',
        'options' => [
            '1' => 'Yes',
            '2' => 'No',
            '3' => 'Low cooling'
        ]
    ],
    'ac_compresor' => [
        'name' => 'AC Compressor',
        'options' => [
            '1' => 'Good',
            '2' => 'Average',
            '3' => 'Noisy',
            '4' => 'Not working'
        ]
    ],
    'heating_system' => [
        'name' => 'Heating System',
        'options' => [
            '1' => 'Good',
            '2' => 'Noisy',
            '3' => 'Not working'
        ]
    ],
    'engine_valve_noise' => [
        'name' => 'Engine Valve Noise'
    ],
    'tappet_noise' => [
        'name' => 'Tappet Noise'
    ],
    'timebelt_noise' => [
        'name' => 'Timebelt Noise'
    ],
    'pulley_noise' => [
        'name' => 'Pulley Noise'
    ], 
    'engine_bearing_noise' => [
        'name' => 'Engine Bearing Noise'
    ],
    'exhaust_manifold_noise' => [
        'name' => 'Exhaust Manifold Noise'
    ],
    'engine_noise' => [
        'name' => 'Engine Noise'
    ],
    'drive_belt_noise' => [
        'name' => 'Drive Belt Noise',
        '1' => 'Good',
        '2' => 'Average',
        '3' => 'Damage',

    ],
    'silencer_condition' => [
        'name' => 'Silencer Condition'
    ], 
    'silencer_smoke_condition' => [
        'name' => 'Silencer Smoke Condition',
        'options' => [
            '1' => 'Good',
            '2' => 'Average',
            '3' => 'White Smoke',
            '4' => 'Black Smoke'
        ]
    ], 
    'engine_turbo_condition' => [
        'name' => 'Engine Turbo Condition',
        'options' => [
            '1' => 'Average',
            '2' => 'Damage',
            '3' => 'Not Available'
        ]
    ],
    'engine_oil_filter' => [
        'name' => 'Engine Oil Filter'
    ],
    'break_fluid' => [
        'name' => 'Break Fluid'
    ],
    'driver_axle_fluid' => [
        'name' => 'Driver Axle Fluid'
    ],
    'collant' => [
        'name' => 'Coolant'
    ],
    'transmission_fluid' => [
        'name' => 'Transmission Fluid'
    ],
    'power_steering_fluid' => [
        'name' => 'Power Steering Fluid'
    ],
    'clutch_fluid' => [
        'name' => 'Clutch Fluid'
    ],
    'engine_startup' => [
        'name' => 'Engine Startup'
    ],
    'engine_exhaust' => [
        'name' => 'Engine Exhaust'
    ],
    'shift_shock' => [
        'name' => 'Shift Shock'
    ],
    'shift_interlock' => [
        'name' => 'Shift interlock'
    ],
    'steer_wheel' => [
        'name' => 'Steer Wheel'
    ],
    'suspension' => [
        'name' => 'Suspension'
    ],
    'electric_seats' => [
        'name' => 'Electric Seats',
        'options' => [
            '1'  => 'Smooth',
            '2'  => 'Noisy',
            '3'  => 'Damage',
        ]
    ],
    'diff_lock' => [
        'name' => 'Diff Lock'
    ],
    'cruise_contorl_condition' => [
        'name' => 'Cruise Control Condition'
    ],
    'manual_gear' => [
        'name' => 'Manual Gear'
    ],
    'differentail_drive_axle' => [
        'name' => 'Differential Drive Axle'
    ],
    'bonnete_shocks' => [
        'name' => 'Bonnete Shocks'
    ],
    'power_lift_gate_electrical' => [
        'name' => 'Power Lift Gate Electrical'
    ],
    'front_head_lights' => [
        'name' => 'Front Head Lights'
    ],
    'tail_lamps' => [
        'name' => 'Tail Lamps'
    ],
    'indicators' => [
        'name' => 'Indicators'
    ],
    'front_right_door_power_switches' => [
        'name' => 'Front Right Door Power Switches'
    ],
    'front_left_door_power_switches' => [
        'name' => 'Front Left Door Power Switches'
    ],
    'rear_right_door_power_switches' => [
        'name' => 'Rear Right Door Power Switchess'
    ],
    'rear_left_door_power_switches' => [
        'name' => 'Rear Left Door Power Switches'
    ],
    'cd_radio_electricals' => [
        'name' => 'CD / Radio /DVD  Electricals'
    ],
    'door_centeral_looks_system' => [
        'name' => 'Doors Centeral Looks system'
    ],
    'arms_rest_console' => [
        'name' => 'Arms Rest Console '
    ],
    'glove_box' => [
        'name' => 'Glove Box'
    ],
    'warning_signal_active' => [
        'name' => 'Warning Signal Active'
    ],
    'horn_working' => [
        'name' => 'Horn Working'
    ],
    'tyres_matches' => [
        'name' => 'Tyres Matches'
    ],
    'wheel_rim_matches' => [
        'name' => 'Wheels / Rims Matches'
    ],
    'spare_tyre_available' => [
        'name' => 'Spare Tyre Available'
    ],
    'jack_tool_available' => [
        'name' => 'Wheel Jack And Tools Available'
    ],
    'front_right_tyre_condition' => [
        'name' => 'Front Right Tyre Condition'
    ],
    'front_left_tyre_condition' => [
        'name' => 'Front Left Tyre Condition'
    ],
    'rear_right_tyre_condition' => [
        'name' => 'Rear Rght Tyre Condition'
    ],
    'rear_left_tyre_condition' => [
        'name' => 'Rear Left Tyre Condition'
    ],
    'wheel_covers' => [
        'name' => 'Wheel Covers',
        'options' => [
            '1' => 'Good',
            '2' => 'Average',
            '3' => 'Damage',
        ]
    ],
    'alloy_wheel_condition' => [
        'name' => 'Alloy Wheels comdition',
        'options' => [
            '1' => 'Good',
            '2' => 'Average',
            '3' => 'Damage',
            '3' => 'Scratches',
        ]
    ],
    'brake_pad_shoe' => [
        'name' => 'Brake Pads and Shoes '
    ],
    'master_cylender_booster' => [
        'name' => 'Master Cylender & Booster '
    ],
    'parking_brake' => [
        'name' => 'Parking Brake '
    ],
    'hand_foot_brake' => [
        'name' => 'Hand Brake / Foot Brake '
    ],
    'seat_belts' => [
        'name' => 'Seat Bealts '
    ],
    'folding_seat_system' => [
        'name' => 'Folding Seats System'
    ],
    'cool_heat_seat_system' => [
        'name' => 'Cooled/ Heater Seats system'
    ],
    'head_liner' => [
        'name' => 'Head Liner '
    ],
    'door_trim_panels' => [
        'name' => 'Door Trims & Door Panels'
    ],
    'floor_carpet' => [
        'name' => 'Floor Carpets',
        'options' => [
            '1' => 'Good',
            '2' => 'Damage',
            '3' => 'Missing',
        ]
    ],
    'right_mirror_camera' => [
        'name' => 'Right Mirror Camera'
    ],
    'left_mirror_camera' => [
        'name' => 'Left Mirror Camera'
    ],
    'front_parking_sensor' => [
        'name' => 'Front  Parking Sensors '
    ],
    'rear_parking_sensor' => [
        'name' => 'Rear Parking Sensors '
    ],
    'sun_para_roof_condition' => [
        'name' => 'Sun Roof / Panaromic Roof'
    ],
    'right_side_mirror' => [
        'name' => 'Right Side Mirror'
    ],
    'left_side_mirror' => [
        'name' => 'Left Side Mirror'
    ],
    'front_wind_screen' => [
        'name' => 'Front Wind Screen'
    ],
    'rear_wind_screen' => [
        'name' => 'Rear Wind Screen'
    ],
    'wiper_blades' => [
        'name' => 'Wiper Blades '
    ],
    'delay_in_shift' => [
        'name' => 'Dealy In Shift'
    ], 
    'transmission_whining_sound' => [
        'name' => 'Transmission Whining Sound'
    ], 
    'shift_shock' => [
        'name' => 'Shift Shock'
    ],
    'gear_slipping' => [
        'name' => 'Gear Slipping'
    ],
    'noise_differential_gear' => [
        'name' => 'Noise Differential Gear'
    ],
    'transmission_oil_leak' => [
        'name' => 'Transmission Oil Leak'
    ],
    'hard_clutch' => [
        'name' => 'Hard Clutch',
        'options' => [
            '1' => 'Yes',
            '2' => 'No',
            '3' => 'Smooth'
        ]
    ],
    'gear_shifting' => [
        'name' => 'Gear Shifting',
        'options' => [
            '1' => 'Delay in Shift',
            '2' => 'Shift Smoothy',
        ]
    ],
    'transmission_mounting' => [
        'name' => 'Transmission Mounting'
    ],
    'from_year' => [
        'placeholder' => 'From Year'
    ],
    'to_year' => [
        'placeholder' => 'To Year'
    ],
    'cylinder_number' => [
        'name' => 'Cylinder Number'
    ],
    'engine_cylinder' => [
        'name' => 'Engine Cylinder'
    ],
    'booking_count' => [
        'name' => 'Booking Count'
    ],
    'checked_in' => [
        'name' => 'Checked In'
    ],
    'deal_status' => [
        'name' => 'Status'
    ]
];