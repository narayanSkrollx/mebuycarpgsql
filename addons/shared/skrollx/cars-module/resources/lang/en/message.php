<?php

return [
    'select_car' => 'Please select your car specifications.',
    'evaluation_complete' => 'Thank you for choosing Mebuycar.com. Your appointment is at :date in :location at :time, a confirmation email has been sent to your email address.',
    'evaluation_complete_no_email' => 'Thank you for choosing Mebuycar.com. Your appointment is at :date in :location at :time .',
    'no_cars' => 'No cars found.',
    'fee_info' => 'Please note that you will be charged an admin fee of :total_fee and a mandatory odometere check with authorities of :odometer_check_fee. Total Added Services: :admin_fee.',
    'max_bid_info' => 'We can close the :car_name if you increase your bid to <span>:max_bid_amount</span>',
    'minimum_bid_info' => 'Minimum Bid <span>:min_bid_amount</span>',
    'rejected_amount_info' => 'Seller already rejected <span>:rejected_amount</span> before. Try bidding more!',
    'car_valuation_info' => 'Please enter following information to see your valuation',
    'could_not_evaluate_car' => 'We couldn\'t find your car value now',
    'could_not_evaluate_car_info' => 'but Never miss a FREE car inspection for final offer! Our network ensures highest prices fast.',
    'thank_you_book_appointment' => 'Thank you. Now Book an Appointment.',
    'extra_cash_no_hassle' => 'Extra Cash. No Hassle. 100% Free',
    'extra_cash_no_hassle_info' => 'You might get more cash, if your car has special equipment. Instant Payment, no paperwork hassle. We buy ANY car, even if it is financed.',
    'suggest_car_worth' => 'Suggest what you think your car is worth:',
    'bidding_success' => 'Bidding has been placed successfully.',
    'bidding_error' => 'Bidding could not be placed.',
    'bidding_no_car_found' => 'No car found for bidding. Please try refreshing page.',
    'bidding_not_available_for_bidding' => 'Listed car is not available for bidding now. Please try with other cars.',
    'bidding_time_expired' => 'Bidding time has expired.',
    'bidding_not_enough_balance' => 'You don\'t have enough balance. Please deposit :amount more to bid for this vehicle.',
    'bidding_min_balance_msg' => 'Bidding price should be more than or equal to :amount',
    'bidding_placed_success' => 'Bidding placed successfully. We will contact you if bid is won.',
    'confirm_bid_message' => '20% will be deducted if bid is won',
    'user_checkin_success' => 'User checked in successfully',
    'user_checkout_success' => 'User checked out successfully',
    'checkin_mobile_invalid' => 'Invalid phone number',
];
