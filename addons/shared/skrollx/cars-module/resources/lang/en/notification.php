<?php

return [
    'evaluation_requested' => [
        'subject'  => 'New car evaluation has been requested by :name',
        'line1'  => 'Car evaluation has been requested by :name on :date',
    ],
    'winner_selected' => [
        'greeting'  => 'Dear :name,',
        'subject'  => 'You are selected as winner for :name',
        'line1'  => 'You can get your car now. One of our team member will contact your shortly.',
    ],
    'bidding_placed' => [
        'subject'  => 'A new bidding has been placed for :name.',
        'line1'  => 'You can now review biddings from admin and select a winner.',
    ],
    'booking-confirmed' => [
        'subject' => 'Your booking has been confirmed.',
        'para1' => '<p>Thank you for choosing mebuycar.com! </p> <p><b>Your booking has been confirmed:</b></p><br> <b>Date & Time:</b> :date :time <br><br><b>location:</b> :location',
        'para2' => '<p>Your satisfaction is our number one priority, we guarantee you that you have chosen the best car buying service company, we assure you that our process will be easy and hassle free. We will inspect the car, give you an offer, and send you the money either by cash or by direct bank transfer.</p><br><p>Before the appointment, please make sure you have the following documents prepared:</p><ul><li><b>Emirates ID</b></li><li><b>Driving License</b></li><li><b>Registration Card</b></li><li><b>Car Keys</b></li></ul>',
        'footerline' => '<p>We look forward to meeting you!</p><br>Sincerely,<br>MeBuyCar Team,<br><br><p>Follow Us On:</p><ul><li>Facebook: :facebook_link</li><li>Instagram: :instagram_link</li><li>Twitter: :twitter_link</li></ul>'
    ]
];
