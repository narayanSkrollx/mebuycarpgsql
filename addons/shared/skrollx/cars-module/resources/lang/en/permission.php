<?php 

	return [
		'cars' => [
			'name' => 'Cars',
			'option' => [
				'read' => 'Can Read Cars?',
				'manage' => 'Can Manage Cars?',
				'write' => 'Can Create Cars?',
				'delete' => 'Can Delete Cars?'
			]
		],
		'makes' => [
			'name' => 'Makes',
			'option' => [
				'read' => 'Can Read Makes?',
				'manage' => 'Can Manage Makes?',
				'write' => 'Can Create Makes?',
				'delete' => 'Can Delete Makes?'
			]
		],
		'models' => [
			'name' => 'Models',
			'option' => [
				'read' => 'Can Read Models?',
				'manage' => 'Can Manage Models?',
				'write' => 'Can Create Models?',
				'delete' => 'Can Delete Models?'
			]
		],
		'locations' => [
			'name' => 'Locations',
			'option' => [
				'read' => 'Can Read Locations?',
				'manage' => 'Can Manage Locations?',
				'write' => 'Can Create Locations?',
				'delete' => 'Can Delete Locations?'
			]
		],
		'dates' => [
			'name' => 'Dates',
			'option' => [
				'read' => 'Can Read Dates?',
				'manage' => 'Can Manage Dates?',
				'write' => 'Can Create Dates?',
				'delete' => 'Can Delete Dates?'
			]
		],
		'engine_sizes' => [
			'name' => 'Engine Sizes',
			'option' => [
				'read' => 'Can Read Engine Sizes?',
				'manage' => 'Can Manage Engine Sizes?',
				'write' => 'Can Create Engine Sizes?',
				'delete' => 'Can Delete Engine Sizes?'
			]
		],
		'prices' => [
			'name' => 'Prices',
			'option' => [
				'read' => 'Can Read Prices?',
				'manage' => 'Can Manage Prices?',
				'write' => 'Can Create Prices?',
				'delete' => 'Can Delete Prices?'
			]
		],
		'evaluation_configure' => [
			'name' => 'Evaluation Configure',
			'option' => [
				'read' => 'Can Read Evaluation Configure?',
				'manage' => 'Can Manage Evaluation Configure?',
				'write' => 'Can Create Evaluation Configure?',
				'delete' => 'Can Delete Evaluation Configure?'
			]
		],
	];