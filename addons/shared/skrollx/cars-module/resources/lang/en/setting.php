<?php

return [
    'admin_fee' => [
        'name'  => 'Admin Fee',
    ],
    'odometer_check_fee' => [
        'name'  => 'Odometer check fee',
    ],
    'bid_increment_amount' => [
        'name'  => 'Bid increment amount',
        'instructions'  => 'Bid amount will be increased by given amount',
    ],
];
