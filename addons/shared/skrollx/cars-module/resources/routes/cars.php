<?php

return [
    'admin/cars' => 'Skrollx\CarsModule\Http\Controller\Admin\CarsController@index',
    'admin/cars/create' => 'Skrollx\CarsModule\Http\Controller\Admin\CarsController@create',
    'admin/cars/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\CarsController@edit',
    'admin/cars/view/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\CarsController@view',
    'admin/cars/biddings' => 'Skrollx\CarsModule\Http\Controller\Admin\CarsController@biddings',
    'admin/cars/biddings/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\CarsController@biddings',
    'admin/cars/select-winner/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\CarsController@selectWinner',

    'ajax/model-dropdown' => 'Skrollx\CarsModule\Http\Controller\AjaxController@modelDropdown',
    'ajax/engine-size-dropdown' => 'Skrollx\CarsModule\Http\Controller\AjaxController@engineSizeDropdown',
    'evaluate/step-1' => 'Skrollx\CarsModule\Http\Controller\CarsController@stepOne',
    'evaluate/step-2' => 'Skrollx\CarsModule\Http\Controller\CarsController@stepTwo',
    'evaluate/thank-you' => 'Skrollx\CarsModule\Http\Controller\CarsController@thankYou',
    'locations' => 'Skrollx\CarsModule\Http\Controller\CarsController@locations',
    
    'cars' => 'Skrollx\CarsModule\Http\Controller\Dealer\CarsController@index',
    'car/{slug}' => 'Skrollx\CarsModule\Http\Controller\Dealer\CarsController@view',
    
    'dealer/ajax-place-bid' => 'Skrollx\CarsModule\Http\Controller\Dealer\AjaxController@placeBid',
    'ajax/location-dates' => 'Skrollx\CarsModule\Http\Controller\AjaxController@getDates',
    'ajax/getTimes' => 'Skrollx\CarsModule\Http\Controller\AjaxController@getTimes',

    'admin/appointments' => 'Skrollx\CarsModule\Http\Controller\Admin\CarsController@adminAppointments',
    'admin/appointments/addlog/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\CarsController@addLog',
    'admin/appointments/viewlog/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\CarsController@viewLog',

    'admin/dashboard/biddings' => 'Skrollx\CarsModule\Http\Controller\Admin\CarsController@adminBiddings',

    'user/check-in' => 'Skrollx\CarsModule\Http\Controller\CarsController@checkInPage',
    'user/check-report' => 'Skrollx\CarsModule\Http\Controller\CarsController@checkVehicleReport',
    'user/view-report/{id}' => 'Skrollx\CarsModule\Http\Controller\CarsController@viewReport',
    'user/changestatus/checkin/{id}' => 'Skrollx\CarsModule\Http\Controller\CarsController@checkIn',
    'user/changestatus/checkout/{id}' => 'Skrollx\CarsModule\Http\Controller\CarsController@checkOut',

    '/evaluation_config/year_row' => 'Skrollx\CarsModule\Http\Controller\Admin\CarsController@getYearRow'
];
