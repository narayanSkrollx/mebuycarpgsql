<?php

return [
    'admin/cars/makes' => 'Skrollx\CarsModule\Http\Controller\Admin\MakesController@index',
    'admin/cars/makes/create' => 'Skrollx\CarsModule\Http\Controller\Admin\MakesController@create',
    'admin/cars/makes/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\MakesController@edit',
    'admin/cars/makes/prices/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\MakesController@prices',
    'makes/prices/getnewrow' => 'Skrollx\CarsModule\Http\Controller\Admin\MakesController@getNewPriceRow'
];
