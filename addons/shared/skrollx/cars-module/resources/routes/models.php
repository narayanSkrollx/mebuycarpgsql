<?php

return [
    'admin/cars/models' => 'Skrollx\CarsModule\Http\Controller\Admin\ModelsController@index',
    'admin/cars/models/create' => 'Skrollx\CarsModule\Http\Controller\Admin\ModelsController@create',
    'admin/cars/models/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\ModelsController@edit'
];
