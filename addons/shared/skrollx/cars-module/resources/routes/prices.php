<?php

return [
    'admin/cars/prices' => 'Skrollx\CarsModule\Http\Controller\Admin\PricesController@index',
    'admin/cars/prices/create' => 'Skrollx\CarsModule\Http\Controller\Admin\PricesController@create',
    'admin/cars/prices/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\PricesController@edit',

    'admin/cars/evaluation_configure' => 'Skrollx\CarsModule\Http\Controller\Admin\EvaluationConfigsController@configure'
];
