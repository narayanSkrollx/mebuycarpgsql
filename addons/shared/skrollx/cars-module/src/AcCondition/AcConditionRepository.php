<?php namespace Skrollx\CarsModule\AcCondition;

use Skrollx\CarsModule\AcCondition\Contract\AcConditionRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class AcConditionRepository extends EntryRepository implements AcConditionRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var AcConditionModel
     */
    protected $model;

    /**
     * Create a new AcConditionRepository instance.
     *
     * @param AcConditionModel $model
     */
    public function __construct(AcConditionModel $model)
    {
        $this->model = $model;
    }
}
