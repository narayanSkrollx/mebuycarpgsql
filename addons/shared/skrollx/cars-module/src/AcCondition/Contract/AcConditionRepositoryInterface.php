<?php namespace Skrollx\CarsModule\AcCondition\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface AcConditionRepositoryInterface extends EntryRepositoryInterface
{

}
