<?php namespace Skrollx\CarsModule\AccessoryPart;

use Skrollx\CarsModule\AccessoryPart\Contract\AccessoryPartInterface;
use Anomaly\Streams\Platform\Model\Cars\CarsAccessoryPartsEntryModel;

class AccessoryPartModel extends CarsAccessoryPartsEntryModel implements AccessoryPartInterface
{

}
