<?php namespace Skrollx\CarsModule\AccessoryPart;

use Skrollx\CarsModule\AccessoryPart\Contract\AccessoryPartRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class AccessoryPartRepository extends EntryRepository implements AccessoryPartRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var AccessoryPartModel
     */
    protected $model;

    /**
     * Create a new AccessoryPartRepository instance.
     *
     * @param AccessoryPartModel $model
     */
    public function __construct(AccessoryPartModel $model)
    {
        $this->model = $model;
    }
}
