<?php namespace Skrollx\CarsModule\AccessoryPart\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface AccessoryPartRepositoryInterface extends EntryRepositoryInterface
{

}
