<?php namespace Skrollx\CarsModule\AccessoryPart\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class AccessoryPartTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'car' => [
            'heading'     => 'skrollx.module.cars::field.car.name',
            'sort_column' => 'name',
            'wrapper'     => '{value.car}-{value.id}',
            'value'       => [
                'car'     => 'entry.car.title',
                'id'    => 'entry.car_id'
            ]
        ],
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit'
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}
