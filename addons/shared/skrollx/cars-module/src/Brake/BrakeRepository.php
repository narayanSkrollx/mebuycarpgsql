<?php namespace Skrollx\CarsModule\Brake;

use Skrollx\CarsModule\Brake\Contract\BrakeRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class BrakeRepository extends EntryRepository implements BrakeRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var BrakeModel
     */
    protected $model;

    /**
     * Create a new BrakeRepository instance.
     *
     * @param BrakeModel $model
     */
    public function __construct(BrakeModel $model)
    {
        $this->model = $model;
    }
}
