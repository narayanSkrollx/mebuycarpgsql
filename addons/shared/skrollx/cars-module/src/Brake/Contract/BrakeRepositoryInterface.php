<?php namespace Skrollx\CarsModule\Brake\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface BrakeRepositoryInterface extends EntryRepositoryInterface
{

}
