<?php namespace Skrollx\CarsModule\Car;

use Anomaly\Streams\Platform\Model\Cars\CarsCarsEntryModel;
use Skrollx\CarsModule\Car\Contract\CarInterface;
use Skrollx\CarsModule\Price\PriceModel;
use Skrollx\CarsModule\Model\ModelModel;
use Skrollx\CarsModule\EngineSize\EngineSizeModel;
use Skrollx\CarsModule\EvaluationConfig\EvaluationConfigModel;

class CarModel extends CarsCarsEntryModel implements CarInterface
{
    public function scopeFilter($query)
    {
        $q          = \Request::get('q');
        $make       = \Request::get('make');
        $model      = \Request::get('model');
        $year       = \Request::get('year');
        $body_type  = \Request::get('body_type');
        $mileage    = \Request::get('mileage');
        $sort_order = \Request::get('sort_order') ?: 'DESC';

        if($make){
            $query->whereIn('cars_cars.make_id', (array)$make);
        }
        if($model){
            $query->whereIn('cars_cars.model_id', (array)$model);
        }
        if($year){
            $query->whereIn('cars_cars.year', (array)$year);
        }
        if($body_type){
            $query->whereIn('cars_cars.body_type', (array)$body_type);
        }
        if($mileage){
            $query->whereIn('cars_cars.mileage', (array)$mileage);
        }
        if($q){
            $query->leftJoin('cars_cars_translations','cars_cars_translations.entry_id','=','cars_cars.id')
                ->where(function($query) use ($q)
                {
                    $query->where('cars_cars_translations.title', 'like', '%'.$q.'%');
                });
        }
        $query->orderBy('cars_cars.created_at', $sort_order);
        $query->groupBy('cars_cars.id');
        return $query;
    }

    public function scopeBiddable($query)
    {
        return $query->where('cars_cars.status', 4)->where('cars_cars.expire_date', '>=', \Carbon\Carbon::now()->toDateTimeString());
    }

    public function scopePostedToday($query)
    {
        return $query->where('cars_cars.created_at', '>=', \Carbon\Carbon::today());
    }

    public function scopePostedLately($query)
    {
        return $query->where('cars_cars.created_at', '>=', \Carbon\Carbon::today()->subMinutes(5));
    }

    public static function evaluateCar($data = [])
    {

        $depreciation = config('skrollx.module.cars::cars.depreciation');
        $increment = config('skrollx.module.cars::cars.increment');

        $years_depreciation = EvaluationConfigModel::where('slug','year')->get()->toArray();
        $mileages_depreciation = EvaluationConfigModel::where('slug','mileage')->get()->toArray();
        $litre_depreciation = EvaluationConfigModel::where('slug','litre')->first()->toArray();

        $return_option = [
            'status' => 0,
            'price' => false
        ];
        $price_option = PriceModel::where('make_id', $data['make'])->where('model_id', $data['model'])->where('year_from','<=',$data['year'])->where('year_to','>=',$data['year'])->where('option',$data['option']);

        if(isset($data['engine_size']) and $data['engine_size'] != ''){
            $price_option = $price_option->where('engine_size_id',$data['engine_size']);
        }

        $price_option = $price_option->first();

        // function for
        // if(!$price_option){
        //     $existing_prices = PriceModel::where('make_id', $data['make'])->where('model_id', $data['model'])->where('option','1')->get();

        //     if(count($existing_prices)){
        //         $min_diff = null;
        //         $near_year_from = null;
        //         $near_year_to = null;
        //         foreach($existing_prices as $key => $value){
        //             if($key == 0){
        //                 $year_from_1 = abs((int)$data['year'] - (int)$value->year_from);
        //                 $year_from_2 = abs((int)$data['year'] - (int)$value->year_to);

        //                 $min_diff = $year_from_1 > $year_from_2 ? $year_from_2 : $year_from_1;

        //                 $near_year_from = $value->year_from;
        //                 $near_year_to = $value->year_to;

        //             }else{
        //                 $year_from_1 = abs((int)$data['year'] - (int)$value->year_from);
        //                 $year_from_2 = abs((int)$data['year'] - (int)$value->year_to);

        //                 $new_min_diff = $year_from_1 > $year_from_2 ? $year_from_2 : $year_from_1;

        //                 if($min_diff > $new_min_diff){
        //                     $min_diff = $new_min_diff;
        //                     $near_year_from = $value->year_from;
        //                     $near_year_to = $value->year_to;
        //                 }

        //             }
        //         }

        //         $price_option = PriceModel::where('make_id', $data['make'])->where('model_id', $data['model'])->where('year_from','<=',$near_year_from)->where('year_to','>=',$near_year_to)->where('option','1')->first();
        //     }
        // }


        if(!$price_option){
            return $return_option;
        }

        $no_year_depre = false;

        if($data['year'] == $price_option->year_to){
            $no_year_depre = true;
        }

        $depre_year_diff = $price_option->year_to - (int)$data['year'];

        $year_diff = (int)date('Y') - (int)$data['year'];
        if(!$year_diff){
            $year_diff = 1;
        }

        $depreciation_percent = 0;
        
        $model= ModelModel::where('id', $data['model'])->first();

        $engine_sizes = $model->engine_sizes;

        if($no_year_depre == false){           
            $min_year_diff = null;
            $nearest_year_dep = null;
            $in_year_range = false;
            foreach($years_depreciation as $key => $value){
                $options_parts = explode('-', $value['config_value']);

                $option_one = (int)date('Y') - (int)$options_parts[0];
                $option_two = (int)date('Y') - (int)$options_parts[1];

                if($key == 0){
                    $diff1 = abs($year_diff - $option_one);
                    $diff2 = abs($year_diff - $option_two);

                    $min_year_diff = $diff1 < $diff2 ? $diff1 : $diff2; 

                    $nearest_year_dep = $value['depreciation'];

                }else{

                    $diff1 = abs($year_diff - $option_one);
                    $diff2 = abs($year_diff - $option_two);

                    $new_min_year_diff = $diff1 < $diff2 ? $diff1 : $diff2; 

                    if($min_year_diff > $new_min_year_diff){
                        $min_year_diff = $new_min_year_diff;
                        $nearest_year_dep = $value['depreciation'];
                    }
                }

                if ($year_diff <= $option_one && $year_diff >= $option_two){
                    $depreciation_percent = $depreciation_percent + ($value['depreciation']*$depre_year_diff);
                    $in_year_range = true;
                    break;
                }
            }

            if($in_year_range == false){
                $depreciation_percent = $depreciation_percent + $nearest_year_dep*$depre_year_diff;
            }
        }


        $mileage = (int)$data['mileage'];

        $mileage_diff = null;
        $nearest_mileage_dep = null;
        $in_mileage_range = false;

        foreach ($mileages_depreciation as $key => $value) {
            $options_parts = explode('-', $value['config_value']);

            if($key == 0){

                $diff1 = abs($mileage - (int)$options_parts[0]);
                $diff2 = abs($mileage - (int)$options_parts[1]);

                $mileage_diff = $diff1 < $diff2 ? $diff1 : $diff2; 

                $nearest_mileage_dep = $value['depreciation'];
            }else{
                $diff1 = abs($mileage - (int)$options_parts[0]);
                $diff2 = abs($mileage - (int)$options_parts[1]);

                $new_mileage_diff = $diff1 < $diff2 ? $diff1 : $diff2; 

                if($mileage_diff >= $new_mileage_diff){

                    $mileage_diff = $new_mileage_diff;

                    $nearest_mileage_dep = $value['depreciation'];
                }
            }

            if ($mileage>(int)$options_parts[0] && $mileage<(int)$options_parts[1]){
                $depreciation_percent = $depreciation_percent + $value['depreciation'];
                $in_mileage_range = true;
                break;
            }
        }

        if($in_mileage_range == false){
            $depreciation_percent = $depreciation_percent + $nearest_mileage_dep;
        }

        $option = $data['option'];
        $options_depreciation = EvaluationConfigModel::where('slug','option')->where('config_value',$option)->first();

        if($options_depreciation){
            $depreciation_percent = $depreciation_percent + $options_depreciation->depreciation;
        }

        $paint = $data['paint'];
        $paints_depreciation = EvaluationConfigModel::where('slug','paint')->where('config_value',$paint)->first();


        if($paints_depreciation){
            $depreciation_percent = $depreciation_percent + $paints_depreciation->depreciation;
        }

        $specs = $data['specs'];
        $specs_depreciation = EvaluationConfigModel::where('slug','specs')->where('config_value',$specs)->first();

        if($specs_depreciation){
            $depreciation_percent = $depreciation_percent + $specs_depreciation->depreciation;
        }

        if($depreciation_percent >= 80){
            $depreciation_percent = 80;
        }

        if($price_option->price != '')
        {
            $price = (int)$price_option->price - $depreciation_percent / 100 * (int)$price_option->price;
            return [
                'status' => 1,
                'price' => $price
            ];
        }

        return $return_option;
    }
}
