<?php namespace Skrollx\CarsModule\Car;

use Anomaly\FilesModule\Folder\FolderModel;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Skrollx\CarsModule\Car\Contract\CarRepositoryInterface;
use Skrollx\CarsModule\Make\MakeModel;
use Skrollx\CarsModule\Model\ModelModel;

class CarSeeder extends Seeder
{
    /**
     * The category repository.
     *
     * @var RoleRepositoryInterface
     */
    protected $cars;

    /**
     * Create a new RoleSeeder instance.
     *
     * @param RoleRepositoryInterface $cars
     */
    public function __construct(CarRepositoryInterface $cars)
    {
        $this->cars = $cars;
    }
    
    /**
     * Run the seeder.
     */
    public function run()
    {
        $this->cars->truncate();
        $faker = \Faker\Factory::create();

        $file_ids = FolderModel::where('slug', 'cars')->first()->files()->pluck('id')->toArray();
        $carMakes = MakeModel::pluck('title', 'id')->toArray();
        shuffle($file_ids);
        
        $limit = 30;
        for ($i = 0; $i < $limit; $i++) 
        {
            $make_id = $faker->numberBetween(1,6);
            $model = ModelModel::where('make_id', $make_id)->inRandomOrder()->first();
            $year = $faker->numberBetween(2000,2017);
            $car_name = $carMakes[array_rand($carMakes, 1)] . ' - ' . $year;

            $data = [
                'title'             => $car_name,
                'slug'              => str_slug($car_name.' '.rand()),
                'image_id'          => count($file_ids) ? $file_ids[array_rand($file_ids, 1)] : null,
                'make_id'           => $make_id,
                'model_id'          => $model ? $model->id : null,
                'transmission'      => $faker->numberBetween(1,2),
                'year'              => $year,
                'mileage'           => $faker->numberBetween(1,16),
                'option'            => $faker->numberBetween(1,4),
                'specs'             => $faker->numberBetween(1,3),
                'cylinders'         => $faker->numberBetween(2,14),
                'odometer_reading'  => $faker->numberBetween(10000,100000),
                'paint'             => $faker->numberBetween(1,3),
                'history_report'    => $faker->numberBetween(1,3),
                'accident_history'  => $faker->numberBetween(0,1),
                'service_history'   => $faker->numberBetween(0,1),
                'body_type'         => $faker->numberBetween(1,12),
                'drive'             => $faker->numberBetween(1,2),
                'color'             => $faker->numberBetween(1,9),
                'fuel_type'         => $faker->numberBetween(1,5),
                'car_number'        => $faker->numberBetween(1000,30000),
                'engine_size'       => $faker->numberBetween(1,10),
                'chassis_damage'    => $faker->numberBetween(0,1),
                'chassis_repaired'  => $faker->numberBetween(0,1),
                'registered_in'     => 'AE',
                'bank_loan'         => $faker->numberBetween(0,1),
                'navigation_system' => $faker->numberBetween(0,1),
                'no_keys'           => $faker->numberBetween(1,5),
                'roof'              => $faker->numberBetween(1,3),
                'rim_type'          => $faker->numberBetween(1,2),
                'rim_condition'     => $faker->numberBetween(1,4),
                'seat_color'        => $faker->numberBetween(1,9),
                'price'             => $faker->numberBetween(3000,5000),
                'evaluation_price'  => $faker->numberBetween(3000,5000),
                'seller_price'      => $faker->numberBetween(3000,5000),
                'min_bid_amount'    => $faker->numberBetween(3000,5000),
                'max_bid_amount'    => $faker->numberBetween(5000,12000),
                'description'       => $faker->paragraph(10, true),
                'expire_date'       => \Carbon\Carbon::now()->addMonths(2),
                'status'            => $faker->numberBetween(1, 4),
                'location'          => $faker->numberBetween(1, 8),
                'first_name'        => $faker->firstName,
                'last_name'         => $faker->lastName,
                'mobile'            => '1234567890',
                'alt_contact'       => '1234567890',
                'email'             => $faker->email,
                'date'              => \Carbon\Carbon::now()->addDays($faker->numberBetween(5,30))
            ];
            $car = $this->cars->create($data);
            $car->images()->sync(array_only($file_ids, [0,1,2]));
        }
    }
}
