<?php namespace Skrollx\CarsModule\Car\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface CarRepositoryInterface extends EntryRepositoryInterface
{

}
