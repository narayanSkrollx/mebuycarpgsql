<?php namespace Skrollx\CarsModule\Car\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class CarFormBuilder extends FormBuilder
{

    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [];

    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [];

    /**
     * The form actions.
     *
     * @var array|string
     */
    protected $actions = [];

    /**
     * The form buttons.
     *
     * @var array|string
     */
    protected $buttons = [];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The form sections.
     *
     * @var array
     */
    protected $sections = [];
    
    protected $rules = [
        'make' => [
            'required'
        ],
        'model' => [
            'required'
        ],
        'year' => [
            'required'
        ],
        'transmission' => [
            'required'
        ],
        'specs' => [
            'required'
        ],
        'cylinders' => [
            'required'
        ],
        'mileage' => [
            'required'
        ],
        'odometer_reading' => [
            'required'
        ],
        'option' => [
            'required'
        ],
        'paint' => [
            'required'
        ],
        'service_history' => [
            'required'
        ],
        'body_type' => [
            'required'
        ],
        'drive' => [
            'required'
        ],
        'color' => [
            'required'
        ],
        'fuel_type' => [
            'required'
        ],
        'registered_in' => [
            'required'
        ],
        'expire_date' => [
            'required'
        ],
        'status' => [
            'required'
        ],
        'first_name' => [
            'required'
        ],
        'location' => [
            'required'
        ],
        'last_name' => [
            'required'
        ],
        'date' => [
            'required'
        ],
        'mobile' => [
            'required'
        ],
        'email' => [
            'required',
            'email'
        ]
    ];

    /**
     * The form assets.
     *
     * @var array
     */
    protected $assets = [
        'scripts.js' => [
            'skrollx.module.cars::js/form.js'
        ]
    ];

}
