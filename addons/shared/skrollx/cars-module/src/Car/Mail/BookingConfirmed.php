<?php namespace Skrollx\CarsModule\Car\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BookingConfirmed extends Mailable
{
    use SerializesModels;

    public $date = null;
    public $time = null;
    public $location = null;
    public $name = null;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->date = $data->date->format('Y/m/d');
        $this->time = $data->date->format('h:i A');
        $this->location = $data->location->name;
        $this->name = $data->first_name.' '.$data->last_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view(
                'skrollx.module.cars::emails.booking-confirmed', 
                [
                    'date' => $this->date, 
                    'time' => $this->time, 
                    'location' => $this->location, 
                    'name' => $this->name
                ]
            )->subject(trans('skrollx.module.cars::notification.booking-confirmed.subject', 
                [
                    'name' => $this->name
                ]
            ));
    }
}
