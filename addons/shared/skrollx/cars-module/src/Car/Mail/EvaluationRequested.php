<?php namespace Skrollx\CarsModule\Car\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EvaluationRequested extends Mailable
{
    use SerializesModels;

    public $car = null;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($car)
    {
        $this->car = $car;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view(
                'skrollx.module.cars::emails.evaluation-requested', 
                [
                    'car' => $this->car, 
                ]
            )->subject(trans('skrollx.module.cars::notification.evaluation_requested.subject', 
                [
                    'name' => $this->car->first_name.' '.$this->car->last_name
                ]
            ));
    }
}
