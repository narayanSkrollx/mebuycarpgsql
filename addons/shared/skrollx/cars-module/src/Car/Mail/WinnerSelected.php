<?php namespace Skrollx\CarsModule\Car\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WinnerSelected extends Mailable
{
    use SerializesModels;

    public $bidding = null;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($bidding)
    {
        $this->bidding = $bidding;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view(
                'skrollx.module.cars::emails.winner-selected', 
                [
                    'bidding' => $this->bidding, 
                ]
            )->subject(trans('skrollx.module.cars::notification.winner_selected.subject', 
                [
                    'name' => $this->bidding->car->title
                ]
            ));
    }
}
