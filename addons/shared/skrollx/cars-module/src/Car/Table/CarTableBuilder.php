<?php namespace Skrollx\CarsModule\Car\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class CarTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        'title',
        'email',
        'mobile',
        'location',
        'make',
        'status',
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'title' => [
            'heading'     => 'skrollx.module.cars::field.title.name',
            'sort_column' => 'name',
            'wrapper'     => '{value.title}-{value.id}',
            'value'       => [
                'title'     => 'entry.title',
                'id'    => 'entry.id'
            ]
        ],
        'make',
        'model',
        'entry.status.value',
        'name' => [
            'heading'     => 'skrollx.module.cars::field.requested_by.name',
            'sort_column' => 'name',
            'wrapper'     => '{value.first_name} {value.last_name} <br/><small>{value.email}</small>',
            'value'       => [
                'first_name'     => 'entry.first_name',
                'last_name'     => 'entry.last_name',
                'email'     => 'entry.email',
            ]
        ],
        'entry.date',
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit',
        'view',
        'biddings' => [
            'href'   => 'admin/cars/biddings/{entry.id}',
        ]
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete',
        'export'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [
        'order_by' => ['id' => 'DESC']
    ];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}
