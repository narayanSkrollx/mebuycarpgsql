<?php namespace Skrollx\CarsModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class CarsModule extends Module
{

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-car';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'cars' => [
            'buttons' => [
                'new_car' => [
                    'enabled' => 'admin/cars'
                ]
            ],
            'sections' => [
                'biddings' => [
                    'href'        => 'admin/cars/biddings'
                ],
                'vehicle_detail' => [
                    'buttons' => [
                        'new_vehicle_detail' => [
                            'enabled' => 'admin/cars/vehicle_detail'
                        ]
                    ]
                ],
                'vehicle_specs' => [
                    'buttons' => [
                        'new_vechile_specs' => [
                            'enabled' => 'admin/cars/vehicle_specs'
                        ]
                    ]
                ],
                'vehicle_general' => [
                    'buttons' => [
                        'new_general' => [
                            'enabled' => 'admin/cars/vehicle_general'
                        ]
                    ]
                ],
                'vehicle_body_report' => [
                    'buttons' => [
                        'new_car_body_report' => [
                            'enabled' => 'admin/cars/vehicle_body_report'
                        ]
                    ]
                ],
                'engine_condition' => [
                    'buttons' => [
                        'new_engine_condition' => [
                            'enabled' => 'admin/cars/engine_condition'
                        ]
                    ]
                ],
                'engine_noise' => [
                    'buttons' => [
                        'new_engine_noises' => [
                            'enabled' => 'admin/cars/engine_noise'
                        ]
                    ]
                ],
                'gearbox_condition' => [
                    'buttons' => [
                        'new_gear_box_condition' => [
                            'enabled' => 'admin/cars/gearbox_condition'
                        ]
                    ]
                ],
                'ac_condition' => [
                    'buttons' => [
                        'new_ac_condition' => [
                            'enabled' => 'admin/cars/ac_condition'
                        ]
                    ]
                ],
                'accessory_parts' => [
                    'buttons' => [
                        'new_sensor_glass_mirror' => [
                            'enabled' => 'admin/cars/accessory_parts'
                        ]
                    ]
                ],
                'fluids' => [
                    'buttons' => [
                        'new_fluids' => [
                            'enabled' => 'admin/cars/fluids'
                        ]
                    ]
                ],
                'testdrive_general' => [
                    'buttons' => [
                        'new_test_drive_general' => [
                            'enabled' => 'admin/cars/testdrive_general'
                        ]
                    ]
                ],
                'electrical_system' => [
                    'buttons' => [
                        'new_electrical_system' => [
                            'enabled' => 'admin/cars/electrical_system'
                        ]
                    ]
                ],
                'tyres_wheels' => [
                    'buttons' => [
                        'new_tyres_wheels' => [
                            'enabled' => 'admin/cars/tyres_wheels'
                        ]
                    ]
                ],
                'interiors' => [
                    'buttons' => [
                        'new_interiors' => [
                            'enabled' => 'admin/cars/interiors'
                        ]
                    ]
                ],
                'brakes' => [
                    'buttons' => [
                        'new_brakes' => [
                            'enabled' => 'admin/cars/brakes'
                        ]
                    ]
                ],
            ]
        ],
        'makes' => [
            'buttons' => [
                'new_make' => [
                    'enabled' => 'admin/cars/makes'
                ]
            ]
        ],
        'models' => [
            'buttons' => [
                'new_model'
            ]
        ],
        'locations' => [
            'buttons' => [
                'new_location',
            ],
        ],
        'dates' => [
            'buttons' => [
                'new_date',
            ],
        ],
        'engine_sizes' => [
            'buttons' => [
                'new_engine_size',
            ],
        ],
        'prices' => [
            'buttons' => [
                'new_price'
            ]
        ],
        'evaluation_configure'
    ];
}
