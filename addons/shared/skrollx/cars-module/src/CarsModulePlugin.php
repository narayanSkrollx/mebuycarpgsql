<?php namespace Skrollx\CarsModule;

use Anomaly\Streams\Platform\Addon\Plugin\Plugin;
use Anomaly\Streams\Platform\Support\Decorator;

/**
 * Class CarsModulePlugin
 *
 * @link          http://skrollx.com/
 * @author        SkrollX <info@skrollx.com>
 * @author        Kamal <kamal@skrollx.com>
 */
class CarsModulePlugin extends Plugin
{

    /**
     * Get the functions.
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'format_url',
                function ($params, $url = null) {
                    $current_url = \Request::url();
                    if($url){
                        $current_url = $url;
                    }
                    $request_params = array_merge((array)\Request::all(), (array)$params);
                    if(isset($request_params['_token'])){
                        unset($request_params['_token']);
                    }
                    return $current_url.'?'.http_build_query(array_filter($request_params));
                }
            ),
            new \Twig_SimpleFunction(
                'filter_error_messags',
                function ($errors) {
                    if($errors && count($errors->all()) > 0){
                        $msg = [];
                        foreach($errors->all() as $error){
                            $parts = explode(' ', str_replace('.', '', $error));
                            if(!in_array('required', $parts)){
                                $msg[] = $error;
                            }
                        }
                        return $msg;
                    }
                    return $errors ? $errors->all() : null;
                }
            ),
            new \Twig_SimpleFunction(
                'unserialize',
                function ($str) {
                    return unserialize($str);
                }
            ),
            new \Twig_SimpleFunction(
                'counter_show_and_count',
                function ($page, $id = null) {
                    return \Counter::showAndCount($page, $id);
                }
            ),
            new \Twig_SimpleFunction(
                'counter_show',
                function ($page, $id = null) {
                    return \Counter::show($page, $id);
                }
            ),
            new \Twig_SimpleFunction(
                'counter_all',
                function () {
                    return \Counter::allHits();
                }
            ),
            new \Twig_SimpleFunction(
                'counter_online',
                function ($mins = null) {
                    $prefix = config('database.connections.' . config('database.default') . '.prefix');
                    $table = $prefix . 'kryptonit3_counter_page_visitor';
                    if ($mins) {
                        $hits = \DB::table($table)
                            ->where('created_at', '>=', \Carbon\Carbon::now()->subMinutes($mins))->count();
                    } else {
                        $hits = \DB::table($table)->count();
                    }
                    \DB::table($table)->where('created_at', '<=', \Carbon\Carbon::now()->subHours(1))->delete();

                    return number_format($hits);
                }
            ),
            new \Twig_SimpleFunction(
                'is_expired',
                function($expiry_date){
                    $expiry_date = strtotime($expiry_date);
                    $current_date = strtotime(date('Y-m-d H:i'));

                    if($current_date >= $expiry_date){
                        return true;
                    }else{
                        return false;
                    }
                }
            )
        ];
    }
}
