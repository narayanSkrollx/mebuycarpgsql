<?php namespace Skrollx\CarsModule;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Skrollx\CarsModule\Car\CarSeeder;
use Skrollx\CarsModule\EngineSize\EngineSizeSeeder;
use Skrollx\CarsModule\Location\LocationSeeder;
use Skrollx\CarsModule\Make\MakeSeeder;
use Skrollx\CarsModule\Model\ModelSeeder;
use Skrollx\CarsModule\Seeder\FolderSeeder;

class CarsModuleSeeder extends Seeder
{
    
    public function run()
    {
        // $this->call(FolderSeeder::class);
        // $this->call(LocationSeeder::class);
        // $this->call(EngineSizeSeeder::class);
        // $this->call(MakeSeeder::class);
        // $this->call(CarSeeder::class);
    }
}
