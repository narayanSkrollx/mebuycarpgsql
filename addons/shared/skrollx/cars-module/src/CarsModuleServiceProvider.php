<?php namespace Skrollx\CarsModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Skrollx\CarsModule\EngineSize\Contract\EngineSizeRepositoryInterface;
use Skrollx\CarsModule\EngineSize\EngineSizeRepository;
use Anomaly\Streams\Platform\Model\Cars\CarsEngineSizesEntryModel;
use Skrollx\CarsModule\EngineSize\EngineSizeModel;
use Skrollx\CarsModule\Date\Contract\DateRepositoryInterface;
use Skrollx\CarsModule\Date\DateRepository;
use Anomaly\Streams\Platform\Model\Cars\CarsDatesEntryModel;
use Skrollx\CarsModule\Date\DateModel;
use Anomaly\Streams\Platform\Model\Cars\CarsLocationsEntryModel;
use Skrollx\CarsModule\Location\Contract\LocationRepositoryInterface;
use Skrollx\CarsModule\Location\LocationModel;
use Skrollx\CarsModule\Location\LocationRepository;
use Skrollx\CarsModule\Price\Contract\PriceRepositoryInterface;
use Skrollx\CarsModule\EvaluationConfig\Contract\EvaluationConfigRepositoryInterface;
use Skrollx\CarsModule\EvaluationConfig\EvaluationConfigRepository;
use Skrollx\CarsModule\Price\PriceRepository;

class CarsModuleServiceProvider extends AddonServiceProvider
{

    protected $plugins = [
        'Skrollx\CarsModule\CarsModulePlugin'
    ];

    protected $commands = [];

    protected $schedules = [];

    protected $api = [];

    protected $routes = [
        'admin/cars/engine_sizes'           => 'Skrollx\CarsModule\Http\Controller\Admin\EngineSizesController@index',
        'admin/cars/engine_sizes/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\EngineSizesController@create',
        'admin/cars/engine_sizes/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\EngineSizesController@edit',
        'admin/cars/dates'           => 'Skrollx\CarsModule\Http\Controller\Admin\DatesController@index',

        'admin/cars/engine_cylinders'           => 'Skrollx\CarsModule\Http\Controller\Admin\EngineCylindersController@index',
        'admin/cars/engine_cylinders/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\EngineCylindersController@create',
        'admin/cars/engine_cylinders/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\EngineCylindersController@edit',

        'admin/cars/dates/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\DatesController@create',
        'admin/cars/dates/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\DatesController@edit',
        'admin/cars/locations'           => 'Skrollx\CarsModule\Http\Controller\Admin\LocationsController@index',
        'admin/cars/locations/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\LocationsController@create',
        'admin/cars/locations/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\LocationsController@edit',

        'admin/cars/vehicle_detail'           => 'Skrollx\CarsModule\Http\Controller\Admin\VehicleDetailController@index',
        'admin/cars/vehicle_detail/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\VehicleDetailController@create',
        'admin/cars/vehicle_detail/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\VehicleDetailController@edit',
        
        'admin/cars/vehicle_specs'           => 'Skrollx\CarsModule\Http\Controller\Admin\VehicleSpecsController@index',
        'admin/cars/vehicle_specs/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\VehicleSpecsController@create',
        'admin/cars/vehicle_specs/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\VehicleSpecsController@edit',
        
        'admin/cars/vehicle_general'           => 'Skrollx\CarsModule\Http\Controller\Admin\VehicleGeneralController@index',
        'admin/cars/vehicle_general/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\VehicleGeneralController@create',
        'admin/cars/vehicle_general/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\VehicleGeneralController@edit',
        
        'admin/cars/vehicle_body_report'           => 'Skrollx\CarsModule\Http\Controller\Admin\VehicleBodyReportController@index',
        'admin/cars/vehicle_body_report/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\VehicleBodyReportController@create',
        'admin/cars/vehicle_body_report/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\VehicleBodyReportController@edit',
        
        'admin/cars/engine_condition'           => 'Skrollx\CarsModule\Http\Controller\Admin\EngineConditionController@index',
        'admin/cars/engine_condition/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\EngineConditionController@create',
        'admin/cars/engine_condition/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\EngineConditionController@edit',
        
        'admin/cars/engine_noise'           => 'Skrollx\CarsModule\Http\Controller\Admin\EngineNoiseController@index',
        'admin/cars/engine_noise/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\EngineNoiseController@create',
        'admin/cars/engine_noise/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\EngineNoiseController@edit',
        
        'admin/cars/gearbox_condition'           => 'Skrollx\CarsModule\Http\Controller\Admin\GearboxConditionController@index',
        'admin/cars/gearbox_condition/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\GearboxConditionController@create',
        'admin/cars/gearbox_condition/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\GearboxConditionController@edit',
        
        'admin/cars/ac_condition'           => 'Skrollx\CarsModule\Http\Controller\Admin\AcConditionController@index',
        'admin/cars/ac_condition/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\AcConditionController@create',
        'admin/cars/ac_condition/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\AcConditionController@edit',
        
        'admin/cars/fluids'           => 'Skrollx\CarsModule\Http\Controller\Admin\FluidsController@index',
        'admin/cars/fluids/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\FluidsController@create',
        'admin/cars/fluids/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\FluidsController@edit',
        
        'admin/cars/testdrive_general'           => 'Skrollx\CarsModule\Http\Controller\Admin\TestdriveGeneralController@index',
        'admin/cars/testdrive_general/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\TestdriveGeneralController@create',
        'admin/cars/testdrive_general/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\TestdriveGeneralController@edit',
        
        'admin/cars/electrical_system'           => 'Skrollx\CarsModule\Http\Controller\Admin\ElectricalSystemController@index',
        'admin/cars/electrical_system/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\ElectricalSystemController@create',
        'admin/cars/electrical_system/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\ElectricalSystemController@edit',
        
        'admin/cars/tyres_wheels'           => 'Skrollx\CarsModule\Http\Controller\Admin\TyresWheelsController@index',
        'admin/cars/tyres_wheels/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\TyresWheelsController@create',
        'admin/cars/tyres_wheels/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\TyresWheelsController@edit',
        
        'admin/cars/brakes'           => 'Skrollx\CarsModule\Http\Controller\Admin\BrakesController@index',
        'admin/cars/brakes/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\BrakesController@create',
        'admin/cars/brakes/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\BrakesController@edit',
        
        'admin/cars/interiors'           => 'Skrollx\CarsModule\Http\Controller\Admin\InteriorsController@index',
        'admin/cars/interiors/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\InteriorsController@create',
        'admin/cars/interiors/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\InteriorsController@edit',
        
        'admin/cars/accessory_parts'           => 'Skrollx\CarsModule\Http\Controller\Admin\AccessoryPartsController@index',
        'admin/cars/accessory_parts/create'    => 'Skrollx\CarsModule\Http\Controller\Admin\AccessoryPartsController@create',
        'admin/cars/accessory_parts/edit/{id}' => 'Skrollx\CarsModule\Http\Controller\Admin\AccessoryPartsController@edit',
    ];

    protected $middleware = [];

    protected $routeMiddleware = [];

    protected $listeners = [];

    protected $aliases = [];

    protected $bindings = [
        CarsEngineSizesEntryModel::class => EngineSizeModel::class,
        CarsDatesEntryModel::class => DateModel::class,
        CarsLocationsEntryModel::class => LocationModel::class,
        PriceRepositoryInterface::class => PriceRepository::class,
        EvaluationConfigRepositoryInterface::class => EvaluationConfigRepository::class,
    ];

    protected $providers = [];

    protected $singletons = [
        EngineSizeRepositoryInterface::class => EngineSizeRepository::class,
        DateRepositoryInterface::class => DateRepository::class,
        LocationRepositoryInterface::class => LocationRepository::class,
        'Skrollx\CarsModule\Car\Contract\CarRepositoryInterface' => 'Skrollx\CarsModule\Car\CarRepository',
        'Skrollx\CarsModule\Make\Contract\MakeRepositoryInterface' => 'Skrollx\CarsModule\Make\MakeRepository',
        'Skrollx\CarsModule\Model\Contract\ModelRepositoryInterface' => 'Skrollx\CarsModule\Model\ModelRepository',
    ];

    protected $overrides = [];

    protected $mobile = [];

    public function register()
    {
    }

    public function map()
    {
    }

}
