<?php namespace Skrollx\CarsModule\Date\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface DateRepositoryInterface extends EntryRepositoryInterface
{

}
