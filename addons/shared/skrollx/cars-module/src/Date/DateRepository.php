<?php namespace Skrollx\CarsModule\Date;

use Skrollx\CarsModule\Date\Contract\DateRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class DateRepository extends EntryRepository implements DateRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var DateModel
     */
    protected $model;

    /**
     * Create a new DateRepository instance.
     *
     * @param DateModel $model
     */
    public function __construct(DateModel $model)
    {
        $this->model = $model;
    }
}
