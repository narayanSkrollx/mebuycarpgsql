<?php namespace Skrollx\CarsModule\Date\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class DateFormBuilder extends FormBuilder
{

    protected $handler = 'Skrollx\CarsModule\Date\Handler\DateFormHandler@handle';

    /**

     * The form model.

     *

     * @var string

     */

    protected $model = 'Skrollx\CarsModule\Date\DateModel';


    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [
        'location'=> [
            'required' => true
        ],
        'from_date' => [
            'required' => true,
            'label' => 'module::field.from_date.name',
            'type'   => 'anomaly.field_type.datetime',
            'config' => [
                'mode'          => 'date',
                'date_format'   => 'Y-m-d',
                'timezone'      => null,
                'step'          => 15
            ]
        ],
        'to_date' => [
            'required' => true,
            'label' => 'module::field.to_date.name',
            'type'   => 'anomaly.field_type.datetime',
            'config' => [
                'mode'          => 'date',
                'date_format'   => 'Y-m-d',
                'timezone'      => null,
                'step'          => 15,
            ]
        ],
        "times" => [
            'label' => 'module::field.times.name',
            "type"   => "anomaly.field_type.checkboxes",
            "config" => [
                "options"=> [

                            "8:00 AM" => "8:00 AM",
                            "8:30 AM" => "8:30 AM",
                            "9:00 AM" => "9:00 AM",
                            "9:30 AM" => "9:30 AM",
                            "10:00 AM" => "10:00 AM",
                            "10:30 AM" => "10:30 AM",
                            "11:00 AM" => "11:00 AM",
                            "11:30 AM" => "11:30 AM",
                            "12:00 AM" => "12:00 AM",
                            "12:30 PM" => "12:30 PM",
                            "1:00 PM" => "1:00 PM",
                            "1:30 PM" => "1:30 PM",
                            "2:00 PM" => "2:00 PM",
                            "2:30 PM" => "2:30 PM",
                            "3:00 PM" => "3:00 PM",
                            "3:30 PM" => "3:30 PM",
                            "4:00 PM" => "4:00 PM",
                            "4:30 PM" => "4:30 PM",
                            "5:00 PM" => "5:00 PM",
                            "5:30 PM" => "5:30 PM",
                            "6:00 PM" => "6:00 PM",
                            "6:30 PM" => "6:30 PM",
                            "7:00 PM" => "7:00 PM",
                            "7:30 PM" => "7:30 PM",
                            "8:00 PM" => "8:00 PM",
                            "8:30 PM" => "8:30 PM",
                            "9:00 PM" => "9:00 PM",
                            "10:00 PM" => "10:00 PM",
                            "11:00 PM" => "11:00 PM",
                            "12:00 PM" => "12:00 PM",
                            "1:00 AM" => "1:00 AM",
                            "2:00 AM" => "2:00 AM",
                            ],
                "mode"  => "checkboxes",
            ]
        ]
    ];

    /**
     * Additional validation rules.
     *
     * @var array|string
     */
    protected $rules = [];

    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [];

    /**
     * The form actions.
     *
     * @var array|string
     */
    protected $actions = [];

    /**
     * The form buttons.
     *
     * @var array|string
     */
    protected $buttons = [];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The form sections.
     *
     * @var array
     */
    protected $sections = [];

    /**
     * The form assets.
     *
     * @var array
     */
    protected $assets = [];

    public function onSaving(){
    }

}
