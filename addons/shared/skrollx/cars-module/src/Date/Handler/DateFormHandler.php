<?php namespace Skrollx\CarsModule\Date\Handler;

use Skrollx\CarsModule\Date\Form\DateFormBuilder;
use Skrollx\CarsModule\Date\DateModel;

class DateFormHandler
{

    /**
     * Handle the form.
     *
     * @param  Repository          $config
     * @param  RegisterFormBuilder $builder
     * @param  UserActivator       $activator
     * @throws \Exception
     */
    public function handle(DateFormBuilder $builder)
    {
        $location = $builder->getFormValue('location');
        $from_date = $builder->getFormValue('from_date');
        $to_date = $builder->getFormValue('to_date');
        $times = $builder->getFormValue('times');

        while (strtotime($from_date) <= strtotime($to_date)) {
            foreach ($times as $key => $value) {
                $date = date('Y-m-d h:i A', strtotime($from_date.$value));

                $date_exist = DateModel::where('location_id',$location)->where('date','=',$date)->first();

                if(!$date_exist){
                    $newDate = new DateModel([
                            'location' => $location,
                            'date' => $date,
                            'reserved' => false
                        ]);
                    $newDate->save();
                }

            }
            $from_date = date ("Y-m-d", strtotime("+1 day", strtotime($from_date)));
        }
    }
}
