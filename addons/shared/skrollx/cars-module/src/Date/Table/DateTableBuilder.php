<?php namespace Skrollx\CarsModule\Date\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class DateTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        'date',
        'location'
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'location',
        'date',
        'booking_count' => [
            'value' => 'entry.booking_count == "" ? 0 : entry.booking_count'
        ],
        'reserved' => [
            'value' => 'entry.reserved.label'
        ]
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

    public function onQuerying($query)
    {
        $query->orderBy('date', 'DESC');
    }

}
