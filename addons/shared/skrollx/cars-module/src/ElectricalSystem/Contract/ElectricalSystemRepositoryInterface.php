<?php namespace Skrollx\CarsModule\ElectricalSystem\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ElectricalSystemRepositoryInterface extends EntryRepositoryInterface
{

}
