<?php namespace Skrollx\CarsModule\ElectricalSystem;

use Skrollx\CarsModule\ElectricalSystem\Contract\ElectricalSystemRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class ElectricalSystemRepository extends EntryRepository implements ElectricalSystemRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ElectricalSystemModel
     */
    protected $model;

    /**
     * Create a new ElectricalSystemRepository instance.
     *
     * @param ElectricalSystemModel $model
     */
    public function __construct(ElectricalSystemModel $model)
    {
        $this->model = $model;
    }
}
