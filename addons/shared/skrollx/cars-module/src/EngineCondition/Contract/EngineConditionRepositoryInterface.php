<?php namespace Skrollx\CarsModule\EngineCondition\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface EngineConditionRepositoryInterface extends EntryRepositoryInterface
{

}
