<?php namespace Skrollx\CarsModule\EngineCondition;

use Skrollx\CarsModule\EngineCondition\Contract\EngineConditionInterface;
use Anomaly\Streams\Platform\Model\Cars\CarsEngineConditionEntryModel;

class EngineConditionModel extends CarsEngineConditionEntryModel implements EngineConditionInterface
{

}
