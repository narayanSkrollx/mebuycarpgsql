<?php namespace Skrollx\CarsModule\EngineCondition;

use Skrollx\CarsModule\EngineCondition\Contract\EngineConditionRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class EngineConditionRepository extends EntryRepository implements EngineConditionRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var EngineConditionModel
     */
    protected $model;

    /**
     * Create a new EngineConditionRepository instance.
     *
     * @param EngineConditionModel $model
     */
    public function __construct(EngineConditionModel $model)
    {
        $this->model = $model;
    }
}
