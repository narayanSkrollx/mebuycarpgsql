<?php namespace Skrollx\CarsModule\EngineCylinder\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface EngineCylinderRepositoryInterface extends EntryRepositoryInterface
{

}
