<?php namespace Skrollx\CarsModule\EngineCylinder;

use Skrollx\CarsModule\EngineCylinder\Contract\EngineCylinderRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class EngineCylinderRepository extends EntryRepository implements EngineCylinderRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var EngineCylinderModel
     */
    protected $model;

    /**
     * Create a new EngineCylinderRepository instance.
     *
     * @param EngineCylinderModel $model
     */
    public function __construct(EngineCylinderModel $model)
    {
        $this->model = $model;
    }
}
