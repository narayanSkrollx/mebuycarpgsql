<?php namespace Skrollx\CarsModule\EngineNoise\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface EngineNoiseRepositoryInterface extends EntryRepositoryInterface
{

}
