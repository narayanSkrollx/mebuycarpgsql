<?php namespace Skrollx\CarsModule\EngineNoise;

use Skrollx\CarsModule\EngineNoise\Contract\EngineNoiseRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class EngineNoiseRepository extends EntryRepository implements EngineNoiseRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var EngineNoiseModel
     */
    protected $model;

    /**
     * Create a new EngineNoiseRepository instance.
     *
     * @param EngineNoiseModel $model
     */
    public function __construct(EngineNoiseModel $model)
    {
        $this->model = $model;
    }
}
