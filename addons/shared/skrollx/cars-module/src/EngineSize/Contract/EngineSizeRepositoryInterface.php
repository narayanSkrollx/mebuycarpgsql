<?php namespace Skrollx\CarsModule\EngineSize\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface EngineSizeRepositoryInterface extends EntryRepositoryInterface
{

}
