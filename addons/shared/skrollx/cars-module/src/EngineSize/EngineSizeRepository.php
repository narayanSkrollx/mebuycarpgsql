<?php namespace Skrollx\CarsModule\EngineSize;

use Skrollx\CarsModule\EngineSize\Contract\EngineSizeRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class EngineSizeRepository extends EntryRepository implements EngineSizeRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var EngineSizeModel
     */
    protected $model;

    /**
     * Create a new EngineSizeRepository instance.
     *
     * @param EngineSizeModel $model
     */
    public function __construct(EngineSizeModel $model)
    {
        $this->model = $model;
    }
}
