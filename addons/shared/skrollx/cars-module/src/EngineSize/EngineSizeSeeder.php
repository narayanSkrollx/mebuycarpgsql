<?php namespace Skrollx\CarsModule\EngineSize;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Skrollx\CarsModule\EngineSize\Contract\EngineSizeRepositoryInterface;

class EngineSizeSeeder extends Seeder
{

    protected $engine_sizes;
    /**
     * Run the seeder.
     */
    
    public function __construct(EngineSizeRepositoryInterface $engine_sizes)
    {
        $this->engine_sizes = $engine_sizes;
    }

    public function run()
    {
        $this->engine_sizes->truncate();

        $engine_sizes = [
            '1.0L',
            '1.2L',
            '1.3L',
            '1.4L',
            '1.6L',
            '2.0L',
            '2.2L',
            '2.4L',
            '2.6L',
            '2.8L',
            '3.0L',
            '3.2L',
            '3.6L',
            '3.8L',
            '4.0L',
        ];
        foreach($engine_sizes as $size)
        {
            $make = $this->engine_sizes->create(
                [
                    'title'     => $size,
                    'live' => 1,
                ]
            );
        }
    }
}
