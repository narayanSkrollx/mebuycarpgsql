<?php namespace Skrollx\CarsModule\EngineSize\Handler;

use Anomaly\SelectFieldType\SelectFieldType;
use Illuminate\Config\Repository;
use Skrollx\CarsModule\EngineSize\EngineSizeModel;
use Skrollx\CarsModule\Model\ModelModel;

/**
 * Class ModelFieldHandler
 */
class EngineSizeFieldHandler
{

    /**
     * Handle the options.
     *
     * @param SelectFieldType $fieldType
     * @param Repository      $config
     */
    public function handle($fieldType, Repository $config)
    {
        $options = [];
        $engineSize = $fieldType->getValue();
        if(isset($engineSize->id)){
            $options[$engineSize->id] = $engineSize->title;
        }
        $fieldType->setOptions($options);
    }
}
