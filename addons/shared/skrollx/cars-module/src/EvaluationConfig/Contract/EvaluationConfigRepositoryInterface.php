<?php namespace Skrollx\CarsModule\EvaluationConfig\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface EvaluationConfigRepositoryInterface extends EntryRepositoryInterface
{

}
