<?php namespace Skrollx\CarsModule\EvaluationConfig;

use Skrollx\CarsModule\EvaluationConfig\Contract\EvaluationConfigRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class EvaluationConfigRepository extends EntryRepository implements EvaluationConfigRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var EvaluationConfigModel
     */
    protected $model;

    /**
     * Create a new EvaluationConfigRepository instance.
     *
     * @param EvaluationConfigModel $model
     */
    public function __construct(EvaluationConfigModel $model)
    {
        $this->model = $model;
    }
}
