<?php namespace Skrollx\CarsModule\Fluid\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface FluidRepositoryInterface extends EntryRepositoryInterface
{

}
