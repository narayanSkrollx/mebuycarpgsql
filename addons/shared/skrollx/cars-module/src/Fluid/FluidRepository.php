<?php namespace Skrollx\CarsModule\Fluid;

use Skrollx\CarsModule\Fluid\Contract\FluidRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class FluidRepository extends EntryRepository implements FluidRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var FluidModel
     */
    protected $model;

    /**
     * Create a new FluidRepository instance.
     *
     * @param FluidModel $model
     */
    public function __construct(FluidModel $model)
    {
        $this->model = $model;
    }
}
