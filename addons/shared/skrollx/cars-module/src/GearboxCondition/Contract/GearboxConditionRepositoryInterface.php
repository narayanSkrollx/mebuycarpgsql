<?php namespace Skrollx\CarsModule\GearboxCondition\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface GearboxConditionRepositoryInterface extends EntryRepositoryInterface
{

}
