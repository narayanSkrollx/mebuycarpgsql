<?php namespace Skrollx\CarsModule\GearboxCondition;

use Skrollx\CarsModule\GearboxCondition\Contract\GearboxConditionInterface;
use Anomaly\Streams\Platform\Model\Cars\CarsGearboxConditionEntryModel;

class GearboxConditionModel extends CarsGearboxConditionEntryModel implements GearboxConditionInterface
{

}
