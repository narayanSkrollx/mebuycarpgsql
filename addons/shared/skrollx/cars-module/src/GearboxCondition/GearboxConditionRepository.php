<?php namespace Skrollx\CarsModule\GearboxCondition;

use Skrollx\CarsModule\GearboxCondition\Contract\GearboxConditionRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class GearboxConditionRepository extends EntryRepository implements GearboxConditionRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var GearboxConditionModel
     */
    protected $model;

    /**
     * Create a new GearboxConditionRepository instance.
     *
     * @param GearboxConditionModel $model
     */
    public function __construct(GearboxConditionModel $model)
    {
        $this->model = $model;
    }
}
