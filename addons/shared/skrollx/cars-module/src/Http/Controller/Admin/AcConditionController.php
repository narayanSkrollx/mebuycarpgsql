<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\AcCondition\Form\AcConditionFormBuilder;
use Skrollx\CarsModule\AcCondition\Table\AcConditionTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class AcConditionController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param AcConditionTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(AcConditionTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param AcConditionFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(AcConditionFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param AcConditionFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(AcConditionFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
