<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\AccessoryPart\Form\AccessoryPartFormBuilder;
use Skrollx\CarsModule\AccessoryPart\Table\AccessoryPartTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class AccessoryPartsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param AccessoryPartTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(AccessoryPartTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param AccessoryPartFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(AccessoryPartFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param AccessoryPartFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(AccessoryPartFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
