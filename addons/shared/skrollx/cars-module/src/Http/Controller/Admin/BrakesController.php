<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\Brake\Form\BrakeFormBuilder;
use Skrollx\CarsModule\Brake\Table\BrakeTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class BrakesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param BrakeTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(BrakeTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param BrakeFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(BrakeFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param BrakeFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(BrakeFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
