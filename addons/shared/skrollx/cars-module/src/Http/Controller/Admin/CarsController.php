<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Skrollx\CarsModule\Car\CarModel;
use Skrollx\CarsModule\Car\Form\CarFormBuilder;
use Skrollx\CarsModule\Car\Mail\WinnerSelected;
use Skrollx\CarsModule\Car\Table\CarTableBuilder;
use Skrollx\DealersModule\Bidding\BiddingModel;
use Skrollx\CarsModule\Log\LogModel;

class CarsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param CarTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(CarTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param CarFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(CarFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param CarFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(CarFormBuilder $form, $id)
    {
        return $form->render($id);
    }

    /**
     * View an existing entry.
     *
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function view($id)
    {
        $car = CarModel::find($id);
        $this->breadcrumbs->add($car->title);
        return view('module::admin/view', compact('car'));
    }

    /**
     * View biddings of a car
     *
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function biddings($id = 0)
    {
        if(!$id){
            return redirect('admin/cars')->with('info', ['Please select car to see biddings.']);
        }
        $car = CarModel::find($id);
        $biddings = BiddingModel::where('car_id', $id)->get();
        $this->breadcrumbs->add($car->title);
        return view('module::admin/biddings', compact('car', 'biddings'));
    }

    public function selectWinner($id)
    {
        $bidding = BiddingModel::find($id);
        if(!$bidding){
            return redirect()->back();
        }

        $bidding->won = 1;
        $bidding->save();

        $car = $bidding->car;
        $car->status = 5;
        $car->save();

        $to_email = $bidding->dealer->user->email;
        \Mail::to($to_email)->send(new WinnerSelected($bidding));

        return redirect()->back()->with('success', [$bidding->dealer->company_name.' has been selected as winner.']);
    }

    public function adminAppointments()
    {
        return view('skrollx.module.cars::admin/appointments');
    }

    public function addLog($id)
    {
        $data = \Request::all();

        unset($data['_token']);

        $car = CarModel::find($data['car']);

        $data['title'] = 'Call log for '.$car->slug;

        $log = new LogModel($data);

        $log->save();

        return back()->with('success',['Log added success.']);
    }

    public function viewLog($id)
    {
        $logs = LogModel::where('car_id',$id)
                        ->join('users_users as user','user.id','cars_logs.created_by_id')
                        ->select('cars_logs.*','user.display_name','user.email')
                        ->get();

        echo json_encode($logs);
    }

    public function adminBiddings()
    {
        return view('anomaly.module.dashboard::admin/dashboards/biddings');
    }

    public function getYearRow()
    {
        $rowcount = \Request::input('count');
        $row_type = 'evaluation_year';

        $new_row = $this->view->make('skrollx.module.cars::admin/newpricerow',compact('rowcount','row_type'));
        return json_encode($new_row->render());
    }
}
