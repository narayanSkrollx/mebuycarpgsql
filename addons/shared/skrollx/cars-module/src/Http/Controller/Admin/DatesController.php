<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\Date\Form\DateFormBuilder;
use Skrollx\CarsModule\Date\Table\DateTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class DatesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param DateTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(DateTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param DateFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(DateFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param DateFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(DateFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
