<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\ElectricalSystem\Form\ElectricalSystemFormBuilder;
use Skrollx\CarsModule\ElectricalSystem\Table\ElectricalSystemTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class ElectricalSystemController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ElectricalSystemTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ElectricalSystemTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ElectricalSystemFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ElectricalSystemFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ElectricalSystemFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ElectricalSystemFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
