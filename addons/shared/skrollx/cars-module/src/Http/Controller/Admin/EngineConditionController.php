<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\EngineCondition\Form\EngineConditionFormBuilder;
use Skrollx\CarsModule\EngineCondition\Table\EngineConditionTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class EngineConditionController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param EngineConditionTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(EngineConditionTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param EngineConditionFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(EngineConditionFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param EngineConditionFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(EngineConditionFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
