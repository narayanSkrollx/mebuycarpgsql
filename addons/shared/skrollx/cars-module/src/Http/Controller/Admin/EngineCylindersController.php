<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\EngineCylinder\Form\EngineCylinderFormBuilder;
use Skrollx\CarsModule\EngineCylinder\Table\EngineCylinderTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class EngineCylindersController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param EngineCylinderTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(EngineCylinderTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param EngineCylinderFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(EngineCylinderFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param EngineCylinderFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(EngineCylinderFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
