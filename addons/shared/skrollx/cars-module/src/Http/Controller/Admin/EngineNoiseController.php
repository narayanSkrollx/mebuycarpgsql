<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\EngineNoise\Form\EngineNoiseFormBuilder;
use Skrollx\CarsModule\EngineNoise\Table\EngineNoiseTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class EngineNoiseController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param EngineNoiseTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(EngineNoiseTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param EngineNoiseFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(EngineNoiseFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param EngineNoiseFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(EngineNoiseFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
