<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\EngineSize\Form\EngineSizeFormBuilder;
use Skrollx\CarsModule\EngineSize\Table\EngineSizeTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class EngineSizesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param EngineSizeTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(EngineSizeTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param EngineSizeFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(EngineSizeFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param EngineSizeFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(EngineSizeFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
