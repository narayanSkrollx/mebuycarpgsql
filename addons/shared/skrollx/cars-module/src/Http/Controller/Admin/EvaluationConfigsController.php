<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\EvaluationConfig\Form\EvaluationConfigFormBuilder;
use Skrollx\CarsModule\EvaluationConfig\Table\EvaluationConfigTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Skrollx\CarsModule\EvaluationConfig\EvaluationConfigModel;
use Skrollx\CarsModule\EvaluationConfig\Contract\EvaluationConfigRepositoryInterface;

class EvaluationConfigsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param EvaluationConfigTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(EvaluationConfigTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param EvaluationConfigFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(EvaluationConfigFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param EvaluationConfigFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(EvaluationConfigFormBuilder $form, $id)
    {
        return $form->render($id);
    }

    public function configure(EvaluationConfigRepositoryInterface $evaluationRepository)
    {
        if(\Request::isMethod('POST')){
            $data = \Request::all();

            $config = EvaluationConfigModel::all();

            $year = $data['year'];
            $mileage = $data['mileage'];
            $paint = $data['paint'];
            $specs = $data['specs'];
            $option = $data['option'];
            $litre= $data['litre'];

            EvaluationConfigModel::where('slug','litre')->delete();
            $litre_config=[];
            $litre_config['title'] = 'Litre';
            $litre_config['slug'] = 'litre';
            $litre_config['config_value'] = 'litre';
            $litre_config['depreciation'] = $litre;

            if(is_numeric($litre)){
                $evaluationRepository->create($litre_config); 
            }

            EvaluationConfigModel::where('slug','year')->delete();
            foreach ($year as $key => $value) {
                if(isset($value['from']) and $value['from'] != '' and isset($value['to']) and $value['to'] != '' and $value['depr'] != ''){
                    
                        $value['title'] = 'Year';
                        $value['slug'] = 'year';
                        $value['config_value'] = $value['from'].'-'.$value['to'];
                        $value['depreciation'] = $value['depr'];

                        unset($value['from']);
                        unset($value['to']);
                        unset($value['depr']);

                        if(is_numeric($value['depreciation'])){
                            $evaluationRepository->create($value);   
                        }
                    }
                }

            EvaluationConfigModel::where('slug','mileage')->delete();
            foreach ($mileage as $key => $value) {
                    $value['title'] = 'Mileage';
                    $value['slug'] = 'mileage';
                    $value['config_value'] = $value['from'].'-'.$value['to'];
                    $value['depreciation'] = $value['depr'];

                    unset($value['from']);
                    unset($value['to']);
                    unset($value['depr']);

                    if(is_numeric($value['depreciation'])){
                        $evaluationRepository->create($value); 
                    }  
                }

            EvaluationConfigModel::where('slug','paint')->delete();
            foreach ($paint as $key => $value) {
                    $paint_config = [];
                    $paint_config['title'] = 'Paint';
                    $paint_config['slug'] = 'paint';
                    $paint_config['config_value'] = $key;
                    $paint_config['depreciation'] = $value;

                    if(is_numeric($paint_config['depreciation'])){
                        $evaluationRepository->create($paint_config);
                    }   
                }

            EvaluationConfigModel::where('slug','specs')->delete();
            foreach ($specs as $key => $value) {
                    $config_spec = [];
                    $config_spec['title'] = 'Specs';
                    $config_spec['slug'] = 'specs';
                    $config_spec['config_value'] = $key;
                    $config_spec['depreciation'] = $value;

                    if(is_numeric($config_spec['depreciation'])){
                        $evaluationRepository->create($config_spec);
                    }   
                }

            EvaluationConfigModel::where('slug','option')->delete();
            foreach ($option as $key => $value) {
                    $config_option = [];
                    $config_option['title'] = 'Option';
                    $config_option['slug'] = 'option';
                    $config_option['config_value'] = $key;
                    $config_option['depreciation'] = $value;

                    if(is_numeric($config_option['depreciation'])){
                        $evaluationRepository->create($config_option);
                    }   
                }

                return redirect()->back();
        }

        $years = EvaluationConfigModel::where('slug','year')->get();
        $specs = EvaluationConfigModel::where('slug','specs')->get();
        $paints = EvaluationConfigModel::where('slug','paint')->get();
        $options = EvaluationConfigModel::where('slug','option')->get();
        $mileages = EvaluationConfigModel::where('slug','mileage')->get();
        $litre = EvaluationConfigModel::where('slug','litre')->first();

        return view('module::admin/evaluation-config',compact('years','specs','paints','mileages','options','litre'));
    }
}
