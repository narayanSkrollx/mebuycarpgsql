<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\Fluid\Form\FluidFormBuilder;
use Skrollx\CarsModule\Fluid\Table\FluidTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class FluidsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param FluidTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(FluidTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param FluidFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(FluidFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param FluidFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(FluidFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
