<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\GearboxCondition\Form\GearboxConditionFormBuilder;
use Skrollx\CarsModule\GearboxCondition\Table\GearboxConditionTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class GearboxConditionController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param GearboxConditionTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(GearboxConditionTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param GearboxConditionFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(GearboxConditionFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param GearboxConditionFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(GearboxConditionFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
