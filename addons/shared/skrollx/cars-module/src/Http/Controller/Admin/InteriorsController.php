<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\Interior\Form\InteriorFormBuilder;
use Skrollx\CarsModule\Interior\Table\InteriorTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class InteriorsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param InteriorTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(InteriorTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param InteriorFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(InteriorFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param InteriorFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(InteriorFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
