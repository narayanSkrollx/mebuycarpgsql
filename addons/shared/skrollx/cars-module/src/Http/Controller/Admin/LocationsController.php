<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\Location\Form\LocationFormBuilder;
use Skrollx\CarsModule\Location\Table\LocationTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class LocationsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param LocationTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(LocationTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param LocationFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(LocationFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param LocationFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(LocationFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
