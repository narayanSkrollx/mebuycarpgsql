<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Skrollx\CarsModule\Make\Form\MakeFormBuilder;
use Skrollx\CarsModule\Make\MakeModel;
use Skrollx\CarsModule\Make\Table\MakeTableBuilder;
use Skrollx\CarsModule\Model\ModelModel;
use Skrollx\CarsModule\Price\Contract\PriceRepositoryInterface;
use Skrollx\CarsModule\Price\PriceModel;
use Skrollx\CarsModule\EngineSize\EngineSizeModel;
use DB;

class MakesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param MakeTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(MakeTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param MakeFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(MakeFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param MakeFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(MakeFormBuilder $form, $id)
    {
        return $form->render($id);
    }

    public function prices(PriceRepositoryInterface $priceRepository, $id)
    {
        $make = MakeModel::find($id);
        $models = ModelModel::where('make_id', $id)->get();
        $priceRaw = PriceModel::where('make_id', $id)->select('id', 'make_id', 'model_id', 'price')->get();
        $engine_sizes = EngineSizeModel::get();
        $prices = [];
        foreach($priceRaw as $price){
            $prices[$price->model_id] = $price->toArray();
        }

        if($data = \Request::get('prices')){
            PriceModel::where('make_id', $id)->delete();
            foreach($data as $price){
                if(!isset($price['model_id']))
                    { 
                        unset($data[$id]); 
                        continue; 
                    }

                foreach ($price['prices'] as $key => $value) {
                    if(isset($value['price']) and !is_numeric($value['price'])) continue;

                    $value['make_id'] = $id;
                    if (isset($value['litre'])) {
                        $value['engine_size_id'] = $value['litre'];
                    }
                    $value['model_id'] = $price['model_id'];

                    unset($value['litre']);

                    $new_price = $priceRepository->create($value);
                }
            }
            return redirect()->back()->with('success', ['Price added successfully.']);
        }

        $this->breadcrumbs->add($make->title);
        $this->breadcrumbs->add('Prices');
        return view('module::admin/prices', compact('make', 'models', 'prices','engine_sizes'));
    }

    public function getNewPriceRow()
    {
        $newcount = \Request::input('count');
        $model = \Request::input('model');
        $engine_sizes = EngineSizeModel::get();

        $new_row = $this->view->make('skrollx.module.cars::admin/newpricerow',compact('newcount','model','engine_sizes'));
        return json_encode($new_row->render());
    }
}
