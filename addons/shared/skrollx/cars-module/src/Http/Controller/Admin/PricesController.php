<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\Price\Form\PriceFormBuilder;
use Skrollx\CarsModule\Price\Table\PriceTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class PricesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param PriceTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PriceTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param PriceFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(PriceFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param PriceFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(PriceFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
