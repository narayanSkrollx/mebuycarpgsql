<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\TestdriveGeneral\Form\TestdriveGeneralFormBuilder;
use Skrollx\CarsModule\TestdriveGeneral\Table\TestdriveGeneralTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class TestdriveGeneralController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param TestdriveGeneralTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(TestdriveGeneralTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param TestdriveGeneralFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(TestdriveGeneralFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param TestdriveGeneralFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(TestdriveGeneralFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
