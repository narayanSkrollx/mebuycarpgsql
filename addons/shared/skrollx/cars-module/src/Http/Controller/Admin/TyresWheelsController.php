<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\TyresWheel\Form\TyresWheelFormBuilder;
use Skrollx\CarsModule\TyresWheel\Table\TyresWheelTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class TyresWheelsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param TyresWheelTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(TyresWheelTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param TyresWheelFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(TyresWheelFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param TyresWheelFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(TyresWheelFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
