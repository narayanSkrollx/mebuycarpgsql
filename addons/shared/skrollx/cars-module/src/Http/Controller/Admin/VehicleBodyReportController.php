<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\VehicleBodyReport\Form\VehicleBodyReportFormBuilder;
use Skrollx\CarsModule\VehicleBodyReport\Table\VehicleBodyReportTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class VehicleBodyReportController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param VehicleBodyReportTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(VehicleBodyReportTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param VehicleBodyReportFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(VehicleBodyReportFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param VehicleBodyReportFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(VehicleBodyReportFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
