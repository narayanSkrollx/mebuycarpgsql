<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\VehicleDetail\Form\VehicleDetailFormBuilder;
use Skrollx\CarsModule\VehicleDetail\Table\VehicleDetailTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class VehicleDetailController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param VehicleDetailTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(VehicleDetailTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param VehicleDetailFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(VehicleDetailFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param VehicleDetailFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(VehicleDetailFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
