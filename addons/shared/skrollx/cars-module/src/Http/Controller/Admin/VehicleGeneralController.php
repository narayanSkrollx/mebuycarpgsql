<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\VehicleGeneral\Form\VehicleGeneralFormBuilder;
use Skrollx\CarsModule\VehicleGeneral\Table\VehicleGeneralTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class VehicleGeneralController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param VehicleGeneralTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(VehicleGeneralTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param VehicleGeneralFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(VehicleGeneralFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param VehicleGeneralFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(VehicleGeneralFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
