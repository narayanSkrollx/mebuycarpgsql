<?php namespace Skrollx\CarsModule\Http\Controller\Admin;

use Skrollx\CarsModule\VehicleSpec\Form\VehicleSpecFormBuilder;
use Skrollx\CarsModule\VehicleSpec\Table\VehicleSpecTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class VehicleSpecsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param VehicleSpecTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(VehicleSpecTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param VehicleSpecFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(VehicleSpecFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param VehicleSpecFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(VehicleSpecFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
