<?php namespace Skrollx\CarsModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Skrollx\CarsModule\Date\DateModel;
use Skrollx\CarsModule\EngineSize\EngineSizeModel;
use Skrollx\CarsModule\Model\ModelModel;
use Skrollx\CarsModule\Price\PriceModel;

class AjaxController extends PublicController
{
    public function modelDropdown()
    {
        $make_id = \Request::get('make') ? \Request::get('make') : \Request::get('elem');
        $models = ModelModel::where('make_id', $make_id)->orderBy('title','ASC')->select('title', 'id')->get()->toArray();

        return $models;
    }
    
    public function engineSizeDropdown()
    {
        $make_id = \Request::get('make') ? \Request::get('make') : \Request::get('elem');
        $year = (int)\Request::get('year');;

        $response = [];

        $sizes = PriceModel::where('make_id',$make_id)
                            ->where('year_from','<=',$year)
                            ->where('year_to','>=',$year)
                            ->select('engine_size_id')
                            ->groupBy(\DB::raw("engine_size_id"))
                            ->get();

        foreach ($sizes as $key => $value) {
            $response[$value->engine_size->id] = $value->engine_size->title;
        }

        // $model_id = \Request::get('model') ? \Request::get('model') : \Request::get('elem');
        // $sizes = ModelModel::find($model_id)->engineSizes()->orderBy('id','asc')->pluck('cars_engine_sizes.title', 'cars_engine_sizes.id');

        return $response;
    }

    public function getDates()
    {
        $holidays = \DB::table('streams_holidays')->pluck('day')->toArray();

        $current_date = date('Y-m-d');
        $location_id = \Request::get('location');
        $dates = DateModel::where('location_id',$location_id)
                            ->groupBy(\DB::raw("CAST(date AS DATE)"))
                            ->select(\DB::raw('CAST(date as DATE) as date'))
                            ->where('reserved',false)
                            ->where('date','>=',$current_date)
                            ->get();

        $results = [];

        foreach ($dates as $key => $value) {
            $date = date('Y-m-d',strtotime($value->date));

            $dayname = date('D',strtotime($value->date));
            $monthname = date('M',strtotime($value->date));
            $day = date('d',strtotime($value->date));
            $year = date('Y',strtotime($value->date));

            $value->dayname = $dayname;
            $value->monthname = $monthname;
            $value->day = $day;
            $value->year = $year;

            if(in_array($date,$holidays)){
                $value->holiday = true;
            }

        }

        echo json_encode($dates);
    }

    public function getTimes()
    {
        $date = \Request::get('date');
        $location_id = \Request::get('location');
        $times = DateModel::where('location_id',$location_id)
                            ->where(\DB::raw("CAST(date AS DATE)"),'=',$date)
                            ->select(\DB::raw('CAST(date as TIME) as time'),'id','location_id')
                            ->where('reserved',false)
                            ->get();
        echo json_encode($times);
    }
}
