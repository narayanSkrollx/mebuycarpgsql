<?php namespace Skrollx\CarsModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Skrollx\CarsModule\Car\CarModel;
use Skrollx\CarsModule\Date\DateModel;
use Skrollx\CarsModule\Car\Mail\EvaluationRequested;
use Skrollx\CarsModule\Car\Mail\BookingConfirmed;
use Skrollx\CarsModule\VehicleDetail\VehicleDetailModel;
use Skrollx\CarsModule\VehicleSpec\VehicleSpecModel;
use Skrollx\CarsModule\VehicleGeneral\VehicleGeneralModel;
use Skrollx\CarsModule\VehicleBodyReport\VehicleBodyReportModel;
use Skrollx\CarsModule\EngineCondition\EngineConditionModel;
use Skrollx\CarsModule\EngineNoise\EngineNoiseModel;
use Skrollx\CarsModule\GearboxCondition\GearboxConditionModel;
use Skrollx\CarsModule\AcCondition\AcConditionModel;
use Skrollx\CarsModule\AccessoryPart\AccessoryPartModel;
use Skrollx\CarsModule\Fluid\FluidModel;
use Skrollx\CarsModule\TestdriveGeneral\TestdriveGeneralModel;
use Skrollx\CarsModule\ElectricalSystem\ElectricalSystemModel;
use Skrollx\CarsModule\TyresWheel\TyresWheelModel;
use Skrollx\CarsModule\Interior\InteriorModel;
use Skrollx\CarsModule\Brake\BrakeModel;
use Auth;
use \DateTime;

class CarsController extends PublicController
{

    /**
     * Evaliation step 1
     */
    public function stepOne()
    {
        if(\Request::method() == 'POST'){
            $post = \Request::all();
            $validator = \Validator::make($post, [
                'make' => 'required',
                'model' => 'required',
                'year' => 'required',
                // 'engine_size' => 'required',
                'mileage' => 'required',
                // 'email' => 'required|email',
                'option' => 'required',
                'paint' => 'required',
                'specs' => 'required',
            ]);
            if($validator->passes()){
                $locale = \App::getLocale();
                unset($post['_token'],$post['terms_conditions']);
                session(['step-1' => $post]);
                return redirect($locale.'/evaluate/step-2')->with('data', $post);
            }else{
                return redirect()->back()->with('data', $post)->with('error', $validator->errors()->all());
            }
        }
        $this->template->meta_title = trans('theme::site.select_your_car');
        return view('module::step-1');
    }

    /**
     * Evaliation step 2
     */
    public function stepTwo()
    {
        $data = session('step-1');
        $post = \Request::all();
        if(isset($post['date_id'])){
            $date_id = $post['date_id'];
            unset($post['date_id']);
        }

        unset($post['_token']);
        if(!isset($data['model'])){
            return redirect('evaluate/step-1')->with('error', [trans('module::message.select_car')]);
        }

        $evaluation = CarModel::evaluateCar($data);
        $data = (array)$data + (array)$post;
        $data['evaluation_price'] = $evaluation['price'];
        if(isset($data['time'])){
            $date_time = new DateTime($post['time']);
            $time = $date_time->format('h:i');
            $date = new DateTime($post['date']);
            $date = $date->format('Y-m-d');
            $data['date'] = $date.' '.$time.':00';
            unset($data['time']);
        }

        $data = new CarModel($data);
        if(\Request::method() == 'POST'){
            if(isset($post['email'])){            
                $email = $post['email'];
                unset($post['email']);
            }
            if($seller_price = \Request::get('seller_price')){
                $step1data = session('step-1');
                $step1data['seller_price'] = $seller_price;
                session(['step-1' => $step1data]);

                return redirect('evaluate/step-2')->with([
                    'success' => [trans('skrollx.module.cars::message.thank_you_book_appointment')], 
                    'data' => $step1data
                ]);
            }
            $validator = \Validator::make($post, [
                'first_name' => 'required',
                'last_name' => 'required',
                'mobile' => 'required',
                'location' => 'required',
                'date' => 'required',
                'time' => 'required',
                // 'alt_contact' => 'required',
            ]);

            if($validator->passes()){
                $data->title = $data->make->title.' '.$data->year;
                $data->slug = str_slug($data->title.' '.rand());
                $data->status = 1;
                $data->save();

                // DateModel::where('id',$date_id)->update(['reserved'=>true]);
                $booking_count = DateModel::where('id',$date_id)->select('booking_count')->first();

                if($booking_count->booking_count < 5){
                    DateModel::where('id',$date_id)->update(['booking_count'=>$booking_count->booking_count+1]);

                    if(($booking_count->booking_count+1) >= 5){
                        DateModel::where('id',$date_id)->update(['reserved'=> true]);
                    }
                }

                $to_email = env('APPOINTMENT_EMAIL', 'admin@mebuycar.com');
                \Mail::to($to_email)->send(new EvaluationRequested($data));
                if(strlen($email)){
                    \Mail::to($email)->send(new BookingConfirmed($data));
                }

                session()->forget('step-1');

                $thank_you_data['date'] = $data->date->format('Y/m/d');
                $thank_you_data['time'] = $data->date->format('h:i: A');
                $thank_you_data['location'] = $data->location->name;

                session(['step-2' => $thank_you_data]);
                
                if(strlen($email)){
                    return redirect('/evaluate/thank-you')->with([
                        'success' => [trans('module::message.evaluation_complete', [
                            'date' => $data->date->format('Y/m/d'),
                            'time' => $data->date->format('h:i A'),
                            'location' => $data->location->name,
                        ])],
                        'data' => $data
                        ]);
                }else{
                    return redirect('/evaluate/thank-you')->with([
                        'success' => [trans('module::message.evaluation_complete_no_email', [
                            'date' => $data->date->format('Y/m/d'),
                            'time' => $data->date->format('h:i A'),
                            'location' => $data->location->name,
                        ])],
                        'data' => $data
                        ]);
                }

            }else{
                return redirect()->back()->with('data', $data)->with('error', $validator->errors()->all());
            }
        }
        
        \Counter::showAndCount('evaluate');
        $this->template->meta_title = trans('theme::site.book_an_appointment');
        return view('module::step-2', ['data' => $data, 'evaluation' => $evaluation]);
    }

    /**
     * Evaliation - thank you
     */
    public function thankYou()
    {
        if(session('step-2')){
            $data = session('step-2');

            session()->forget('step-2');
            session()->forget('step-1');

            $this->template->meta_title = trans('theme::site.thank_you');

            return view('module::thank-you',compact('data'));
        }else{
            return redirect('/');
        }
    }

    /**
     * Locations
     */
    public function locations()
    {
        $this->template->meta_title = trans('theme::site.locations');
        return view('module::locations');
    }

    public function checkInPage()
    {
        $mobile = \Request::get('m');
        if(\Request::isMethod('post')){
            $mobile = \Request::input('mobile_no');
            
            $informations = CarModel::where('mobile',$mobile)->get();
            if(!count($informations)){
                return redirect('user/check-in?m='.$mobile)->with('error',[trans('module::message.checkin_mobile_invalid')]);
            }
        }

        $this->template->meta_title = trans('module::site.check_in');
        return view('module::check-in',compact('informations','mobile'));
    }

    public function checkVehicleReport()
    {
        $mobile = \Request::get('m');
        if(is_numeric($mobile)){
            $vehicles = CarModel::where('mobile',$mobile)->get();
        }
        if(\Request::isMethod('post')){
            $mobile = \Request::input('mobile_no');
            
            $vehicles = CarModel::where('mobile',$mobile)->get();
            if(!count($vehicles)){
                return redirect('user/check-report?m='.$mobile)->with('error',[trans('module::message.checkin_mobile_invalid')]);
            }
        }

        $this->template->meta_title = trans('module::site.vehicle_report');
        return view('module::vehicle-report',compact('vehicles','mobile'));
    }

    public function viewReport($id)
    {
        $car = CarModel::find($id);

        $tabs = [];

        $VehicleDetail = VehicleDetailModel::where('car_id',$car->id)->first();
        
        if($VehicleDetail){
            $tabs[trans('module::site.VehicleDetail')] = [
                'id' => 'VehicleDetail',
                'data' => $VehicleDetail,
            ];
        }

        $VehicleSpec = VehicleSpecModel::where('car_id',$car->id)->first();
        
        if($VehicleSpec){
            $tabs[trans('module::site.VehicleSpec')] = [
                'data' => $VehicleSpec,
                'id' => 'VehicleSpec' 
            ];
        }

        $VehicleGeneral = VehicleGeneralModel::where('car_id',$car->id)->first();
        
        if($VehicleGeneral){
            $tabs[trans('module::site.VehicleGeneral')] = [
                'data' => $VehicleGeneral,
                'id' => 'VehicleGeneral' 
            ];
        }

        $VehicleBodyReport = VehicleBodyReportModel::where('car_id',$car->id)->first();
        
        if($VehicleBodyReport){
            $tabs[trans('module::site.VehicleBodyReport')] = [
                'data' => $VehicleBodyReport,
                'id' => 'VehicleBodyReport' 
            ];
        }

        $EngineCondition = EngineConditionModel::where('car_id',$car->id)->first();
        
        if($EngineCondition){
            $tabs[trans('module::site.EngineCondition')] = [
                'data' => $EngineCondition,
                'id' => 'EngineCondition' 
            ];
        }

        $EngineNoise = EngineNoiseModel::where('car_id',$car->id)->first();
        
        if($EngineNoise){
            $tabs[trans('module::site.EngineNoise')] = [
                'data' => $EngineNoise,
                'id' => 'EngineNoise' 
            ];
        }

        $GearboxCondition = GearboxConditionModel::where('car_id',$car->id)->first();
        
        if($GearboxCondition){
            $tabs[trans('module::site.GearboxCondition')] = [
                'data' => $GearboxCondition,
                'id' => 'GearboxCondition' 
            ];
        }

        $AcCondition = AcConditionModel::where('car_id',$car->id)->first();
        
        if($AcCondition){
            $tabs[trans('module::site.AcCondition')] = [
                'data' => $AcCondition,
                'id' => 'AcCondition' 
            ];
        }

        $AccessoryPart = AccessoryPartModel::where('car_id',$car->id)->first();
        
        if($AccessoryPart){
            $tabs[trans('module::site.AccessoryPart')] = [
                'data' => $AccessoryPart,
                'id' => 'AccessoryPart' 
            ];
        }

        $Fluid = FluidModel::where('car_id',$car->id)->first();
        
        if($Fluid){
            $tabs[trans('module::site.Fluid')] = [
                'data' => $Fluid,
                'id' => 'Fluid' 
            ];
        }

        $TestdriveGeneral = TestdriveGeneralModel::where('car_id',$car->id)->first();
        
        if($TestdriveGeneral){
            $tabs[trans('module::site.TestdriveGeneral')] = [
                'data' => $TestdriveGeneral,
                'id' => 'TestdriveGeneral' 
            ];
        }

        $ElectricalSystem = ElectricalSystemModel::where('car_id',$car->id)->first();
        
        if($ElectricalSystem){
            $tabs[trans('module::site.ElectricalSystem')] = [
                'data' => $ElectricalSystem,
                'id' => 'ElectricalSystem' 
            ];
        }

        $TyresWheel = TyresWheelModel::where('car_id',$car->id)->first();
        
        if($TyresWheel){
            $tabs[trans('module::site.TyresWheel')] = [
                'data' => $TyresWheel,
                'id' => 'TyresWheel' 
            ];
        }

        $Interior = InteriorModel::where('car_id',$car->id)->first();
        
        if($Interior){
            $tabs[trans('module::site.Interior')] = [
                'data' => $Interior,
                'id' => 'Interior' 
            ];
        }

        $Brake = BrakeModel::where('car_id',$car->id)->first();

        if($Brake){
            $tabs[trans('module::site.Brake')] = [
                'data' => $Brake,
                'id' => 'Brake' 
            ];
        }

        $this->template->meta_title = trans('module::site.vehicle_report');
        return view('module::view-report',compact('tabs','car'));
    }

    public function checkIn($id)
    {

        CarModel::where('id',$id)->update(['checked_in'=> true,'deal_status'=>'checkedin']);

        return redirect()->back()->with('success',[trans('module::message.user_checkin_success')]);
    }

    public function checkOut($id)
    {

        CarModel::where('id',$id)->update(['checked_in'=> false,'deal_status'=>'checkedout']);

        return redirect()->back()->with('success',[trans('module::message.user_checkout_success')]);
    }
}
