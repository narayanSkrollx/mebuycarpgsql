<?php namespace Skrollx\CarsModule\Http\Controller\Dealer;

use Anomaly\SettingsModule\Setting\SettingRepository;
use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Anomaly\Streams\Platform\Support\Currency;
use Skrollx\CarsModule\Car\CarModel;
use Skrollx\CarsModule\Car\Mail\BiddingPlaced;
use Skrollx\DealersModule\Bidding\BiddingModel;
use Skrollx\DealersModule\Bidding\BiddingRepository;

class AjaxController extends PublicController
{
    public function placeBid(SettingRepository $settings, Currency $currency, BiddingRepository $biddings)
    {
        $return = [
            'success' => 0,
            'msg' => trans('skrollx.module.cars::message.bidding_error')
        ];

        $car_id = \Request::get('car_id');
        $dealer_id = \Request::get('dealer_id');
        $amount = \Request::get('amount');

        $car = CarModel::find($car_id);
        $bid_increment_setting = $settings->get('skrollx.module.cars::bid_increment_amount');
        $bid_increment_amount = $bid_increment_setting ? $bid_increment_setting->value : 500;
        if(!$car){
            $return['msg'] = trans('skrollx.module.cars::message.bidding_no_car_found');
            return $return;
        }
        if($car->status != 4){
            $return['msg'] = trans('skrollx.module.cars::message.bidding_not_available_for_bidding');
            return $return;
        }
        if(\Carbon\Carbon::now()->diffInMinutes($car->expire_date, false) <= 0){
            $return['msg'] = trans('skrollx.module.cars::message.bidding_time_expired');
            return $return;
        }

        $balance = \Auth::user()->dealer()->getBalance();
        $bidding = BiddingModel::where(['dealer_id' => $dealer_id, 'car_id' => $car_id])->first();
        
        // if(isset($bidding->amount)){
        //     // add amount if he has already placed bidding for this car
        //     $balance = $balance + $bidding->amount;
        // }

        $min_bid_amount = $car->min_bid_amount + $bid_increment_amount;
        $bidding_limits = config('skrollx.module.cars::cars.bidding_limit');
        $min_deposit_amount = array_keys($bidding_limits)[0];
        // if($balance < $min_deposit_amount){
        //     $return['msg'] = trans('skrollx.module.cars::message.bidding_not_enough_balance', ['amount' => $currency->format($min_deposit_amount - $balance)]);
        //     return $return;
        // }
        if($amount < $min_bid_amount){
            $return['msg'] = trans('skrollx.module.cars::message.bidding_min_balance_msg', ['amount' => $currency->format($min_bid_amount)]);
            return $return;
        }

        $min_bidding_balance = 0;
        $min_deposit_amount = 0;
        foreach($bidding_limits as $deposit_amt => $max_bid_amt){
            if($amount < $max_bid_amt){
                $min_bidding_balance = $max_bid_amt;
                $min_deposit_amount = $deposit_amt;
                break;
            }
        }
        // if($balance < $min_deposit_amount){
        //     $return['msg'] = trans('skrollx.module.cars::message.bidding_not_enough_balance', ['amount' => $currency->format($min_deposit_amount - $balance)]);
        //     return $return;
        // }

        $bidding_data = [
            'dealer_id' => $dealer_id,
            'car_id'    => $car_id,
            'amount'    => $amount,
            'date'    => \Carbon\Carbon::now(),
        ];
        $bidding = BiddingModel::updateOrCreate(
            ['dealer_id' => $dealer_id, 'car_id' => $car_id],
            $bidding_data
        );

        $car->min_bid_amount = $amount;
        $car->save();

        /**
         * @todo send email to admin and bidder with details.
         */
        $to_email = env('ADMIN_EMAIL', 'admin@mebuycar.com');
        \Mail::to($to_email)->send(new BiddingPlaced($bidding));
        
        return [
            'success' => 1,
            'msg' => trans('skrollx.module.cars::message.bidding_placed_success')
        ];
    }
}
