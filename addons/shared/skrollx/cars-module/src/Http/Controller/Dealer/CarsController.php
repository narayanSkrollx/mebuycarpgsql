<?php namespace Skrollx\CarsModule\Http\Controller\Dealer;

use Anomaly\SettingsModule\Setting\SettingRepository;
use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Skrollx\CarsModule\Car\CarRepository;
use Skrollx\CarsModule\Car\CarModel;
use Skrollx\CarsModule\VehicleDetail\VehicleDetailModel;
use Skrollx\CarsModule\VehicleSpec\VehicleSpecModel;
use Skrollx\CarsModule\VehicleGeneral\VehicleGeneralModel;
use Skrollx\CarsModule\VehicleBodyReport\VehicleBodyReportModel;
use Skrollx\CarsModule\EngineCondition\EngineConditionModel;
use Skrollx\CarsModule\EngineNoise\EngineNoiseModel;
use Skrollx\CarsModule\GearboxCondition\GearboxConditionModel;
use Skrollx\CarsModule\AcCondition\AcConditionModel;
use Skrollx\CarsModule\AccessoryPart\AccessoryPartModel;
use Skrollx\CarsModule\Fluid\FluidModel;
use Skrollx\CarsModule\TestdriveGeneral\TestdriveGeneralModel;
use Skrollx\CarsModule\ElectricalSystem\ElectricalSystemModel;
use Skrollx\CarsModule\TyresWheel\TyresWheelModel;
use Skrollx\CarsModule\Interior\InteriorModel;
use Skrollx\CarsModule\Brake\BrakeModel;

class CarsController extends PublicController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->middleware(\Skrollx\DealersModule\Http\Middleware\CheckDealerAccess::class);
    }

    /**
     * Display cars applicable for bidding
     */
    public function index(SettingRepository $settings)
    {
        $perpage = \Request::get('per_page') ?: 10;
        $cars = CarModel::filter()->where('status', 4)->where('expire_date', '>=', \Carbon\Carbon::now()->toDateTimeString())->paginate($perpage);
        $this->template->meta_title = trans('module::addon.title');
        return view('module::dealer/cars', compact('cars'));
    }

    /**
     * view car details
     */
    public function view(CarRepository $cars, $slug)
    {
        $car = is_numeric($slug) ? $cars->find($slug) : $cars->findBySlug($slug);

        $tabs = [];

        $VehicleDetail = VehicleDetailModel::where('car_id',$car->id)->first();
        
        if($VehicleDetail){
            $tabs[trans('module::site.VehicleDetail')] = [
                'id' => 'VehicleDetail',
                'data' => $VehicleDetail,
            ];
        }

        $VehicleSpec = VehicleSpecModel::where('car_id',$car->id)->first();
        
        if($VehicleSpec){
            $tabs[trans('module::site.VehicleSpec')] = [
                'data' => $VehicleSpec,
                'id' => 'VehicleSpec' 
            ];
        }

        $VehicleGeneral = VehicleGeneralModel::where('car_id',$car->id)->first();
        
        if($VehicleGeneral){
            $tabs[trans('module::site.VehicleGeneral')] = [
                'data' => $VehicleGeneral,
                'id' => 'VehicleGeneral' 
            ];
        }

        $VehicleBodyReport = VehicleBodyReportModel::where('car_id',$car->id)->first();
        
        if($VehicleBodyReport){
            $tabs[trans('module::site.VehicleBodyReport')] = [
                'data' => $VehicleBodyReport,
                'id' => 'VehicleBodyReport' 
            ];
        }

        $EngineCondition = EngineConditionModel::where('car_id',$car->id)->first();
        
        if($EngineCondition){
            $tabs[trans('module::site.EngineCondition')] = [
                'data' => $EngineCondition,
                'id' => 'EngineCondition' 
            ];
        }

        $EngineNoise = EngineNoiseModel::where('car_id',$car->id)->first();
        
        if($EngineNoise){
            $tabs[trans('module::site.EngineNoise')] = [
                'data' => $EngineNoise,
                'id' => 'EngineNoise' 
            ];
        }

        $GearboxCondition = GearboxConditionModel::where('car_id',$car->id)->first();
        
        if($GearboxCondition){
            $tabs[trans('module::site.GearboxCondition')] = [
                'data' => $GearboxCondition,
                'id' => 'GearboxCondition' 
            ];
        }

        $AcCondition = AcConditionModel::where('car_id',$car->id)->first();
        
        if($AcCondition){
            $tabs[trans('module::site.AcCondition')] = [
                'data' => $AcCondition,
                'id' => 'AcCondition' 
            ];
        }

        $AccessoryPart = AccessoryPartModel::where('car_id',$car->id)->first();
        
        if($AccessoryPart){
            $tabs[trans('module::site.AccessoryPart')] = [
                'data' => $AccessoryPart,
                'id' => 'AccessoryPart' 
            ];
        }

        $Fluid = FluidModel::where('car_id',$car->id)->first();
        
        if($Fluid){
            $tabs[trans('module::site.Fluid')] = [
                'data' => $Fluid,
                'id' => 'Fluid' 
            ];
        }

        $TestdriveGeneral = TestdriveGeneralModel::where('car_id',$car->id)->first();
        
        if($TestdriveGeneral){
            $tabs[trans('module::site.TestdriveGeneral')] = [
                'data' => $TestdriveGeneral,
                'id' => 'TestdriveGeneral' 
            ];
        }

        $ElectricalSystem = ElectricalSystemModel::where('car_id',$car->id)->first();
        
        if($ElectricalSystem){
            $tabs[trans('module::site.ElectricalSystem')] = [
                'data' => $ElectricalSystem,
                'id' => 'ElectricalSystem' 
            ];
        }

        $TyresWheel = TyresWheelModel::where('car_id',$car->id)->first();
        
        if($TyresWheel){
            $tabs[trans('module::site.TyresWheel')] = [
                'data' => $TyresWheel,
                'id' => 'TyresWheel' 
            ];
        }

        $Interior = InteriorModel::where('car_id',$car->id)->first();
        
        if($Interior){
            $tabs[trans('module::site.Interior')] = [
                'data' => $Interior,
                'id' => 'Interior' 
            ];
        }

        $Brake = BrakeModel::where('car_id',$car->id)->first();

        if($Brake){
            $tabs[trans('module::site.Brake')] = [
                'data' => $Brake,
                'id' => 'Brake' 
            ];
        }

        if(!$car){
            return redirect()->back();
        }
        $this->template->meta_title = $car->title;
        return view('module::dealer/view-car', compact('car','VehicleDetail','VehicleSpec','VehicleGeneral','VehicleBodyReport','EngineCondition','EngineNoise','GearboxConition','AcCondition','AccessoryPart','Fluid','TestdriveGeneral','ElectricalSystem','TyresWheel','Interior','Brake','tabs'));
    }
}