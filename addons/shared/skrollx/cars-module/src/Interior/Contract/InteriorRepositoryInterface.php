<?php namespace Skrollx\CarsModule\Interior\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface InteriorRepositoryInterface extends EntryRepositoryInterface
{

}
