<?php namespace Skrollx\CarsModule\Interior;

use Skrollx\CarsModule\Interior\Contract\InteriorRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class InteriorRepository extends EntryRepository implements InteriorRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var InteriorModel
     */
    protected $model;

    /**
     * Create a new InteriorRepository instance.
     *
     * @param InteriorModel $model
     */
    public function __construct(InteriorModel $model)
    {
        $this->model = $model;
    }
}
