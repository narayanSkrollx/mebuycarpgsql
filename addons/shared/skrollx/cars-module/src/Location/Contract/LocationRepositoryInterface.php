<?php namespace Skrollx\CarsModule\Location\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface LocationRepositoryInterface extends EntryRepositoryInterface
{

}
