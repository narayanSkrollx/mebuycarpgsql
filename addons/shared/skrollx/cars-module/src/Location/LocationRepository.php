<?php namespace Skrollx\CarsModule\Location;

use Skrollx\CarsModule\Location\Contract\LocationRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class LocationRepository extends EntryRepository implements LocationRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var LocationModel
     */
    protected $model;

    /**
     * Create a new LocationRepository instance.
     *
     * @param LocationModel $model
     */
    public function __construct(LocationModel $model)
    {
        $this->model = $model;
    }
}
