<?php namespace Skrollx\CarsModule\Location;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Skrollx\CarsModule\Location\Contract\LocationRepositoryInterface;

class LocationSeeder extends Seeder
{

    /**
     * The category repository.
     *
     * @var RoleRepositoryInterface
     */
    protected $locations;

    /**
     * Create a new RoleSeeder instance.
     *
     * @param RoleRepositoryInterface $locations
     */
    public function __construct(LocationRepositoryInterface $locations)
    {
        $this->locations = $locations;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        $this->locations->truncate();
        $faker = \Faker\Factory::create();

        $locations = [
            'Dubai',
            'Abu Dhabi',
            'Sharjah',
            'Al Ain',
            'Ajman',
            'Ras Al Khaimah',
            'Fujairah',
            'Umm al-Quwain'
        ];

        foreach($locations as $location){
            $this->locations->create(
                [
                    'en' => [
                        'name' => $location,
                        'description' => $faker->paragraph(10, true),
                    ],
                    'slug' => str_slug($location),
                    'live' => 1,
                ]
            );
        }
    }
}
