<?php namespace Skrollx\CarsModule\Log\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface LogRepositoryInterface extends EntryRepositoryInterface
{

}
