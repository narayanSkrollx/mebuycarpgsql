<?php namespace Skrollx\CarsModule\Make\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface MakeRepositoryInterface extends EntryRepositoryInterface
{

}
