<?php namespace Skrollx\CarsModule\Make;

use Skrollx\CarsModule\Make\Contract\MakeRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class MakeRepository extends EntryRepository implements MakeRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var MakeModel
     */
    protected $model;

    /**
     * Create a new MakeRepository instance.
     *
     * @param MakeModel $model
     */
    public function __construct(MakeModel $model)
    {
        $this->model = $model;
    }
}
