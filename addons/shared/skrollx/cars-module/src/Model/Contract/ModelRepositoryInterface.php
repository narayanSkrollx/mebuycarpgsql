<?php namespace Skrollx\CarsModule\Model\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ModelRepositoryInterface extends EntryRepositoryInterface
{

}
