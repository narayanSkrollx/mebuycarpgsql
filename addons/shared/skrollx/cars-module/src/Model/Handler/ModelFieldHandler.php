<?php namespace Skrollx\CarsModule\Model\Handler;

use Anomaly\SelectFieldType\SelectFieldType;
use Illuminate\Config\Repository;
use Skrollx\CarsModule\Model\ModelModel;

/**
 * Class ModelFieldHandler
 */
class ModelFieldHandler
{

    /**
     * Handle the options.
     *
     * @param SelectFieldType $fieldType
     * @param Repository      $config
     */
    public function handle($fieldType, Repository $config)
    {
        $options = [];
        $model = $fieldType->getValue();
        if(isset($model->make_id)){
            $models = ModelModel::where('make_id', $model->make_id)->pluck('title', 'id');
            if($models->count()){
                foreach($models as $id => $title){
                    $options[$id] = $title;
                }
            }
        }
        $fieldType->setOptions($options);
    }
}
