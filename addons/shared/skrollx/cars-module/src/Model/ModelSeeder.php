<?php namespace Skrollx\CarsModule\Model;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Skrollx\CarsModule\Model\Contract\ModelRepositoryInterface;

class ModelSeeder extends Seeder
{

    /**
     * The category repository.
     *
     * @var RoleRepositoryInterface
     */
    protected $models;

    /**
     * Create a new RoleSeeder instance.
     *
     * @param RoleRepositoryInterface $models
     */
    public function __construct(ModelRepositoryInterface $models)
    {
        $this->models = $models;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        return;
    }
}
