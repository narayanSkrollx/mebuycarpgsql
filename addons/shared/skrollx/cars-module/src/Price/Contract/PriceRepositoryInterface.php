<?php namespace Skrollx\CarsModule\Price\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface PriceRepositoryInterface extends EntryRepositoryInterface
{

}
