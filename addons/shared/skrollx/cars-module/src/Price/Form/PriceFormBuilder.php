<?php namespace Skrollx\CarsModule\Price\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class PriceFormBuilder extends FormBuilder
{

    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [];

    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [];

    /**
     * The form actions.
     *
     * @var array|string
     */
    protected $actions = [];

    /**
     * The form buttons.
     *
     * @var array|string
     */
    protected $buttons = [];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The form sections.
     *
     * @var array
     */
    protected $sections = [];

    /**
     * The form assets.
     *
     * @var array
     */
    protected $assets = [
        'scripts.js' => [
            'skrollx.module.cars::js/form.js'
        ]
    ];

    // public function onPost()
    // {
    //     if($this->getFormMode() == 'edit'){
    //         $entry = $this->getFormEntry();
    //         $this->addRules('model_id', [
    //             'required',
    //             'unique:cars_prices,model_id,'.$entry->id.',id'
    //         ]);
    //     }
    // }
}
