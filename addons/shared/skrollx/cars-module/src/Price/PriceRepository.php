<?php namespace Skrollx\CarsModule\Price;

use Skrollx\CarsModule\Price\Contract\PriceRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class PriceRepository extends EntryRepository implements PriceRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var PriceModel
     */
    protected $model;

    /**
     * Create a new PriceRepository instance.
     *
     * @param PriceModel $model
     */
    public function __construct(PriceModel $model)
    {
        $this->model = $model;
    }
}
