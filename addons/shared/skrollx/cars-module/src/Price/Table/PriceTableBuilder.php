<?php namespace Skrollx\CarsModule\Price\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class PriceTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        'make'
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'make',
        'model',
        'price',
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit'
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}
