<?php namespace Skrollx\CarsModule\Seeder;

use Anomaly\FilesModule\Disk\Contract\DiskRepositoryInterface;
use Anomaly\FilesModule\Folder\Contract\FolderRepositoryInterface;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class FolderSeeder
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 */
class FolderSeeder extends Seeder
{

    /**
     * The disk repository.
     *
     * @var DiskRepositoryInterface
     */
    protected $disks;

    /**
     * The folder repository.
     *
     * @var FolderRepositoryInterface
     */
    protected $folders;

    /**
     * Create a new FolderSeeder instance.
     *
     * @param DiskRepositoryInterface   $disks
     * @param FolderRepositoryInterface $folders
     */
    public function __construct(DiskRepositoryInterface $disks, FolderRepositoryInterface $folders)
    {
        $this->disks   = $disks;
        $this->folders = $folders;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        $disk = $this->disks->findBySlug('local');

        if(!$this->folders->findBySlug('cars'))
        {
            $this->folders->create(
                [
                    'en'            => [
                        'name'        => 'Cars',
                        'description' => 'A folder for car images.',
                    ],
                    'slug'          => 'cars',
                    'disk'          => $disk,
                    'allowed_types' => [
                        'png',
                        'jpeg',
                        'jpg',
                    ],
                ]
            );
        }
    }
}
