<?php namespace Skrollx\CarsModule\TestdriveGeneral\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface TestdriveGeneralRepositoryInterface extends EntryRepositoryInterface
{

}
