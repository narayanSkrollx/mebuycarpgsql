<?php namespace Skrollx\CarsModule\TestdriveGeneral;

use Skrollx\CarsModule\TestdriveGeneral\Contract\TestdriveGeneralRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class TestdriveGeneralRepository extends EntryRepository implements TestdriveGeneralRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var TestdriveGeneralModel
     */
    protected $model;

    /**
     * Create a new TestdriveGeneralRepository instance.
     *
     * @param TestdriveGeneralModel $model
     */
    public function __construct(TestdriveGeneralModel $model)
    {
        $this->model = $model;
    }
}
