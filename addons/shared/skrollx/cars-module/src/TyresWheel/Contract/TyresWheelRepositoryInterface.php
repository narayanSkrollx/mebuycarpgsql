<?php namespace Skrollx\CarsModule\TyresWheel\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface TyresWheelRepositoryInterface extends EntryRepositoryInterface
{

}
