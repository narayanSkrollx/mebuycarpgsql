<?php namespace Skrollx\CarsModule\TyresWheel;

use Skrollx\CarsModule\TyresWheel\Contract\TyresWheelRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class TyresWheelRepository extends EntryRepository implements TyresWheelRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var TyresWheelModel
     */
    protected $model;

    /**
     * Create a new TyresWheelRepository instance.
     *
     * @param TyresWheelModel $model
     */
    public function __construct(TyresWheelModel $model)
    {
        $this->model = $model;
    }
}
