<?php namespace Skrollx\CarsModule\VehicleBodyReport\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface VehicleBodyReportRepositoryInterface extends EntryRepositoryInterface
{

}
