<?php namespace Skrollx\CarsModule\VehicleBodyReport;

use Skrollx\CarsModule\VehicleBodyReport\Contract\VehicleBodyReportInterface;
use Anomaly\Streams\Platform\Model\Cars\CarsVehicleBodyReportEntryModel;

class VehicleBodyReportModel extends CarsVehicleBodyReportEntryModel implements VehicleBodyReportInterface
{

}
