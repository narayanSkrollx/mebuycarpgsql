<?php namespace Skrollx\CarsModule\VehicleBodyReport;

use Skrollx\CarsModule\VehicleBodyReport\Contract\VehicleBodyReportRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class VehicleBodyReportRepository extends EntryRepository implements VehicleBodyReportRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var VehicleBodyReportModel
     */
    protected $model;

    /**
     * Create a new VehicleBodyReportRepository instance.
     *
     * @param VehicleBodyReportModel $model
     */
    public function __construct(VehicleBodyReportModel $model)
    {
        $this->model = $model;
    }
}
