<?php namespace Skrollx\CarsModule\VehicleDetail\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface VehicleDetailRepositoryInterface extends EntryRepositoryInterface
{

}
