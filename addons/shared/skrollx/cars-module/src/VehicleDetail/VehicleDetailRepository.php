<?php namespace Skrollx\CarsModule\VehicleDetail;

use Skrollx\CarsModule\VehicleDetail\Contract\VehicleDetailRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class VehicleDetailRepository extends EntryRepository implements VehicleDetailRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var VehicleDetailModel
     */
    protected $model;

    /**
     * Create a new VehicleDetailRepository instance.
     *
     * @param VehicleDetailModel $model
     */
    public function __construct(VehicleDetailModel $model)
    {
        $this->model = $model;
    }
}
