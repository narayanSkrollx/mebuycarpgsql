<?php namespace Skrollx\CarsModule\VehicleGeneral\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface VehicleGeneralRepositoryInterface extends EntryRepositoryInterface
{

}
