<?php namespace Skrollx\CarsModule\VehicleGeneral;

use Skrollx\CarsModule\VehicleGeneral\Contract\VehicleGeneralInterface;
use Anomaly\Streams\Platform\Model\Cars\CarsVehicleGeneralEntryModel;

class VehicleGeneralModel extends CarsVehicleGeneralEntryModel implements VehicleGeneralInterface
{

}
