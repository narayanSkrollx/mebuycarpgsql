<?php namespace Skrollx\CarsModule\VehicleGeneral;

use Skrollx\CarsModule\VehicleGeneral\Contract\VehicleGeneralRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class VehicleGeneralRepository extends EntryRepository implements VehicleGeneralRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var VehicleGeneralModel
     */
    protected $model;

    /**
     * Create a new VehicleGeneralRepository instance.
     *
     * @param VehicleGeneralModel $model
     */
    public function __construct(VehicleGeneralModel $model)
    {
        $this->model = $model;
    }
}
