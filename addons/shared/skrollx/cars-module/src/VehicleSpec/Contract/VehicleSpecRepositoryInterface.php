<?php namespace Skrollx\CarsModule\VehicleSpec\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface VehicleSpecRepositoryInterface extends EntryRepositoryInterface
{

}
