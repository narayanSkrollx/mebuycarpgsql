<?php namespace Skrollx\CarsModule\VehicleSpec;

use Skrollx\CarsModule\VehicleSpec\Contract\VehicleSpecRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class VehicleSpecRepository extends EntryRepository implements VehicleSpecRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var VehicleSpecModel
     */
    protected $model;

    /**
     * Create a new VehicleSpecRepository instance.
     *
     * @param VehicleSpecModel $model
     */
    public function __construct(VehicleSpecModel $model)
    {
        $this->model = $model;
    }
}
