<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleDealersCreateDealersFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'user' => [ 
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Users\UsersUsersEntryModel',
            ]
        ],
        'dealer' => [ 
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Dealers\DealersDealersEntryModel',
            ]
        ],
        'car' => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Cars\CarsCarsEntryModel',
            ]
        ],
        'name'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'company_name'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'company_address'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'company_building'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'company_country' => [
            'type' => 'anomaly.field_type.country',
            'config' => [
                'mode'          => 'dropdown',
            ]
        ],
        'company_city'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'company_state'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'company_zip'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'phone_no'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'mobile_no'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'passport' => [
            'type'   => 'anomaly.field_type.file',
            'locked' => false,
            'config' => [
                'folders'       => ['dealers'],
                'max'           => 1,
            ]
        ],
        'power_of_attorny' => [
            'type'   => 'anomaly.field_type.file',
            'locked' => false,
            'config' => [
                'folders'       => ['dealers'],
                'max'           => 1,
            ]
        ],
        'trade_license' => [
            'type'   => 'anomaly.field_type.file',
            'locked' => false,
            'config' => [
                'folders'       => ['dealers'],
                'max'           => 1,
            ]
        ],
        'other_docs' => [
            'type'   => 'anomaly.field_type.file',
            'locked' => false,
            'config' => [
                'folders'       => ['dealers'],
                'max'           => 1,
            ]
        ],
        'email'       => [
            'type' => 'anomaly.field_type.email'
        ],
        'traffic_file_code'       => [
            'type' => 'anomaly.field_type.text'
        ],
        'timezone' => [
            'type'   => 'anomaly.field_type.select',
            'config' => [
                'default_value' => null,
                'mode'          => 'dropdown',
                'handler'       => 'timezones'
            ]
        ],
        'country' => [
            'type' => 'anomaly.field_type.country',
            'config' => [
                'mode'          => 'dropdown',
            ]
        ],
        'shipping_country' => [
            'type' => 'anomaly.field_type.country',
            'config' => [
                'mode'          => 'dropdown',
            ]
        ],
        'shipping_city' => [ 
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Dealers\DealersCitiesEntryModel',
            ]
        ],
        'shipping_region' => [ 
            'type'   => 'anomaly.field_type.text'
        ],
        'shipping_building' => [ 
            'type'   => 'anomaly.field_type.text'
        ],
        'shipping_apartment' => [ 
            'type'   => 'anomaly.field_type.text'
        ],
        'shipping_note' => [ 
            'type'   => 'anomaly.field_type.text'
        ],
        'billing_country' => [
            'type' => 'anomaly.field_type.country',
            'config' => [
                'mode'          => 'dropdown',
            ]
        ],
        'billing_city' => [ 
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Dealers\DealersCitiesEntryModel',
            ]
        ],
        'billing_region' => [ 
            'type'   => 'anomaly.field_type.text'
        ],
        'billing_building' => [ 
            'type'   => 'anomaly.field_type.text'
        ],
        'billing_apartment' => [ 
            'type'   => 'anomaly.field_type.text'
        ],
        'billing_note' => [ 
            'type'   => 'anomaly.field_type.text'
        ],
        'live' => [ 
            'type'   => 'anomaly.field_type.boolean'
        ],
        'makes' => [ 
            'type'   => 'anomaly.field_type.multiple',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Cars\CarsMakesEntryModel',
            ]
        ],
        'year'       => [
            'type' => 'anomaly.field_type.integer'
        ],
        'mileage' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.cars::field.mileage.options.1',
                    '2'  => 'skrollx.module.cars::field.mileage.options.2',
                    '3'  => 'skrollx.module.cars::field.mileage.options.3',
                    '4'  => 'skrollx.module.cars::field.mileage.options.4',
                    '5'  => 'skrollx.module.cars::field.mileage.options.5',
                    '6'  => 'skrollx.module.cars::field.mileage.options.6',
                    '7'  => 'skrollx.module.cars::field.mileage.options.7',
                    '8'  => 'skrollx.module.cars::field.mileage.options.8',
                    '9'  => 'skrollx.module.cars::field.mileage.options.9',
                    '10' => 'skrollx.module.cars::field.mileage.options.10',
                    '11' => 'skrollx.module.cars::field.mileage.options.11',
                    '12' => 'skrollx.module.cars::field.mileage.options.12',
                    '13' => 'skrollx.module.cars::field.mileage.options.13',
                    '14' => 'skrollx.module.cars::field.mileage.options.14',
                    '15' => 'skrollx.module.cars::field.mileage.options.15',
                    '16' => 'skrollx.module.cars::field.mileage.options.16',
                ],
                'max' => 2
            ]
        ],
        'lang' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    'en'  => 'skrollx.module.dealers::field.lang.options.en',
                    'ar'  => 'skrollx.module.dealers::field.lang.options.ar',
                ],
                'mode' => 'radio',
                'default_value' => 'en',
                'max' => 2
            ]
        ],
        'deposit_type' => [
            'type' => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    '1'  => 'skrollx.module.dealers::field.deposit_type.options.1',
                    '2'  => 'skrollx.module.dealers::field.deposit_type.options.2',
                    '3'  => 'skrollx.module.dealers::field.deposit_type.options.3',
                ],
                'default_value' => 1,
                'max' => 1
            ]
        ],
        'date' => [
            'type'   => 'anomaly.field_type.datetime',
            'config' => [
                'mode'          => 'date',
                'date_format'   => 'Y-m-d',
                'year_range'    => '-1:+1',
                'timezone'      => null
            ]
        ],
        'amount'       => [
            'type' => 'anomaly.field_type.decimal'
        ],
        'note' => [ 
            'type'   => 'anomaly.field_type.text'
        ],
        'due_date' => [
            'type'   => 'anomaly.field_type.datetime',
            'config' => [
                'mode'          => 'date',
                'date_format'   => 'Y-m-d',
                'year_range'    => '-1:+1',
                'timezone'      => null
            ]
        ],
        'paid_date' => [
            'type'   => 'anomaly.field_type.datetime',
            'config' => [
                'mode'          => 'date',
                'date_format'   => 'Y-m-d',
                'year_range'    => '-1:+1',
                'timezone'      => null
            ]
        ],
        'invoice_file' => [
            'type'   => 'anomaly.field_type.file',
            'locked' => false,
            'config' => [
                'folders'       => ['invoices'],
                'max'           => 1,
            ]
        ],
        'won' => [ 
            'type'   => 'anomaly.field_type.boolean'
        ],
        'car_no' => [ 
            'type'   => 'anomaly.field_type.text'
        ],
        'invoice_no' => [ 
            'type'   => 'anomaly.field_type.text'
        ],
        'saved_cars' => [
            'type'   => 'anomaly.field_type.multiple',
            'config' => [
                'related'     => 'Anomaly\Streams\Platform\Model\Cars\CarsCarsEntryModel',
            ]
        ]
    ];

}
