<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleDealersCreateDealersStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'dealers',
        'title_column' => 'company_name',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'company_name' => [
            'required' => true
        ],
        'company_address',
        'company_building',
        'company_country',
        'company_city',
        'company_state',
        'company_zip',
        'phone_no',
        'user',
        'mobile_no',
        'passport',
        'power_of_attorny',
        'trade_license',
        'other_docs',
        'email',
        'traffic_file_code',
        'timezone',
        'shipping_country',
        'shipping_city',
        'shipping_region',
        'shipping_building',
        'shipping_apartment',
        'shipping_note',
        'billing_country',
        'billing_city',
        'billing_region',
        'billing_building',
        'billing_apartment',
        'billing_note',
        'saved_cars',
    ];

}
