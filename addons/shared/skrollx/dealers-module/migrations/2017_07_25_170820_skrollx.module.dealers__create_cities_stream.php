<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleDealersCreateCitiesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'cities',
         'title_column' => 'name',
         'translatable' => true,
         'trashable' => false,
         'searchable' => false,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [ 'required' => true, 'translatable' => true  ],
        'country' => [ 'required' => true ],
        'live',
    ];

}
