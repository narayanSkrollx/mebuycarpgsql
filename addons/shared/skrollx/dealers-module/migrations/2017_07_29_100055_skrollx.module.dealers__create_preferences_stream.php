<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleDealersCreatePreferencesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'preferences',
        'title_column' => 'country',
        'translatable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'user',
        'country',
        'year',
        'makes',
        'mileage',
        'lang'
    ];

}
