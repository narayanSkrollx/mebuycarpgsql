<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleDealersCreateDepositsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'deposits',
        'title_column' => 'dealer_id',
        'translatable' => true,
        'trashable' => false,
        'searchable' => false,
        'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'dealer' => [
            'required' => true,
        ],
        'date',
        'amount',
        'deposit_type',
        'note',
    ];

}
