<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleDealersCreateInvoicesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'invoices',
         'title_column' => 'name',
         'translatable' => false,
         'trashable' => false,
         'searchable' => false,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'dealer' => [
            'required' => true,
        ],
        'name' => [
            'translatable' => true,
            'required' => true,
        ],
        'amount' => [
            'required' => true,
        ],
        'date' => [
            'required' => true,
        ],
        'due_date' => [
            'required' => true,
        ],
        'invoice_file' => [
            'required' => true,
        ],
        'paid_date',
    ];

}
