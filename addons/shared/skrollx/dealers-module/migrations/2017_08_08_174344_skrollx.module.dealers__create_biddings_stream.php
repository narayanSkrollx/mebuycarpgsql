<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleDealersCreateBiddingsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'biddings',
        'title_column' => 'dealer_id',
        'translatable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'dealer' => [
            'required' => true,
        ],
        'car' => [
            'required' => true,
        ],
        'amount' => [
            'required' => true,
        ],
        'date' => [
            'required' => true,
        ],
        'won'
    ];

}
