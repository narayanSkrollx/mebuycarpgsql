<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class SkrollxModuleDealersCreateBillingsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'billings',
        'title_column' => 'dealer_id',
        'translatable' => true,
        'trashable' => false,
        'searchable' => false,
        'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'dealer' => [
            'required' => true,
        ],
        'date' => [
            'required' => true,
        ],
        'car_no' => [
            'required' => true,
        ],
        'invoice_no' => [
            'required' => true,
        ],
        'amount' => [
            'required' => true,
        ],
        'paid_date',
    ];

}
