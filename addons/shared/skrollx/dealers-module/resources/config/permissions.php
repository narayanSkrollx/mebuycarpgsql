<?php 
	
	return [
		'dealers' => [
			'manage',
			'read',
	        'write',
	        'delete',
		],
		'biddings' => [
			'manage',
			'read',
	        'write',
	        'delete',
		]
	];