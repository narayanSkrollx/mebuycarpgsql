$(function(){
  $('form.contact-form').validate({
    ignore: [],
    rules: {
      first_name: {
          minlength: 3,
          required: true
      },
      last_name: {
          minlength: 3,
          required: true
      },
      email: {
          required: true,
          email: true
      },
      passport: {
          required: true,
      },
      power_of_attorny: {
          required: true,
      },
      trade_license: {
          required: true,
      },
      traffic_file_code: {
          required: true,
      },
      timezone: {
          required: true,
      },
      phone_no: {
          required: true,
      },
      mobile_no: {
          required: true,
      }
    },
    messages: {
      first_name: false,
      last_name: false,
      power_of_attorny: false,
      trade_license: false,
      timezone: false,
      phone_no: {
      	required: false
      },
      mobile_no: {
      	required: false
      },
      email: {
        required: false,
      },
      password: {
        required: false,
      },
      contact_no: {
        required: false,
      }
    },
    highlight: function(element) {
      $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
      $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });
});