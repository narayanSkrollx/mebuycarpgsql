<?php

return [
    'title'       => 'تجار - وكلاء',
    'name'        => 'تجار - وكلاء وحدة',
    'description' => '',
    'section' => [
        'dealers' => 'تجار - وكلاء',
        'cities' => 'مدن',
        'deposits' => 'الودائع',
        'invoices' => 'الفواتير',
        'biddings' => 'المناقصات',
        'billings' => 'الفواتير',
    ]
];
