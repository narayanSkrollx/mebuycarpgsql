<?php

return [
    'new_dealer'  => 'تاجر جديد',
    'new_city'    => 'مدينة جديدة',
    'new_deposit' => 'إيداع جديد',
    'new_invoice' => 'فاتورة جديدة',
    'new_bidding' => 'عروض أسعار جديدة',
    'new_billing' => 'الفوترة الجديدة',
    'activate' => 'تفعيل',
    'deactivate' => 'عطل',
    'view_all_bids' => 'عرض جميع المقتطفات بيدينغس',
];
