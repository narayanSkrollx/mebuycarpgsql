<?php

return [
    'current_password' => [
        'name' => 'كلمة السر الحالي'
    ],
    'new_password' => [
        'name' => 'كلمة السر الجديد'
    ],
    'confirm_new_password' => [
        'name' => 'تأكيد كلمة المرور الجديد'
    ],
    'name' => [
        'name' => 'اس'
    ],
    'company_name' => [
        'name' => 'اسم الشركة',
        'placeholder' => 'اسم الشركة *',
    ],
    'company_address' => [
        'name' => 'عنوان الشارع',
        'placeholder' => 'عنوان الشارع *',
    ],
    'company_building' => [
        'name' => 'بناء، جناح، الخ',
        'placeholder' => 'بناء، جناح، الخ *',
    ],
    'company_country' => [
        'name' => 'بلد',
        'placeholder' => 'بلد *',
    ],
    'company_city' => [
        'name' => 'مدينة',
        'placeholder' => 'مدينة *',
    ],
    'company_state' => [
        'name' => 'حالة',
        'placeholder' => 'حالة *',
    ],
    'company_zip' => [
        'name' => 'الرمز البريدي',
        'placeholder' => 'الرمز البريدي *',
    ],
    'contact_no' => [
        'name' => 'رقم الاتصا'
    ],
    'phone_no' => [
        'name' => 'رقم الهات'
    ],
    'mobile_no' => [
        'name' => 'رقم المحمو'
    ],
    'passport' => [
        'name' => 'جواز سف'
    ],
    'power_of_attorny' => [
        'name' => 'قوة أتورن'
    ],
    'trade_license' => [
        'name' => 'الرخصة التجاري'
    ],
    'other_docs' => [
        'name' => 'مستندات أخر'
    ],
    'email' => [
        'name' => 'البريد الإلكترون'
    ],
    'traffic_file_code' => [
        'name' => 'رمز ملف حركة المرو'
    ],
    'timezone' => [
        'name' => 'وحدة زمنية',
        'placeholder' => 'حدد المنطقة الزمنية ',
    ],
    'country' => [
        'name' => 'بلد',
        'placeholder' => 'حدد الدولة'
    ],
    'shipping_country' => [
        'name' => 'بلد الشح'
    ],
    'shipping_city' => [
        'name' => 'الشحن المدين'
    ],
    'shipping_region' => [
        'name' => 'منطقة الشح'
    ],
    'shipping_building' => [
        'name' => 'بناء الشح'
    ],
    'shipping_apartment' => [
        'name' => 'الشحن شق'
    ],
    'shipping_note' => [
        'name' => 'ملاحظة الشح'
    ],
    'billing_country' => [
        'name' => 'بلد إرسال الفواتي'
    ],
    'billing_city' => [
        'name' => 'مدينة الفوتر'
    ],
    'billing_region' => [
        'name' => 'منطقة الفوتر'
    ],
    'billing_building' => [
        'name' => 'إنشاء الفواتي'
    ],
    'billing_apartment' => [
        'name' => 'شقة الفوتر'
    ],
    'billing_note' => [
        'name' => 'ملاحظة الفوتر'
    ],
    'city' => [
        'name' => 'مدينة',
        'placeholder' => 'اختر مدينة',
    ],
    'region' => [
        'name' => 'منطق'
    ],
    'building' => [
        'name' => 'بنا'
    ],
    'apartment' => [
        'name' => 'شق'
    ],
    'note' => [
        'name' => 'ملحوظ'
    ],
    'live' => [
        'name' => 'ح'
    ],
    'user' => [
        'name' => 'المستعمل',
        'placeholder' => 'اختر المستخدم'
    ],
    'dealer' => [
        'name' => 'تاجر',
        'placeholder' => 'حدد تاجر'
    ],
    'car' => [
        'name' => 'سيار'
    ],
    'make' => [
        'name' => 'يصنع',
        'placeholder' => 'حدد إجراء'
    ],
    'makes' => [
        'name' => 'يصنع',
        'placeholder' => 'حدد'
    ],
    'year' => [
        'name' => 'عام',
        'placeholder' => 'حدد السنة'
    ],
    'lang' => [
        'name' => 'لغة',
        'placeholder' => 'اختار اللغة',
        'options' => [
            'en' => 'English',
            'ar' => 'Arabic'
        ]
    ],
    'deposit_type' => [
        'name' => 'نوع الإيداع',
        'placeholder' => 'حدد نوع الإيداع ',
        'options' => [
            '1' => 'Bank Deposit',
            '2' => 'Cash',
            '3' => 'Cheque'
        ]
    ],
    'mileage' => [
        'name' => 'الأميال',
        'placeholder' => 'حدد الأميال'
    ],
    'date' => [
        'name' => 'العابر للتاريخ',
        'placeholder' => 'عبر التاريخ'
    ],
    'amount'       => [
        'name' => 'كمي'
    ],
    'due_date'       => [
        'name' => 'تاريخ الاستحقا'
    ],
    'paid_date'       => [
        'name' => 'تاريخ المدفوع'
    ],
    'transaction_date'       => [
        'name' => 'تاريخ الصفق'
    ],
    'invoice_file'       => [
        'name' => 'فاتور'
    ],
    'won'       => [
        'name' => 'وو'
    ],
    'car_no'       => [
        'name' => 'سيارة '
    ],
    'invoice_no'       => [
        'name' => 'فاتورة '
    ],
];
