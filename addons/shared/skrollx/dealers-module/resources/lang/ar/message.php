<?php

return [
    'password_changed' => 'تم تغيير كلمة المرور بنجاح.',
    'password_change_error' => 'مشكلة في تغيير كلمة المرور.',
    'incorrect_current_password' => 'كلمة المرور الحالية غير صحيحة.',
    'already_loggedin' => 'لقد سجلت دخولك من قبل. <a href="/"> انتقل إلى الصفحة الرئيسية </a>',
    'preferences_instructions' => 'ما هي المركبات التي تريد تلقي إشعارات بشأنها؟',
    'your_deposite_balance_is' => 'رصيد الإيداع هو:',
    'increase_deposit_instructions' => 'إذا قمت بزيادة الودائع الخاصة بك سوف تكون قادرة على تقديم عطاءات على مورe cars simultaniously',
    'no_deposits' => 'لم تودع الأموال بعد.',
    'no_invoices' => 'لا توجد فواتير في الوقت الحالي.',
    'no_billings' => 'لا توجد فواتير في الوقت الحالي.',
    'no_biddings' => 'لا يوجد عروض في الوقت الحالي.',
    'registration_complete_message' => 'نشكرك على استكمال عملية التسجيل. سيقوم أحد أعضاء فريقنا بمراجعة تفاصيلك والاتصال بك قريبا.',
    'subscribe_already_subscribed' => 'لقد اشتركت من قبل.',
    'subscribe_success' => 'اشترك في النشرة الإخبارية بنجاح.',
    'subscribe_failure' => 'تعذر الاشتراك في النشرة الإخبارية.',
];
