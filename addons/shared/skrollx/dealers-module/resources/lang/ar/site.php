<?php

return [
    'dashboard'                => 'لوحة القيادة',
    'contact_info'             => 'معلومات الاتصال',
    'personal_info'            => 'معلومات شخصية',
    'documents'                => 'مستندات',
    'notification_preferences' => 'تفضيلات الإعلام',
    'language_preferences'     => 'تفضيلات اللغة',
    'address'                  => 'عنوان',
    'shipping_address'         => 'عنوان الشحن',
    'billing_address'          => 'عنوان وصول الفواتير',
    'billing'                  => 'الفواتير',
    'deposit'                  => 'الوديعة',
    'invoice'                  => 'فاتورة',
    'bids'                     => 'العروض',
    'recent_bids'              => 'العروض',
    'change_password'          => 'تغيير كلمة السر',
    'authorized_signatory'     => 'المفوض بالتوقيع',
    'company_details'          => 'تفاصيل الشركة',
    'running'                  => 'جري',
];
