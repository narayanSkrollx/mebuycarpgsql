<?php

return [
    'title'       => 'Dealers',
    'name'        => 'Dealers Module',
    'description' => '',
    'section' => [
        'dealers' => 'Dealers',
        'cities' => 'Cities',
        'deposits' => 'Deposits',
        'invoices' => 'Invoices',
        'biddings' => 'Biddings',
        'billings' => 'Billings',
    ]
];
