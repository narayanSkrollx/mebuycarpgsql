<?php

return [
    'new_dealer'  => 'New Dealer',
    'new_city'    => 'New City',
    'new_deposit' => 'New Deposit',
    'new_invoice' => 'New Invoice',
    'new_bidding' => 'New Bidding',
    'new_billing' => 'New Billing',
    'activate' => 'Activate',
    'deactivate' => 'Deactivate',
    'view_all_bids' => 'View all biddings',
];
