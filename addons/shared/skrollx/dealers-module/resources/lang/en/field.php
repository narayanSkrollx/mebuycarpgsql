<?php

return [
    'current_password' => [
        'name' => 'Current Password'
    ],
    'new_password' => [
        'name' => 'New Password'
    ],
    'confirm_new_password' => [
        'name' => 'Confirm New Password'
    ],
    'name' => [
        'name' => 'Name'
    ],
    'company_name' => [
        'name' => 'Company name',
        'placeholder' => 'Company name *',
    ],
    'company_address' => [
        'name' => 'Street Address',
        'placeholder' => 'Street Address',
    ],
    'company_building' => [
        'name' => 'Building, Suite, Etc',
        'placeholder' => 'Building, Suite, Etc',
    ],
    'company_country' => [
        'name' => 'Nationality',
        'placeholder' => 'Nationality *',
    ],
    'company_city' => [
        'name' => 'City',
        'placeholder' => 'City *',
    ],
    'company_state' => [
        'name' => 'State',
        'placeholder' => 'State *',
    ],
    'company_zip' => [
        'name' => 'P.O. Box',
        'placeholder' => 'P.O. Box',
    ],
    'contact_no' => [
        'name' => 'Contact No'
    ],
    'phone_no' => [
        'name' => 'Phone No'
    ],
    'mobile_no' => [
        'name' => 'Mobile No'
    ],
    'passport' => [
        'name' => 'Passport'
    ],
    'power_of_attorny' => [
        'name' => 'Power of attorny'
    ],
    'trade_license' => [
        'name' => 'Trade license'
    ],
    'other_docs' => [
        'name' => 'Other docs'
    ],
    'email' => [
        'name' => 'Email'
    ],
    'traffic_file_code' => [
        'name' => 'Traffic file code'
    ],
    'timezone' => [
        'name' => 'Timezone',
        'placeholder' => 'Select Timezone',
    ],
    'country' => [
        'name' => 'Country',
        'placeholder' => 'Select Country'
    ],
    'shipping_country' => [
        'name' => 'Shipping country'
    ],
    'shipping_city' => [
        'name' => 'Shipping city'
    ],
    'shipping_region' => [
        'name' => 'Shipping region'
    ],
    'shipping_building' => [
        'name' => 'Shipping building'
    ],
    'shipping_apartment' => [
        'name' => 'Shipping apartment'
    ],
    'shipping_note' => [
        'name' => 'Shipping note'
    ],
    'billing_country' => [
        'name' => 'Billing country'
    ],
    'billing_city' => [
        'name' => 'Billing city'
    ],
    'billing_region' => [
        'name' => 'Billing region'
    ],
    'billing_building' => [
        'name' => 'Billing building'
    ],
    'billing_apartment' => [
        'name' => 'Billing apartment'
    ],
    'billing_note' => [
        'name' => 'Billing note'
    ],
    'city' => [
        'name' => 'City',
        'placeholder' => 'Select City',
    ],
    'region' => [
        'name' => 'Region'
    ],
    'building' => [
        'name' => 'Building'
    ],
    'apartment' => [
        'name' => 'Apartment'
    ],
    'note' => [
        'name' => 'Note'
    ],
    'live' => [
        'name' => 'Live'
    ],
    'user' => [
        'name' => 'User',
        'placeholder' => 'Select User'
    ],
    'dealer' => [
        'name' => 'Dealer',
        'placeholder' => 'Select Dealer'
    ],
    'car' => [
        'name' => 'Car'
    ],
    'make' => [
        'name' => 'Make',
        'placeholder' => 'Select Make'
    ],
    'makes' => [
        'name' => 'Makes',
        'placeholder' => 'Select Makes'
    ],
    'year' => [
        'name' => 'Year',
        'placeholder' => 'Select Year'
    ],
    'lang' => [
        'name' => 'Language',
        'placeholder' => 'Select Language',
        'options' => [
            'en' => 'English',
            'ar' => 'Arabic'
        ]
    ],
    'deposit_type' => [
        'name' => 'Deposit type',
        'placeholder' => 'Select Deposit type',
        'options' => [
            '1' => 'Bank Deposit',
            '2' => 'Cash',
            '3' => 'Cheque'
        ]
    ],
    'mileage' => [
        'name' => 'Mileage',
        'placeholder' => 'Select Mileage'
    ],
    'date' => [
        'name' => 'Trans-Date',
        'placeholder' => 'Trans-Date'
    ],
    'amount'       => [
        'name' => 'Amount'
    ],
    'due_date'       => [
        'name' => 'Due Date'
    ],
    'paid_date'       => [
        'name' => 'Paid Date'
    ],
    'transaction_date'       => [
        'name' => 'Transaction Date'
    ],
    'invoice_file'       => [
        'name' => 'Invoice'
    ],
    'won'       => [
        'name' => 'Won'
    ],
    'car_no'       => [
        'name' => 'Car #'
    ],
    'invoice_no'       => [
        'name' => 'Invoice #'
    ],
    'saved_cars'       => [
        'name' => 'Saved Cars'
    ],
    'emirates_id'       => [
        'name' => 'Emirates ID',
        'placeholder' => 'Emirates ID *'
    ],
];
