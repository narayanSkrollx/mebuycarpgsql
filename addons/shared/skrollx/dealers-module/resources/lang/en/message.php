<?php

return [
    'password_changed' => 'Password has been changed successfully.',
    'password_change_error' => 'Problem changing Password.',
    'incorrect_current_password' => 'Current Password is incorrect.',
    'already_loggedin' => 'You are already logged in. <a href="/">Go to homepage</a>',
    'preferences_instructions' => 'For which vehicles do you want to receive notifications?',
    'your_deposite_balance_is' => 'Your deposit balance is:',
    'increase_deposit_instructions' => 'If you increase your deposit you will be able to bid on more cars simultaniously',
    'no_deposits' => 'You have not deposited funds yet.',
    'no_invoices' => 'There are no invoices at the moment.',
    'no_billings' => 'There are no billing at the moment.',
    'no_biddings' => 'There are no bids at the moment.',
    'no_saved_cars' => 'You have not saved cars yet.',
    'car_save_success' => 'Car has been saved to your list.',
    'car_save_failed' => 'Something went wrong while saving car.',
    'car_remove_success' => 'Car has been removed from your list.',
    'registration_complete_message' => 'Thank your for completing the registration process. One of our team member will review your details and contact you shortly.',
    'subscribe_already_subscribed' => 'You have already suscribed.',
    'subscribe_success' => 'Subscribed to newsletter successfully.',
    'subscribe_failure' => 'Could not subscribe to newsletter.',
];
