<?php

return [
    'registration_completed' => [
        'subject'  => ':name has completed registration as a dealer.',
        'line1'  => 'A new dealer has registered. You have to review the provided information and approve registration from admin.',
    ],
    'account_activated' => [
        'greeting'  => 'Dear :name,',
        'subject'  => 'Dealer account has been activated.',
        'line1'  => 'Your account has been activated. You can login and get the best used car deals now.',
    ],
    'bill_generated' => [
        'greeting'  => 'Dear :name,',
        'subject'  => 'Bill has been regerated from mebuycar.com.',
        'line1'  => 'Bill has been generated and need your attention. Please check and let us know on your comments.',
    ],
    'subscriber_alert' => [
        'greeting'  => 'Dear :name,',
        'subject'  => 'A cars is available for biddings',
        'line1'  => 'A car is availabe for bidding matching your preference criteria.',
    ]
];
