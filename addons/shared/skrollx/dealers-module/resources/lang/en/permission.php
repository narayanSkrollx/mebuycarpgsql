<?php 

	return [
		'dealers' => [
			'name' => 'Dealers',
			'option' => [
				'read' => 'Can Read Dealers?',
				'manage' => 'Can Manage Dealers?',
				'write' => 'Can Create Dealers?',
				'delete' => 'Can Delete Dealers?'
			]
		],
		'biddings' => [
			'name' => 'Biddings',
			'option' => [
				'read' => 'Can Read Biddings?',
				'manage' => 'Can Manage Biddings?',
				'write' => 'Can Create Biddings?',
				'delete' => 'Can Delete Biddings?'
			]
		],
	];