<?php

return [
    'dashboard'                => 'Dashboard',
    'contact_info'             => '<span>Contact</span> Info',
    'personal_info'            => 'Personal Info',
    'documents'                => 'Documents',
    'notification_preferences' => 'Notification Preferences',
    'language_preferences'     => 'Language Preferences',
    'address'                  => 'Address',
    'shipping_address'         => 'Shipping Address',
    'billing_address'          => 'Billing Address',
    'billing'                  => 'Billing',
    'deposit'                  => 'Deposit',
    'invoice'                  => 'Invoice',
    'bids'                     => 'Bids',
    'recent_bids'              => 'Recent Bids',
    'change_password'          => 'Change Password',
    'authorized_signatory'     => 'Authorized Signatory',
    'company_details'          => 'Company Details',
    'running'                  => 'Running',
];
