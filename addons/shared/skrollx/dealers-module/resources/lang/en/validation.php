<?php

return [
    'password' => [
        'required' => 'Please enter Current Password'
    ],
    'new_password' => [
        'required' => 'Please enter New Password'
    ],
    'confirm_new_password' => [
        'required' => 'Please enter Confirm New Password.',
        'same' => '"New Password" and "Confirm New Password" must match.'
    ],
    'email' => [],
    'confirm_email' => [
        'equalTo' => 'Email and Confirm Email must match.'
    ],
];
