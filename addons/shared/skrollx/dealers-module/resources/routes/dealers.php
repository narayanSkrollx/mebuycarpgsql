<?php

return [
    'admin/dealers'                   => 'Skrollx\DealersModule\Http\Controller\Admin\DealersController@index',
    'admin/dealers/create'            => 'Skrollx\DealersModule\Http\Controller\Admin\DealersController@create',
    'admin/dealers/edit/{id}'         => 'Skrollx\DealersModule\Http\Controller\Admin\DealersController@edit',
    'admin/dealers/view/{id}'         => 'Skrollx\DealersModule\Http\Controller\Admin\DealersController@view',
    'admin/dealers/activate/{user_id}/{status}'         => 'Skrollx\DealersModule\Http\Controller\Admin\DealersController@activate',
    'admin/dealers/export'         => 'Skrollx\DealersModule\Http\Controller\Admin\DealersController@export',
    
    'dealer/login'                    => 'Skrollx\DealersModule\Http\Controller\DealersController@login',
    'dealer/register'                 => 'Skrollx\DealersModule\Http\Controller\DealersController@register',
    'dealer/complete-registration'                 => 'Skrollx\DealersModule\Http\Controller\DealersController@completeRegistration',
    'dealer/dashboard'                => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@index',
    'dealer/contact-info'             => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@contactInfo',
    'dealer/edit-contact-info'             => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@editContactInfo',
    'ajax/upload-document'             => 'Skrollx\DealersModule\Http\Controller\AjaxController@uploadDocument',
    'dealer/notification-preferences' => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@notificationPreferences',
    'dealer/address'                  => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@address',
    'dealer/edit-address'                  => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@editAddress',
    'dealer/billing'                  => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@billing',
    'dealer/deposit'                  => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@deposit',
    'dealer/invoice'                  => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@invoice',
    'dealer/bids'                     => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@bids',
    'dealer/change-password'          => 'Skrollx\DealersModule\Http\Controller\Dealer\PasswordsController@changePassword',
    
    'dealer/save-car/{id}'          => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@saveCar',
    'dealer/remove-car/{id}'          => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@removeCar',
    'dealer/saved-cars'          => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@savedCars',
    'dealer/bid-car/{id}'          => 'Skrollx\DealersModule\Http\Controller\Dealer\DealersController@bidCar',

    'ajax/city-dropdown'                     => 'Skrollx\DealersModule\Http\Controller\AjaxController@cityDropdown',
    'ajax/subscribe'                     => 'Skrollx\DealersModule\Http\Controller\AjaxController@subscribe',
];
