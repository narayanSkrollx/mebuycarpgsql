<?php namespace Skrollx\DealersModule\Bidding;

use Anomaly\SettingsModule\Setting\SettingRepository;
use Anomaly\Streams\Platform\Model\Dealers\DealersBiddingsEntryModel;
use Anomaly\Streams\Platform\Support\Currency;
use Skrollx\DealersModule\Bidding\BiddingRepository;
use Skrollx\DealersModule\Bidding\Contract\BiddingInterface;

class BiddingModel extends DealersBiddingsEntryModel implements BiddingInterface
{
    
}
