<?php namespace Skrollx\DealersModule\Bidding;

use Skrollx\DealersModule\Bidding\Contract\BiddingRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class BiddingRepository extends EntryRepository implements BiddingRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var BiddingModel
     */
    protected $model;

    /**
     * Create a new BiddingRepository instance.
     *
     * @param BiddingModel $model
     */
    public function __construct(BiddingModel $model)
    {
        $this->model = $model;
    }

}
