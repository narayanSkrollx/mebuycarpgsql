<?php namespace Skrollx\DealersModule\Bidding\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface BiddingRepositoryInterface extends EntryRepositoryInterface
{

}
