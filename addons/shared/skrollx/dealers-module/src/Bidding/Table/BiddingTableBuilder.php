<?php namespace Skrollx\DealersModule\Bidding\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class BiddingTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        'car',
        'dealer',
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'dealer',
        'car',
        'amount',
        'date',
        'won',
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit'
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}
