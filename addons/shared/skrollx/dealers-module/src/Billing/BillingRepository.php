<?php namespace Skrollx\DealersModule\Billing;

use Skrollx\DealersModule\Billing\Contract\BillingRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class BillingRepository extends EntryRepository implements BillingRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var BillingModel
     */
    protected $model;

    /**
     * Create a new BillingRepository instance.
     *
     * @param BillingModel $model
     */
    public function __construct(BillingModel $model)
    {
        $this->model = $model;
    }
}
