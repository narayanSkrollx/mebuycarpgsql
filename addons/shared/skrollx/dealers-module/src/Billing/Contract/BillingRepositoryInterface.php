<?php namespace Skrollx\DealersModule\Billing\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface BillingRepositoryInterface extends EntryRepositoryInterface
{

}
