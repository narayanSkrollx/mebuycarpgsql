<?php namespace Skrollx\DealersModule\Billing\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;
use Skrollx\DealersModule\Billing\Mail\BillGenerated;

class BillingFormBuilder extends FormBuilder
{

    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [];

    /**
     * Additional validation rules.
     *
     * @var array|string
     */
    protected $rules = [];

    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [];

    /**
     * The form actions.
     *
     * @var array|string
     */
    protected $actions = [];

    /**
     * The form buttons.
     *
     * @var array|string
     */
    protected $buttons = [];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The form sections.
     *
     * @var array
     */
    protected $sections = [];

    /**
     * The form assets.
     *
     * @var array
     */
    protected $assets = [];

    public function onSaved()
    {
        $entry = $this->getFormEntry();
        $to_email = $entry->dealer->user->email;
        \Mail::to($to_email)->send(new BillGenerated($entry));
    }
}
