<?php namespace Skrollx\DealersModule\Billing\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BillGenerated extends Mailable
{
    use SerializesModels;

    public $billing = null;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($billing)
    {
        $this->billing = $billing;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view(
                'skrollx.module.dealers::emails.bill-generated', 
                [
                    'billing' => $this->billing, 
                ]
            )->subject(trans('skrollx.module.dealers::notification.bill_generated.subject'));
    }
}
