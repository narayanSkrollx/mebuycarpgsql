<?php namespace Skrollx\DealersModule\City;

use Skrollx\DealersModule\City\Contract\CityRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class CityRepository extends EntryRepository implements CityRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var CityModel
     */
    protected $model;

    /**
     * Create a new CityRepository instance.
     *
     * @param CityModel $model
     */
    public function __construct(CityModel $model)
    {
        $this->model = $model;
    }
}
