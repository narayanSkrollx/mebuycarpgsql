<?php namespace Skrollx\DealersModule\City;

use Skrollx\DealersModule\City\Contract\CityRepositoryInterface;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class CitySeeder
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 * @package       Skrollx\JobsModule\Seeder
 */
class CitySeeder extends Seeder
{

    /**
     * The category repository.
     *
     * @var CityRepositoryInterface
     */
    protected $cities;

    /**
     * Create a new CitySeeder instance.
     *
     * @param CityRepositoryInterface $cities
     */
    public function __construct(CityRepositoryInterface $cities)
    {
        $this->cities = $cities;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        $this->cities->truncate();

        $this->cities->create(
            [
                'en'   => [
                    'name'     => 'Dubai'
                ],
                'country' => "AE",
                'live' => 1,
            ]
        );

        $this->cities->create(
            [
                'en'   => [
                    'name'     => 'Abu Dhabi'
                ],
                'country' => "AE",
                'live' => 1,
            ]
        );

        $this->cities->create(
            [
                'en'   => [
                    'name'     => 'Sharjah'
                ],
                'country' => "AE",
                'live' => 1,
            ]
        );

        $this->cities->create(
            [
                'en'   => [
                    'name'     => 'Ajman'
                ],
                'country' => "AE",
                'live' => 1,
            ]
        );

        $this->cities->create(
            [
                'en'   => [
                    'name'     => 'Fujairah'
                ],
                'country' => "AE",
                'live' => 1,
            ]
        );

        $this->cities->create(
            [
                'en'   => [
                    'name'     => 'Umm al-Quwain'
                ],
                'country' => "AE",
                'live' => 1,
            ]
        );

        $this->cities->create(
            [
                'en'   => [
                    'name'     => 'Ras Al Khaimah'
                ],
                'country' => "AE",
                'live' => 1,
            ]
        );
    }
}
