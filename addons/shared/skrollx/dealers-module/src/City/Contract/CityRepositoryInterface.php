<?php namespace Skrollx\DealersModule\City\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface CityRepositoryInterface extends EntryRepositoryInterface
{

}
