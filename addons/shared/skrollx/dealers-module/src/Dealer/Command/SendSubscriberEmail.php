<?php namespace Skrollx\DealersModule\Dealer\Command;

use Illuminate\Console\Command;
use Skrollx\CarsModule\Car\CarModel;
use Skrollx\DealersModule\Preference\PreferenceModel;

class SendSubscriberEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'emails:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email to subscribers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cars = CarModel::biddable()->postedLately()->get();
        if($cars->count() == 0) return;

        foreach($cars as $car){
            $subscriptionOptions = [
                'country' => $car->registered_in,
                'make_id' => $car->make_id,
                'year' => $car->year,
                'mileage' => $car->mileage,
                'lang' => config('app.locale'),
            ];
            $subscribers = PreferenceModel::getSubscribers($subscriptionOptions);

            if($subscribers->count() == 0) continue;

            foreach($subscribers as $subscriber){
                $user = $subscriber->createdBy;
                $data['subscriber'] = $subscriber;
                $data['car'] = $car;
                $data['greeting'] = 'Hi '.$user->name().',';
                $to = $user->email;
                \Mail::send('skrollx.module.dealers::emails.subscriber-alert', $data, function ($message) use($to) {

                    $message->from('noreply@mebuycar.com', 'MeBuyCar');

                    $message->to($to)->subject('MeBuyCar.com - A cars is available for biddings');

                });
                $this->info('Email send successfully to '.$to.'!');
            }
        }
        $this->info('Operation completed!');
    }
}
