<?php namespace Skrollx\DealersModule\Dealer\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface DealerRepositoryInterface extends EntryRepositoryInterface
{

}
