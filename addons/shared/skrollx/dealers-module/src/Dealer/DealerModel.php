<?php namespace Skrollx\DealersModule\Dealer;

use Anomaly\Streams\Platform\Model\Dealers\DealersDealersEntryModel;
use Skrollx\DealersModule\Bidding\BiddingModel;
use Skrollx\DealersModule\Dealer\Contract\DealerInterface;
use Skrollx\DealersModule\Deposit\DepositModel;

class DealerModel extends DealersDealersEntryModel implements DealerInterface
{
    public function getBalance()
    {
        return $depositTotal = DepositModel::where('dealer_id', $this->id)->sum('amount');
        /* we won't reduce balance from dealer. so returning actual deposits as balance*/
        
        $biddingsTotal = BiddingModel::where('dealer_id', $this->id)->sum('amount');
        $availableBalance = (int)$depositTotal - (int)$biddingsTotal;
        // if($availableBalance < 0){
        //     return 0;
        // }
        return $availableBalance;
    }
}
