<?php namespace Skrollx\DealersModule\Dealer;

use Skrollx\DealersModule\Dealer\Contract\DealerRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class DealerRepository extends EntryRepository implements DealerRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var DealerModel
     */
    protected $model;

    /**
     * Create a new DealerRepository instance.
     *
     * @param DealerModel $model
     */
    public function __construct(DealerModel $model)
    {
        $this->model = $model;
    }
}
