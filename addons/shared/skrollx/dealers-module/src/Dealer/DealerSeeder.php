<?php namespace Skrollx\DealersModule\Dealer;

use Anomaly\FilesModule\Folder\FolderModel;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Anomaly\UsersModule\Role\RoleModel;
use Skrollx\DealersModule\Dealer\Contract\DealerRepositoryInterface;

class DealerSeeder extends Seeder
{
    /**
     * The category repository.
     *
     * @var RoleRepositoryInterface
     */
    protected $dealers;

    /**
     * Create a new RoleSeeder instance.
     *
     * @param RoleRepositoryInterface $dealers
     */
    public function __construct(DealerRepositoryInterface $dealers)
    {
        $this->dealers = $dealers;
    }
    
    /**
     * Run the seeder.
     */
    public function run()
    {
        $this->dealers->truncate();
        $faker = \Faker\Factory::create();

        $users = RoleModel::where('slug', 'dealer')->first()->users()->get();

        foreach($users as $user){
            $data = [
                'company_name' => $faker->company,
                'phone_no' => '123456789',
                'user_id' => $user->id,
                'mobile_no' => '123456789',
                'email' => $user->email,
            ];
            $dealer = $this->dealers->create($data);
            $dealer->setAttribute('created_by_id', $user->id)->save();
        }
    }
}
