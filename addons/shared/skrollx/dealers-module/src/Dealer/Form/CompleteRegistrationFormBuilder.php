<?php namespace Skrollx\DealersModule\Dealer\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;
use Skrollx\DealersModule\Dealer\Mail\RegistrationCompleted;

class CompleteRegistrationFormBuilder extends FormBuilder
{

    protected $model = 'Skrollx\DealersModule\Dealer\DealerModel';

    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [   
        'first_name' => [
            'type' => 'anomaly.field_type.text'
        ],
        'last_name' => [
            'type' => 'anomaly.field_type.text'
        ],
        'passport',
        'power_of_attorny',
        'trade_license',
        'other_docs',
        'company_address',
        'company_building',
        'company_country',
        'company_city',
        'company_state',
        'company_zip',
        'user',
        'emirates_id'
    ];

    protected $rules = [
        'first_name' => [
            'required'
        ],
        'last_name' => [
            'required'
        ],
        'passport' => [
            'required'
        ],
        'trade_license' => [
            'required'
        ],
        'company_country' => [
            'required'
        ],
        'company_city' => [
            'required'
        ],
        'emirates_id' => [
            'required'
        ],
    ];
    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = ['company_state'];

    /**
     * The form actions.
     *
     * @var array|string
     */
    protected $actions = [];

    /**
     * The form buttons.
     *
     * @var array|string
     */
    protected $buttons = [];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The form sections.
     *
     * @var array
     */
    protected $sections = [];

    /**
     * The form assets.
     *
     * @var array
     */
    protected $assets = [];

    public function onPost()
    {
        $this->setSkips(['first_name', 'last_name']);
    }

    public function onPosted()
    {
        $entry = $this->getFormEntry();
        $user = $entry->user;
        if($user){
            $user->first_name = \Request::get('first_name');
            $user->last_name = \Request::get('last_name');
            $user->display_name = $user->first_name .' '. $user->last_name;
            $user->save();
        }
    }

    public function onSaved()
    {
        $entry = $this->getFormEntry();
        $to_email = env('DEALER_EMAIL', 'admin@mebuycar.com');
        \Mail::to($to_email)->send(new RegistrationCompleted($entry));
        session()->forget('user_id');
    }

}
