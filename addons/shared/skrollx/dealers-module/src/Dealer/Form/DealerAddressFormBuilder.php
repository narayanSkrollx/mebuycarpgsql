<?php namespace Skrollx\DealersModule\Dealer\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class DealerAddressFormBuilder extends FormBuilder
{

    protected $model = 'Skrollx\DealersModule\Dealer\DealerModel';

    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [
        'user',
        'shipping_country',
        'shipping_city',
        'shipping_region',
        'shipping_building',
        'shipping_apartment',
        'shipping_note',
        'billing_country',
        'billing_city',
        'billing_region',
        'billing_building',
        'billing_apartment',
        'billing_note'
    ];

    protected $rules = [
        'shipping_country' => [
            'required'
        ],
        'shipping_city' => [
            'required'
        ],
        'shipping_region' => [
            'required'
        ],
        'shipping_building' => [
            'required'
        ],
        'shipping_apartment' => [
            'required'
        ],
        'billing_country' => [
            'required',
        ],
        'billing_city' => [
            'required'
        ],
        'billing_region' => [
            'required'
        ],
        'billing_building' => [
            'required'
        ],
        'billing_apartment' => [
            'required'
        ]
    ];
    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [];

    /**
     * The form actions.
     *
     * @var array|string
     */
    protected $actions = [];

    /**
     * The form buttons.
     *
     * @var array|string
     */
    protected $buttons = [];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The form sections.
     *
     * @var array
     */
    protected $sections = [];

    /**
     * The form assets.
     *
     * @var array
     */
    protected $assets = [];

}
