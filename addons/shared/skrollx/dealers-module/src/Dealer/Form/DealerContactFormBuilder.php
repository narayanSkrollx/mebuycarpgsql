<?php namespace Skrollx\DealersModule\Dealer\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class DealerContactFormBuilder extends FormBuilder
{

    protected $model = 'Skrollx\DealersModule\Dealer\DealerModel';

    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [
        'first_name' => [
            'type' => 'anomaly.field_type.text'
        ],
        'last_name' => [
            'type' => 'anomaly.field_type.text'
        ],
        'company_name',
        'passport',
        'power_of_attorny',
        'trade_license',
        'other_docs',
        'traffic_file_code',
        'email' => [
            'type' => 'anomaly.field_type.email'
        ],
        'timezone',
        'phone_no',
        'mobile_no',
        'user'
    ];

    protected $rules = [
        'first_name' => [
            'required'
        ],
        'last_name' => [
            'required'
        ],
        'company_name' => [
            'required'
        ],
        'passport' => [
            'required'
        ],
        'power_of_attorny' => [
            'required'
        ],
        'trade_license' => [
            'required'
        ],
        'other_docs',
        'email' => [
            'required',
            'email'
        ],
        'traffic_file_code' => [
            'required'
        ],
        'timezone' => [
            'required'
        ],
        'phone_no' => [
            'required'
        ],
        'mobile_no' => [
            'required'
        ]
    ];
    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [];

    /**
     * The form actions.
     *
     * @var array|string
     */
    protected $actions = [];

    /**
     * The form buttons.
     *
     * @var array|string
     */
    protected $buttons = [];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The form sections.
     *
     * @var array
     */
    protected $sections = [];

    /**
     * The form assets.
     *
     * @var array
     */
    protected $assets = [];

    public function onPost()
    {
        $this->addRules('email', [
            'required',
            'email',
            'unique:users_users,email,'.auth()->id().'id'
        ]);

        $this->setSkips(['first_name', 'last_name']);
    }

}
