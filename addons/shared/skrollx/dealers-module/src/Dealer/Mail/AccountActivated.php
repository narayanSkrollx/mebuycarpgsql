<?php namespace Skrollx\DealersModule\Dealer\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AccountActivated extends Mailable
{
    use SerializesModels;

    public $user = null;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $name = $this->user->first_name.' '.$this->user->last_name;
        return $this->view(
                'skrollx.module.dealers::emails.account-activated', 
                [
                    'greeting' => trans('skrollx.module.dealers::notification.account_activated.greeting', ['name' => $name]), 
                    'user' => $this->user,
                ]
            )->subject(trans('skrollx.module.dealers::notification.account_activated.subject', 
                [
                    'name' => $name
                ]
            ));
    }
}
