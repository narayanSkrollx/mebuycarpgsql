<?php namespace Skrollx\DealersModule\Dealer\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegistrationCompleted extends Mailable
{
    use SerializesModels;

    public $dealer = null;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dealer)
    {
        $this->dealer = $dealer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view(
                'skrollx.module.dealers::emails.registration-completed', 
                [
                    'dealer' => $this->dealer, 
                ]
            )->subject(trans('skrollx.module.dealers::notification.registration_completed.subject', 
                [
                    'name' => \Request::get('first_name').' '.\Request::get('last_name')
                ]
            ));
    }
}
