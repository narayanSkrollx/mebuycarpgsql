<?php namespace Skrollx\DealersModule\Dealer\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class DealerTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        'company_name',
        'mobile_no',
        'phone_no',
        'email',
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'company_name',
        'user',
        'mobile_no',
        'phone_no',
        'entry.user.email',
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit',
        'view',
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

}
