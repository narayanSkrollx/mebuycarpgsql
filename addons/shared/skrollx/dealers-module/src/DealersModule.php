<?php namespace Skrollx\DealersModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class DealersModule extends Module
{

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-handshake-o';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'dealers' => [
            'buttons' => [
                // 'new_dealer' => [
                //     'enabled' => 'admin/dealers'
                // ],
                'export' => [
                    'class' => 'btn btn-info',
                    'icon' => 'fa fa-file-excel-o',
                    'href' => 'admin/dealers/export',
                    'enabled' => 'admin/dealers'
                ],
            ],
        ],
        'cities' => [
            'buttons' => [
                'new_city',
            ]
        ],
        'deposits' => [
            'buttons' => [
                'new_deposit',
            ],
        ],
        'invoices' => [
            'buttons' => [
                'new_invoice',
            ],
        ],
        // 'biddings' => [
        //     'buttons' => [
        //         'new_bidding',
        //     ],
        // ],
        'billings' => [
            'buttons' => [
                'new_billing',
            ],
        ],
    ];
}
