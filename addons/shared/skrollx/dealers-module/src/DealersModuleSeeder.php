<?php namespace Skrollx\DealersModule;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Skrollx\DealersModule\City\CitySeeder;
use Skrollx\DealersModule\Dealer\DealerSeeder;
use Skrollx\DealersModule\Seeder\FolderSeeder;
use Skrollx\DealersModule\Seeder\UserSeeder;
/**
 * Class TripsModuleSeeder
 */
class DealersModuleSeeder extends Seeder
{

    /**
     * Run the seeder.
     */
    public function run()
    {
        // $this->call(CitySeeder::class);
        // $this->call(UserSeeder::class);
        // $this->call(DealerSeeder::class);
        // $this->call(FolderSeeder::class);
    }
}