<?php namespace Skrollx\DealersModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Skrollx\DealersModule\Billing\Contract\BillingRepositoryInterface;
use Skrollx\DealersModule\Billing\BillingRepository;
use Anomaly\Streams\Platform\Model\Dealers\DealersBillingsEntryModel;
use Skrollx\DealersModule\Billing\BillingModel;
use Skrollx\DealersModule\Bidding\Contract\BiddingRepositoryInterface;
use Skrollx\DealersModule\Bidding\BiddingRepository;
use Anomaly\Streams\Platform\Model\Dealers\DealersBiddingsEntryModel;
use Skrollx\DealersModule\Bidding\BiddingModel;
use Anomaly\Streams\Platform\Model\Dealers\DealersCitiesEntryModel;
use Anomaly\Streams\Platform\Model\Dealers\DealersDepositsEntryModel;
use Anomaly\Streams\Platform\Model\Dealers\DealersInvoicesEntryModel;
use Anomaly\Streams\Platform\Model\Dealers\DealersPreferencesEntryModel;
use Anomaly\UsersModule\User\UserModel;
use Skrollx\DealersModule\City\CityModel;
use Skrollx\DealersModule\City\CityRepository;
use Skrollx\DealersModule\City\Contract\CityRepositoryInterface;
use Skrollx\DealersModule\Dealer\Contract\DealerRepositoryInterface;
use Skrollx\DealersModule\Dealer\DealerModel;
use Skrollx\DealersModule\Dealer\DealerRepository;
use Skrollx\DealersModule\Deposit\Contract\DepositRepositoryInterface;
use Skrollx\DealersModule\Deposit\DepositModel;
use Skrollx\DealersModule\Deposit\DepositRepository;
use Skrollx\DealersModule\Invoice\Contract\InvoiceRepositoryInterface;
use Skrollx\DealersModule\Invoice\InvoiceModel;
use Skrollx\DealersModule\Invoice\InvoiceRepository;
use Skrollx\DealersModule\Preference\Contract\PreferenceRepositoryInterface;
use Skrollx\DealersModule\Preference\PreferenceModel;
use Skrollx\DealersModule\Preference\PreferenceRepository;

class DealersModuleServiceProvider extends AddonServiceProvider
{

    protected $plugins = [];

    protected $commands = [
        '\Skrollx\DealersModule\Dealer\Command\SendSubscriberEmail',
    ];

    protected $schedules = [
        'everyMinute' => [
            \Skrollx\DealersModule\Dealer\Command\SendSubscriberEmail::class
        ]
    ];

    protected $api = [];

    protected $routes = [
        'admin/dealers/billings'           => 'Skrollx\DealersModule\Http\Controller\Admin\BillingsController@index',
        'admin/dealers/billings/create'    => 'Skrollx\DealersModule\Http\Controller\Admin\BillingsController@create',
        'admin/dealers/billings/edit/{id}' => 'Skrollx\DealersModule\Http\Controller\Admin\BillingsController@edit',
        'admin/dealers/biddings'           => 'Skrollx\DealersModule\Http\Controller\Admin\BiddingsController@index',
        'admin/dealers/biddings/create'    => 'Skrollx\DealersModule\Http\Controller\Admin\BiddingsController@create',
        'admin/dealers/biddings/edit/{id}' => 'Skrollx\DealersModule\Http\Controller\Admin\BiddingsController@edit',
        'admin/dealers/invoices'           => 'Skrollx\DealersModule\Http\Controller\Admin\InvoicesController@index',
        'admin/dealers/invoices/create'    => 'Skrollx\DealersModule\Http\Controller\Admin\InvoicesController@create',
        'admin/dealers/invoices/edit/{id}' => 'Skrollx\DealersModule\Http\Controller\Admin\InvoicesController@edit',
        'admin/dealers/deposits'           => 'Skrollx\DealersModule\Http\Controller\Admin\DepositsController@index',
        'admin/dealers/deposits/create'    => 'Skrollx\DealersModule\Http\Controller\Admin\DepositsController@create',
        'admin/dealers/deposits/edit/{id}' => 'Skrollx\DealersModule\Http\Controller\Admin\DepositsController@edit',
        'admin/dealers/preferences'           => 'Skrollx\DealersModule\Http\Controller\Admin\PreferencesController@index',
        'admin/dealers/preferences/create'    => 'Skrollx\DealersModule\Http\Controller\Admin\PreferencesController@create',
        'admin/dealers/preferences/edit/{id}' => 'Skrollx\DealersModule\Http\Controller\Admin\PreferencesController@edit',
        'admin/dealers/cities'           => 'Skrollx\DealersModule\Http\Controller\Admin\CitiesController@index',
        'admin/dealers/cities/create'    => 'Skrollx\DealersModule\Http\Controller\Admin\CitiesController@create',
        'admin/dealers/cities/edit/{id}' => 'Skrollx\DealersModule\Http\Controller\Admin\CitiesController@edit',
    ];

    protected $middleware = [];

    protected $routeMiddleware = [];

    protected $listeners = [];

    protected $aliases = [];

    protected $bindings = [
        DealersBillingsEntryModel::class => BillingModel::class,
        DealersBiddingsEntryModel::class => BiddingModel::class,
        DealersInvoicesEntryModel::class => InvoiceModel::class,
        DealersDepositsEntryModel::class => DepositModel::class,
        DealersPreferencesEntryModel::class => PreferenceModel::class,
        DealersCitiesEntryModel::class      => CityModel::class,
        'complete_registration_form'               => 'Skrollx\DealersModule\Dealer\Form\CompleteRegistrationFormBuilder',
        'dealer_contact_form'               => 'Skrollx\DealersModule\Dealer\Form\DealerContactFormBuilder',
        'dealer_address_form'               => 'Skrollx\DealersModule\Dealer\Form\DealerAddressFormBuilder',
        'dealer_preference_form'            => 'Skrollx\DealersModule\Preference\Form\PreferenceFormBuilder',
    ];

    protected $providers = [];

    protected $singletons = [
        BillingRepositoryInterface::class => BillingRepository::class,
        BiddingRepositoryInterface::class => BiddingRepository::class,
        DealerRepositoryInterface::class => DealerRepository::class,
        InvoiceRepositoryInterface::class => InvoiceRepository::class,
        DepositRepositoryInterface::class => DepositRepository::class,
        PreferenceRepositoryInterface::class => PreferenceRepository::class,
        CityRepositoryInterface::class => CityRepository::class,
    ];

    protected $overrides = [];

    protected $mobile = [];

    public function register(UserModel $user, DealerModel $profile)
    {
        $user->bind(
            'dealer',
            function () {

                /* @var UserModel $this */
                return $this
                    ->hasOne(DealerModel::class, 'user_id')
                    ->orderBy('created_at', 'DESC')
                    ->first();
            }
        );

    }

    public function map()
    {
    }

}
