<?php namespace Skrollx\DealersModule\Deposit\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface DepositRepositoryInterface extends EntryRepositoryInterface
{

}
