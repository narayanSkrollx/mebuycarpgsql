<?php namespace Skrollx\DealersModule\Deposit;

use Skrollx\DealersModule\Deposit\Contract\DepositInterface;
use Anomaly\Streams\Platform\Model\Dealers\DealersDepositsEntryModel;

class DepositModel extends DealersDepositsEntryModel implements DepositInterface
{
    
}
