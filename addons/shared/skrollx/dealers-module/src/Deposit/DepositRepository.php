<?php namespace Skrollx\DealersModule\Deposit;

use Skrollx\DealersModule\Deposit\Contract\DepositRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class DepositRepository extends EntryRepository implements DepositRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var DepositModel
     */
    protected $model;

    /**
     * Create a new DepositRepository instance.
     *
     * @param DepositModel $model
     */
    public function __construct(DepositModel $model)
    {
        $this->model = $model;
    }
}
