<?php namespace Skrollx\DealersModule\Deposit\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class DepositTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        'dealer'
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'dealer',
        'date',
        'amount',
        'name' => [
            'heading'     => 'skrollx.module.dealers::field.deposit_type.name',
            'wrapper'     => 'skrollx.module.dealers::field.deposit_type.options.{entry.deposit_type}',
        ]
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit'
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete'
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [];

    protected $rules = [
        'dealer' => [
            'required' => true,
        ],
        'date' => [
            'required' => true,
        ],
        'amount' => [
            'required' => true,
        ],
        'deposit_type' => [
            'required' => true,
        ]
    ];

}
