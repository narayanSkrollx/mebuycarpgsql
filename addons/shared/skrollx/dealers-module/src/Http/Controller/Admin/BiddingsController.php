<?php namespace Skrollx\DealersModule\Http\Controller\Admin;

use Skrollx\DealersModule\Bidding\Form\BiddingFormBuilder;
use Skrollx\DealersModule\Bidding\Table\BiddingTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class BiddingsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param BiddingTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(BiddingTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param BiddingFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(BiddingFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param BiddingFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(BiddingFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
