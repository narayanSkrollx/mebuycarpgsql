<?php namespace Skrollx\DealersModule\Http\Controller\Admin;

use Skrollx\DealersModule\Billing\Form\BillingFormBuilder;
use Skrollx\DealersModule\Billing\Table\BillingTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class BillingsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param BillingTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(BillingTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param BillingFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(BillingFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param BillingFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(BillingFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
