<?php namespace Skrollx\DealersModule\Http\Controller\Admin;

use Skrollx\DealersModule\City\Form\CityFormBuilder;
use Skrollx\DealersModule\City\Table\CityTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class CitiesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param CityTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(CityTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param CityFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(CityFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param CityFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(CityFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
