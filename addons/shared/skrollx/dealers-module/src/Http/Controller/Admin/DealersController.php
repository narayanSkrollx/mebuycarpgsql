<?php namespace Skrollx\DealersModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Anomaly\UsersModule\User\UserModel;
use Skrollx\DealersModule\Dealer\DealerModel;
use Skrollx\DealersModule\Dealer\Form\DealerFormBuilder;
use Skrollx\DealersModule\Dealer\Mail\AccountActivated;
use Skrollx\DealersModule\Dealer\Table\DealerTableBuilder;
use Illuminate\Routing\ResponseFactory;

class DealersController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param DealerTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(DealerTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param DealerFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(DealerFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param DealerFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(DealerFormBuilder $form, $id)
    {
        return $form->render($id);
    }

    /**
     * View an existing entry.
     *
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function view($id)
    {
        $dealer = DealerModel::find($id);
        $this->breadcrumbs->add($dealer->company_name);
        return view('module::admin/view', compact('dealer'));
    }

    /**
     * Activate deactivate dealer login
     *
     * @param $user_id
     * @return void
     */
    public function activate($user_id, $status = 1)
    {
        $user = UserModel::find($user_id);
        if(!$user){
            return redirect()->back()->with('error', ['User doesn\'t exist']);
        }
        $user->activated = (bool)$status;
        $user->save();
        if($status == 1){
            $to_email = $user->email;
            \Mail::to($to_email)->send(new AccountActivated($user));
        }
        $msg = $status ? 'User activated successfully.' : 'User deactivated successfully.';
        return redirect()->back()->with('success', [$msg]);
    }

    /**
     * Export to csv
     * @param  ResponseFactory $response
     * @return [type]                    [description]
     */
    public function export(ResponseFactory $response)
    {
        $data = DealerModel::select(
            'dealers_dealers.*',
            'users_users.display_name as name',
            'users_users.email')
            ->leftJoin('users_users', 'users_users.id', 'dealers_dealers.created_by_id')
        ->get();
        
        $headers = [
            'Content-Disposition' => 'attachment; filename=mebuycar-dealers.csv',
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Pragma'              => 'public',
            'Expires'             => '0',
        ];

        $callback = function () use ($data) {
            $output = fopen('php://output', 'w');

            /* @var EloquentModel $entry */
            foreach ($data->all() as $k => $entry) {
                if ($k == 0) {
                    fputcsv($output, array_keys($entry->toArray()));
                }

                fputcsv($output, $entry->toArray());
            }

            fclose($output);
        };

        return $response->stream($callback, 200, $headers);
    }
}
