<?php namespace Skrollx\DealersModule\Http\Controller\Admin;

use Skrollx\DealersModule\Deposit\Form\DepositFormBuilder;
use Skrollx\DealersModule\Deposit\Table\DepositTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class DepositsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param DepositTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(DepositTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param DepositFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(DepositFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param DepositFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(DepositFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
