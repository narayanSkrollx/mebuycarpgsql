<?php namespace Skrollx\DealersModule\Http\Controller\Admin;

use Skrollx\DealersModule\Invoice\Form\InvoiceFormBuilder;
use Skrollx\DealersModule\Invoice\Table\InvoiceTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class InvoicesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param InvoiceTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(InvoiceTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param InvoiceFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(InvoiceFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param InvoiceFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(InvoiceFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
