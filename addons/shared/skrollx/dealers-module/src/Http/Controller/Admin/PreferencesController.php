<?php namespace Skrollx\DealersModule\Http\Controller\Admin;

use Skrollx\DealersModule\Preference\Form\PreferenceFormBuilder;
use Skrollx\DealersModule\Preference\Table\PreferenceTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class PreferencesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param PreferenceTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PreferenceTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param PreferenceFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(PreferenceFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param PreferenceFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(PreferenceFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
