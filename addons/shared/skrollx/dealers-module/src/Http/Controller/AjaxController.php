<?php namespace Skrollx\DealersModule\Http\Controller;

use Anomaly\FilesModule\Folder\Contract\FolderRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Illuminate\Contracts\Config\Repository;
use Skrollx\DealersModule\City\CityModel;
use Skrollx\DealersModule\Dealer\Form\DealerFormBuilder;
use Skrollx\DealersModule\Dealer\Table\DealerTableBuilder;
use Skrollx\DealersModule\File\FileUploader;

class AjaxController extends PublicController
{

    public function subscribe()
    {
        $email = \Request::get('email');
        $name_raw = explode(',', \Request::get('name'));
        $fname = isset($name_raw[0]) ? $name_raw[0] : '';
        $lname = isset($name_raw[1]) ? $name_raw[1] : '';
        
        $newsletter = app()->make('laravel-newsletter');
        if($newsletter->hasMember($email)){
            return response()->json(['status' => 0, 'message' => trans('skrollx.module.dealers::message.subscribe_already_subscribed')]);
        }
        if($email && $newsletter->subscribeOrUpdate($email, ['FNAME'=>$fname, 'LNAME'=>$lname])){
            return response()->json(['status' => 1, 'message' => trans('skrollx.module.dealers::message.subscribe_success')]);
        }
        return response()->json(['status' => 0, 'message' => trans('skrollx.module.dealers::message.subscribe_failure')]);
    }

    public function cityDropdown()
    {
        $country = \Request::get('elem');
        return CityModel::join('dealers_cities_translations', 'dealers_cities_translations.entry_id', '=', 'dealers_cities.id')
            ->where('dealers_cities.country', $country)
            ->where('dealers_cities_translations.locale', config('app.locale'))
            ->pluck('dealers_cities_translations.name', 'dealers_cities.id');
    }

    public function uploadDocument(Repository $config, FileUploader $uploader, FolderRepositoryInterface $folders)
    {
        $data = \Request::all();
        $image = \Request::file('file');
        if($image){            
            $uploaded = $uploader->upload($image, $folders->findBySlug('dealer_documents'));
            if($uploaded){
                return response()->json(['success' => true, 'name' => $uploaded->getName(), 'id' => $uploaded->id]);
            }
        }
        return response()->json(['success' => false]);
    }

}
