<?php namespace Skrollx\DealersModule\Http\Controller\Dealer;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Skrollx\DealersModule\Dealer\DealerModel;
use Skrollx\DealersModule\Dealer\Form\DealerFormBuilder;
use Skrollx\DealersModule\Dealer\Table\DealerTableBuilder;
use Skrollx\DealersModule\Deposit\DepositModel;
use Skrollx\DealersModule\Preference\PreferenceModel;

class DealersController extends PublicController
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
        $this->middleware(\Skrollx\DealersModule\Http\Middleware\CheckDealerAccess::class);
    }

    public function index()
    {
        $this->template->meta_title = trans('module::site.dashboard');
        return view('module::dealer/dashboard');
    }

    public function contactInfo()
    {
        $this->template->meta_title = trans('module::site.contact_info');
        return view('module::dealer/contact-info');
    }

    public function editContactInfo()
    {
        $user = auth()->user();
        $dealer = $user->dealer();

        $this->template->meta_title = trans('module::site.contact_info');
        return view('module::dealer/edit-contact-info', compact('dealer', 'user'));
    }

    public function notificationPreferences()
    {
        $user = auth()->user();
        $dealer = $user->dealer();
        $preference = PreferenceModel::where('user_id', $user->id)->first();
        $this->template->meta_title = trans('module::site.contact_info');
        return view('module::dealer/notification-preferences', compact('dealer', 'preference'));
    }

    public function address()
    {
        $user = auth()->user();
        $dealer = $user->dealer();
        $this->template->meta_title = trans('module::site.address');
        return view('module::dealer/address', compact('dealer'));
    }

    public function editAddress()
    {
        $user = auth()->user();
        $dealer = $user->dealer();
        $this->template->meta_title = trans('module::site.address');
        return view('module::dealer/edit-address', compact('dealer'));
    }

    public function billing()
    {
        $this->template->meta_title = trans('module::site.billing');
        return view('module::dealer/billing');
    }

    public function deposit()
    {
        $this->template->meta_title = trans('module::site.deposit');
        return view('module::dealer/deposit');
    }

    public function invoice()
    {
        $this->template->meta_title = trans('module::site.invoice');
        return view('module::dealer/invoice');
    }

    public function bids()
    {
        $this->template->meta_title = trans('module::site.bids');
        return view('module::dealer/bids');
    }

    public function saveCar($id)
    {
        $dealer = \Auth::user()->dealer();
        $dealer->savedCars()->sync([$id], false);
        return redirect()->back()->with('success', [trans('module::message.car_save_success')]);
    }

    public function removeCar($id)
    {
        $dealer = \Auth::user()->dealer();
        $dealer->savedCars()->where('related_id', $id)->delete();
        return redirect()->back()->with('success', [trans('module::message.car_remove_success')]);
    }

    public function savedCars()
    {
        $this->template->meta_title = trans('module::field.saved_cars.name');
        return view('module::dealer/saved-cars');
    }

    /**
     * moved to Skrollx\DealersModule\Http\Controller\Dealer\AjaxController
     */
    public function bidCar($id)
    {
        return redirect()->back()->with('success', ['Depricated! Bidding moved to ajax now.']);
    }
    /**
     * moved to Skrollx\DealersModule\Http\Controller\Dealer\PasswordsController@changePassword
     */
    public function changePassword(){}

}
