<?php namespace Skrollx\DealersModule\Http\Controller\Dealer;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Anomaly\UsersModule\User\UserModel;

class PasswordsController extends PublicController
{

    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');
    }

    public function changePassword()
    {
        if(\Request::get('reset_password')){
            $request_data = \Request::all();
            $messages = [
                'current_password.required'  => trans('skrollx.module.dealers::validation.password.required'),
                'password.required'          => trans('skrollx.module.dealers::validation.new_password.required'),
                'password_confirmation.required' => trans('skrollx.module.dealers::validation.confirm_new_password.required'),
                'password_confirmation.same' => trans('skrollx.module.dealers::validation.confirm_new_password.same'),
            ];

            $validator = \Validator::make($request_data, [
                'current_password' => 'required',
                'password' => 'required|same:password',
                'password_confirmation' => 'required|same:password',     
            ], $messages);

            if($validator->passes())
            {
                $current_password = auth()->user()->password;           
                if(\Hash::check($request_data['current_password'], $current_password))
                {
                    $obj_user = auth()->user();
                    $obj_user->password = $request_data['password'];
                    $obj_user->save(); 
                    return redirect()->back()->with('success', [trans('module::message.password_changed')]);
                }
                else
                {
                    return redirect()->back()->with('error', [trans('module::message.incorrect_current_password')]);
                }
            }else{
                return redirect()->back()->with('error', $validator->errors()->all());
            }
        }  
        $this->template->meta_title = trans('skrollx.module.dealers::site.change_password');
        return view('module::dealer/change-password', compact('errors'));
    }
}
