<?php namespace Skrollx\DealersModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Anomaly\UsersModule\User\UserModel;
use Skrollx\DealersModule\Dealer\Form\DealerFormBuilder;
use Skrollx\DealersModule\Dealer\Table\DealerTableBuilder;

class DealersController extends PublicController
{

    public function login()
    {
        $this->template->meta_title = trans('theme::site.dealer_login');
        return view('skrollx.theme.mebuycar::core/users/login');
    }

    public function register()
    {
        $this->template->meta_title = trans('theme::site.dealer_register');
        return view('skrollx.theme.mebuycar::core/users/register');
    }

    public function completeRegistration()
    {
        $user = UserModel::find(session('user_id'));
        // $user = UserModel::find(4);
        if(!$user){
            return redirect('dealer/register');
        }
        $dealer = $user->dealer();
        if(!$dealer){
            return redirect('dealer/register');
        }

        $cities = \DB::table('streams_cities')->get();

        $this->template->meta_title = trans('theme::site.complete_your_details');
        return view('skrollx.module.dealers::dealer/complete-registration', compact('user', 'dealer','cities'));
    }

}
