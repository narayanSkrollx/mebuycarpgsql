<?php namespace Skrollx\DealersModule\Http\Middleware;

use Closure;

class CheckDealerAccess
{
    /**
     * Handle dealer access check attaching in controllers middleware
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        $allowedSegments = ['login', 'register'];
        if($request->segment(1) == 'dealer' && !in_array($request->segment(2), $allowedSegments)){

            if($user){
                if($user->hasRole('admin')){
                    return redirect('/admin')->send();
                }
                elseif($user->hasRole('user')){
                    return redirect('/')->send();
                }
            }

            if($user && !$user->hasRole('dealer')){
                return redirect('/')->send();
            }
        }
        
        // if($request->segment(1) == 'dealer' 
        //     && $request->segment(2) != 'edit-contact-info' 
        //     && $request->segment(2) != 'edit-address')
        // {
            
        // }

        return $next($request);
    }

}