<?php namespace Skrollx\DealersModule\Invoice\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface InvoiceRepositoryInterface extends EntryRepositoryInterface
{

}
