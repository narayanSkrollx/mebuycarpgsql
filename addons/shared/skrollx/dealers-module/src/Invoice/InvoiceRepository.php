<?php namespace Skrollx\DealersModule\Invoice;

use Skrollx\DealersModule\Invoice\Contract\InvoiceRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class InvoiceRepository extends EntryRepository implements InvoiceRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var InvoiceModel
     */
    protected $model;

    /**
     * Create a new InvoiceRepository instance.
     *
     * @param InvoiceModel $model
     */
    public function __construct(InvoiceModel $model)
    {
        $this->model = $model;
    }
}
