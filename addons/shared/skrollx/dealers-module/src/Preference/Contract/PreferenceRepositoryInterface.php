<?php namespace Skrollx\DealersModule\Preference\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface PreferenceRepositoryInterface extends EntryRepositoryInterface
{

}
