<?php namespace Skrollx\DealersModule\Preference;

use Skrollx\DealersModule\Preference\Contract\PreferenceInterface;
use Anomaly\Streams\Platform\Model\Dealers\DealersPreferencesEntryModel;

class PreferenceModel extends DealersPreferencesEntryModel implements PreferenceInterface
{
    public static function getSubscribers($options = [])
    {
        $make = $options['make_id'];
        unset($options['make_id']);
        
        return PreferenceModel::where($options)->whereHas('makes', function($q) use($make) {
            $q->whereIn('related_id', (array)$make);
        })->get();
    }
}
