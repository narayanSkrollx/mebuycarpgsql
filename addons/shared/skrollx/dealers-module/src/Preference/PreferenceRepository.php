<?php namespace Skrollx\DealersModule\Preference;

use Skrollx\DealersModule\Preference\Contract\PreferenceRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class PreferenceRepository extends EntryRepository implements PreferenceRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var PreferenceModel
     */
    protected $model;

    /**
     * Create a new PreferenceRepository instance.
     *
     * @param PreferenceModel $model
     */
    public function __construct(PreferenceModel $model)
    {
        $this->model = $model;
    }
}
