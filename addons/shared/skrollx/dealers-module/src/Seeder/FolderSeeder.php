<?php namespace Skrollx\DealersModule\Seeder;

use Anomaly\FilesModule\Disk\Contract\DiskRepositoryInterface;
use Anomaly\FilesModule\Folder\Contract\FolderRepositoryInterface;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class FolderSeeder
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 */
class FolderSeeder extends Seeder
{

    /**
     * The disk repository.
     *
     * @var DiskRepositoryInterface
     */
    protected $disks;

    /**
     * The folder repository.
     *
     * @var FolderRepositoryInterface
     */
    protected $folders;

    /**
     * Create a new FolderSeeder instance.
     *
     * @param DiskRepositoryInterface   $disks
     * @param FolderRepositoryInterface $folders
     */
    public function __construct(DiskRepositoryInterface $disks, FolderRepositoryInterface $folders)
    {
        $this->disks   = $disks;
        $this->folders = $folders;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        $disk = $this->disks->findBySlug('local');

        if(!$this->folders->findBySlug('dealer_documents'))
        {
            $this->folders->create(
                [
                    'en'            => [
                        'name'        => 'Dealer Documents',
                        'description' => 'A folder for dealer documents.',
                    ],
                    'slug'          => 'dealer_documents',
                    'disk'          => $disk,
                    'allowed_types' => [],
                ]
            );
        }

        if(!$this->folders->findBySlug('invoices'))
        {
            $this->folders->create(
                [
                    'en'            => [
                        'name'        => 'Invoices',
                        'description' => 'A folder for dealer invoices.',
                    ],
                    'slug'          => 'invoices',
                    'disk'          => $disk,
                    'allowed_types' => [],
                ]
            );
        }
    }
}
