<?php namespace Skrollx\DealersModule\Seeder;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Anomaly\UsersModule\Role\Contract\RoleRepositoryInterface;
use Anomaly\UsersModule\User\Contract\UserInterface;
use Anomaly\UsersModule\User\UserActivator;
use Anomaly\FilesModule\Folder\FolderModel;
use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;

/**
 * Class UserSeeder
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 */
class UserSeeder extends Seeder
{

    /**
     * The user repository.
     *
     * @var UserRepositoryInterface
     */
    protected $users;

    /**
     * The role repository.
     *
     * @var RoleRepositoryInterface
     */
    protected $roles;

    /**
     * The activator utility.
     *
     * @var UserActivator
     */
    protected $activator;

    /**
     * Create a new UserSeeder instance.
     *
     * @param UserRepositoryInterface $users
     * @param RoleRepositoryInterface $roles
     * @param UserActivator           $activator
     */
    public function __construct(
        UserRepositoryInterface $users,
        RoleRepositoryInterface $roles,
        UserActivator $activator
    ) {
        $this->users     = $users;
        $this->roles     = $roles;
        $this->activator = $activator;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        \DB::table('users_users')->truncate();
        \DB::table('users_users_roles')->truncate();

        $user    = $this->roles->findBySlug('user');
        $dealer   = $this->roles->findBySlug('dealer');
        $admin   = $this->roles->findBySlug('admin');
        $site_admin   = $this->roles->findBySlug('site_admin');

        if(!$admin){
            $admin = $this->roles->create([
                'en'   => [
                    'name'        => 'Admin',
                    'description' => 'The admin role.',
                ],
                'slug' => 'admin',
            ]);
        }
        if(!$site_admin){
            $site_admin = $this->roles->create([
                'en'   => [
                    'name'        => 'Site Admin',
                    'description' => 'The site admin role.',
                ],
                'slug' => 'site_admin',
            ]);
        }
        if(!$dealer){
            $dealer = $this->roles->create([
                'en'   => [
                    'name'        => 'Dealer',
                    'description' => 'The dealer role.',
                ],
                'slug' => 'dealer',
            ]);
        }
        if(!$user){
            $user = $this->roles->create([
                'en'   => [
                    'name'        => 'User',
                    'description' => 'The guest user role.',
                ],
                'slug' => 'user',
            ]);
        }

        $faker = \Faker\Factory::create();

        $administrator = $this->users->create(
            [
                'display_name' => 'Administrator',
                'email'        => 'kamal@skrollx.com',
                'username'     => 'admin',
                'password'     => 'skr011x',
            ]
        );
        $administrator->roles()->sync([$admin->getId()]);
        $this->activator->force($administrator);

        $siteadminuser = $this->users->create(
            [
                'display_name' => 'Site Admin',
                'email'        => 'siteadmin@skrollx.com',
                'username'     => 'siteadmin',
                'password'     => 'skr011x',
            ]
        );
        $siteadminuser->roles()->sync([$site_admin->getId()]);
        $this->activator->force($siteadminuser);

        for ($i = 1; $i <= 2; $i++) {
            $fname = $faker->firstName;
            $lname = $faker->lastName;
            $demodealer = $this->users->create(
                [
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                    'display_name' => $fname . ' ' . $lname,
                    'first_name'   => $fname,
                    'last_name'    => $lname,
                    'email'        => 'dealer'.$i.'@demo.com',
                    'password'     => 'password',
                    'username'     => 'dealer'.$i
                ]
            );
            $demodealer->roles()->sync([$dealer->getId()]);
            $this->activator->force($demodealer);
        }
    }
}
