var skrollx = {
    alert: function(elem, type, text){
        $(window).trigger('skrollx.alert', {elem:elem, type:type, text:text});
    },
    validateEmail: function(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    },
    changeSubmitText: function($this, htmlText){
        $button = $this.find('button[type="submit"]');
        var btnHtml = $button.html();
        $button.html(htmlText);
        return btnHtml;
    },
    updateNotificationsBlock: function(){
        $.get('/ajax/notifications-block', function (response) {
            if(response){
                $('.badge-notification').html(response);
            }
        });
    },
    readNotification: function(notification_id){
        $.get('/ajax/read_notification/'+notification_id, function (response) {
            return true;
        });
    },
    clearForm: function(form, formElem){
        for (elem in formElem){
            $(form).find(formElem[elem]).val('');
        }
    },
    requiredValidationFields: function(form, fieldGroupElem){
        var errorsCount = 0;
        $(form+' '+fieldGroupElem).each(function(i, elm){
            $field = $(elm).find('input.form-control[name], select.form-control[name], textarea.form-control[name]');
            if(!$field.val() && $field.length)
            {
                $field.closest(fieldGroupElem).addClass('has-error');
                errorsCount += 1;
            }else{
                $field.closest(fieldGroupElem).removeClass('has-error');
            }
        });

        if(errorsCount == 0){
            return true;
        }
        if($(fieldGroupElem+'.has-error').length){            
            $('html, body').animate({
                scrollTop: ($(fieldGroupElem+'.has-error').offset().top)
            }, 1000);
        }
        return false;
    },
    requiredValidation: function(form, fieldGroupElem){
        $(form).on('submit', function(e){
            return skrollx.requiredValidationFields(form, fieldGroupElem);
        });
    },
    submitAjaxForm: function(formElem, callback, beforSubmit){
        $(document).on('submit', formElem, function(e){
            e.preventDefault();

            if(beforSubmit){
                if(beforSubmit($form) == false){
                    return false;
                }
            }

            var $form      = $(this);
            var $msg       = $form.find('.form-message');
            var successMsg = $msg.attr('data-success');
            var errorMsg   = $msg.attr('data-error');
            var $submit      = $form.find('[type="submit"]');
            var submitText      = $submit.text();

            $submit.html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
            $submit.attr('disabled', true);
            $.ajax({
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: 'json',
                method: 'POST',
            }).done(function( d ) {
              if(d.success){
                var msg = d.msg ? d.msg : successMsg;
                skrollx.alert($msg, 'success', msg);
                if(callback){
                    callback($form, d);
                }
              }else{
                var msg = d.msg ? d.msg : successMsg;
                skrollx.alert($msg, 'danger', msg);
              }
              $submit.attr('disabled', false);
              $submit.text(submitText);
            });
        });
    },
    checkUncheck: function(elem){
        $this = $(elem);
        var allCount = $this.closest('.checkbox').nextAll().length;
        var checkedCount = $this.closest('.checkbox').nextAll().find('input:checkbox:checked').length;
        if(allCount == checkedCount){
            $this.prop('checked', true);
        }else{
            $this.prop('checked', false);
        }
    },
    streamRequiredValidation: function(form, fieldGroupElem){
        $(document).on('submit', form, function(){
            var errorsCount = 0;
            $(form+' .required').each(function(i, elm){
                var $formGroup = $(elm).closest('.form-group');
                var $label = $formGroup.find('label');
                var $input = $formGroup.find('input.form-control[name], select.form-control[name], textarea.form-control[name]');
                if($input.val() == ''){
                    $formGroup.addClass('has-error');
                    errorsCount += 1;
                }else{
                    $formGroup.removeClass('has-error');
                }
            });
            if(errorsCount == 0){
            return true;
            }
            if($(fieldGroupElem+'.has-error').length){            
                $('html, body').animate({
                    scrollTop: ($(fieldGroupElem+'.has-error').offset().top)
                }, 1000);
            }
            return false;
        });
    },
    ajaxFileUpload: function(input){
      if(typeof(input) == 'undefined'){
        return; 
      }
      var uploadURL       = $(input).attr('data-url');
      var formGroupElem   = $(input).attr('data-form-group') ? $(input).attr('data-form-group') : '.form-group';
      var maxFileSize     = $(input).attr('data-max-size') ? $(input).attr('data-max-size') : 1024;
      var nameInputName   = $(input).attr('data-name-input');
      var valueInputName  = $(input).attr('data-value-input');
      var validFileStr    = $(input).attr('data-valid-files') ? $(input).attr('data-valid-files') : 'jpg,jpeg,png';
      var validFiles      = validFileStr.split(',');
      var fileName        = input.files[0].name;
      var fileSize        = input.files[0].size;
      var filetype        = fileName.substr(fileName.lastIndexOf('.') + 1);
      var nameInput       = $(input).closest(formGroupElem).find(nameInputName);
      var valueInput      = $(input).closest(formGroupElem).find(valueInputName);
      var submitBtn       = $(input).closest('form').find('[type="submit"]');
      if(!nameInput || !valueInput || !uploadURL){
        return;
      }
      if(fileSize/1000 > maxFileSize){
        $(input).closest('.form-group').addClass('has-error');
        nameInput.text('The file you are trying to upload exceeds '+(maxFileSize/1024)+' MB (Migabytes).');
        // alert('The file you are trying to upload exceeds '+(maxFileSize/1024)+' MB (Migabytes).');
        return;
      }
      if($.inArray(filetype,validFiles)==-1){
        $(input).closest('.form-group').addClass('has-error');
        nameInput.text(validFileStr + ' files are only allowed to upload.');
        // alert(validFileStr + ' files are only allowed to upload.');
        return;
      }
      if (input.files && input.files[0])
      {
        var reader = new FileReader();

        var data = new FormData();
        var file = input.files[0];
        data.append('file',file);
        reader.onload = function (e) 
        {
          nameInput.html('<i class="fa fa-spinner fa-pulse fa-fw"></i>');
          submitBtn.attr('disabled', true);
          $.ajax({
            url: uploadURL,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(d){
              if(d.success){
                if(valueInput){
                  valueInput.val(d.id);
                }
                nameInput.html(d.thumb ? d.thumb : d.name);
              }
              submitBtn.attr('disabled', false);
            }
          });
        };
        reader.readAsDataURL(input.files[0]);
      }
    },
    filterFormParams: function(form){
        $(document).on('submit', form, function() {
            $(this).find(":input").filter(function(){return !this.value;}).attr("disabled", "disabled");
        });
        $(form).find(":input").prop("disabled", false);
    },
        makeInputChanged: function(elem, targetElem){

        var targetElem = targetElem || '[name="model"]';

        var make_id = $(elem).val();

        var model_id = $(targetElem).attr('data-value');

        if(!make_id){

          return;

        }

        var $modelElem = $(elem).closest('form').find(targetElem);

        $.ajax({

          url: '/ajax/model-dropdown',

          data: {make:make_id},

          dataType: 'json',

          method: 'POST',

        }).done(function( d ) {



          var options = "";

          if(d.length != '0'){

            for(key in d){

              if(d[key]['id'] == model_id){

                selected = 'selected';

              }else{

                selected = '';

              }

              options += '<option label="'+d[key]['title']+'" value="'+d[key]['id']+'" '+selected+'>'+d[key]['title']+'</option>';

              }

            $modelElem.closest('.form-group').show();

            $modelElem.html(options);

            $modelElem.trigger('change');

            skrollx.formRelatedChanged('[name="make"]', '[name="engine_size"]', '/ajax/engine-size-dropdown', 1);

            }else{

              $modelElem.html(options);

            }

            if($.isFunction($.fn.selectpicker)){

                $('.selectpicker, .custom-select').selectpicker('refresh');

            }

          });

    },
    formRelatedChanged: function(elem, targetElem, ajaxUrl, hide){
        if(!ajaxUrl) return false;

        var targetElem = targetElem || '[name="city"]'
        var elem_id = $(elem).val();
        var year = $('input[name="year"]').val();
        var $form = $(elem).closest('form');
        var $target = $form.find(targetElem);
        var target_ids = $target.attr('data-value');
        if(target_ids){
            target_ids = target_ids.split(",")
        }else{
            target_ids = [];
        }
        if(!elem_id) return false;

        $.ajax({
          url: ajaxUrl,
          data: {elem:elem_id,year:year},
          dataType: 'json',
          method: 'POST',
        }).done(function( d ) {
          var options = "";
          if(d.length != '0'){
            for(key in d){
              if(target_ids.indexOf(key) >= 0){
                selected = 'selected';
              }else{
                selected = '';
              }
              options += '<option value="'+key+'" '+selected+'>'+d[key]+'</option>';
              }
            $target.closest('.form-group').show();
            $target.html(options);
            $target.trigger('change');
          }else{
            $target.closest('.form-group').hide();
            $target.html(options);
          }
          if($.isFunction($.fn.selectpicker)){
            $('.selectpicker, .custom-select, .c-select').selectpicker('refresh');
          }
        });
    },
}

$(document).ready(function(){
    $(window).bind('skrollx.alert', function(e,data){
        var type = data.type || 'info';
        var alert_html = '<div class="alert alert-'+type+'">'
            +'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
            + data.text +'</div>'
        $(data.elem).html(alert_html);
        setTimeout(function() {
            $(data.elem).html('');
        }, 10000);
    });

    $(window).bind('skrollx.update.notifications', function(e,data){
        skrollx.updateNotificationsBlock();
    });

    var slider = $('.mainBannerSlider').bxSlider({
    speed: 1000,
    pause: 3000,
    mode: 'fade',
    captions: true,
    // auto: ($('.mainBannerSlider').children().length < 2) ? false : true,
    auto: false, 
    infiniteLoop: true,
    stopAuto: false,
    controls:true,
    pager: false,
    onSlideAfter: function(){
      stopVideo();
    },
    });

    function stopVideo() {
      if ($('.mainBannerSlider .slider-item').find('iframe').length > 0) {
        $('.mainBannerSlider .slider-item').find('iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*'); 
      }

    }

    $('.btn.load-more').on('click', function(e){
        e.preventDefault();
        $(".testimonial-block-wrap [class^='col-']:hidden").slice(0, 2).removeClass('hide').slideDown();
        if ($(".testimonial-block-wrap [class^='col-']:hidden").length == 0) {
            $(this).fadeOut('slow');
        }
    });

    // Date Selector
    var date = new Date();
    $('.datepicker input, input.datepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent: false,
        minDate: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
    });

    // Time Selector
    // $('.timepicker input, input.timepicker').datetimepicker({
    //     format: 'LT'
    // });
    $('.timepicker input, input.timepicker').timepicker({
        snapToStep: true,
        minuteStep: 30,
        showInputs: false,
        showMeridian: false,
    });

    // $('.carousel').carousel({
    //     interval:5000
    // });

    $('.navbar-toggle').on('click', function(event) {
        event.preventDefault();
        $('#mobileResponsiveMenu').slideToggle();
    });

    $('.selectpicker, .custom-select').selectpicker();

    skrollx.makeInputChanged('[name="make"]', '[name="model"]');
    $(document).on('change', '[name="make"]', function(){
        skrollx.makeInputChanged(this, '[name="model"]');
    });
    $(document).on('change', '[name="make"]', function(){
        skrollx.formRelatedChanged(this, '[name="engine_size"]', '/ajax/engine-size-dropdown', 1);
    });
    skrollx.requiredValidation('form.form-valuation', '.form-group.required');
    // skrollx.requiredValidation('form.dealer-register-form', '.form-group.required');
    skrollx.filterFormParams('form.car-filter-form');
    $('select.submit-url').on('change', function(){
      var url = $(this).val();
      window.location.href = url;
    });

});

  $(document).on('keypress','div.select-appearance',function(evt){


      var key = String.fromCharCode(evt.keyCode);
      var typed = $(this).data('typed');
      $(this).data('typed',typed+key);

      var new_typed = $(this).data('typed');

      var that = $(this);


         if (new_typed !== "") {
            var sel = that.find('select');

            var typed_len = new_typed.length;

            $(sel.find('option')).each(function(){
              var sub_text = $(this).text().substr(0,typed_len-1);
              console.log(sub_text);
              if(new_typed == sub_text){
                 $(this).prop('selected',true);
               }
              sel.trigger('change');

              return false;
            });
         }

      setTimeout(function() {
          that.data('typed','');
      }, 1000);

  });