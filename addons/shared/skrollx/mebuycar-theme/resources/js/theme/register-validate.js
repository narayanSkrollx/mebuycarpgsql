$(function(){
  $('form.dealer-register-form').validate({
  rules: {
    company_name: {
        minlength: 3,
        required: true
    },
    email: {
        required: true,
        email: true
    },
    confirm_email: {
        required: true,
        email: true,
        equalTo: '.email-field .form-control'
    },
    password: {
        required: true,
        minlength: 8
    },
    contact_no: {
        required: true,
        number: true
    }
  },
  messages: {
        company_name: false,
        email: {
          required: false,
        },
        confirm_email:{
          required: false,
          equalTo: "{{ trans('skrollx.module.dealers::validation.confirm_email.equalTo') }}",
        },
        password: {
          required: false,
        },
        contact_no: {
          required: false,
        }
      },
  highlight: function(element) {
    $(element).closest('.form-group').addClass('has-error');
  },
  unhighlight: function(element) {
    $(element).closest('.form-group').removeClass('has-error');
  },
  errorElement: 'span',
  errorClass: 'help-block',
  errorPlacement: function(error, element) {
    if(element.parent('.input-group').length) {
      error.insertAfter(element.parent());
    } else {
      error.insertAfter(element);
    }
  }
  });
});