    $(function() {
        $('input[name="daterange"]').daterangepicker({
           autoclose: true,
            viewMode: "years",
            minViewMode: "years",
            showDropdowns: true,
            locale: {
                    format: 'YYYY'
                }
            
        });
    });