<?php

return [
    'send'                 => 'إرسال',
    'login'                => 'تسجيل الدخول',
    'logout'               => 'الخروج',
    'register'             => 'تسجيل',
    'register_now'         => 'سجل الان!',
    'submit'               => 'عرض',
    'save'                 => 'حفظ',
    'cancel'               => 'إلغاء',
    'view'                 => 'رأي',
    'edit'                 => 'تصحيح',
    'download'             => 'تحميل',
    'load_more'            => 'تحميل المزيد',
    'about'                => 'حولنا',
    'contact'              => 'اتصل بنا',
    'contact_us'           => 'اتصل بنا',
    'english'              => 'English',
    'arabic'               => 'عربى',
    'click_here'           => 'انقر هنا',
    'continue'             => 'استمر',
    'car_valuation'        => 'تقييم السيارة',
    'how_it_works'         => 'كيف نعمل',
    'want_to_sell_a_car'   => 'تريد أن تبيع سيارة؟',
    'are_you_a_car_dealer' => 'هل أنت تاجر سيارات؟',
    'evaluate_my_car_now'  => 'قييم سيارتي الان',
    'get_best_deals'  => 'الحصول على أفضل الصفقات السيارات المستعملة الآن',
    'subscribe'  => 'الاشتراك',
];
