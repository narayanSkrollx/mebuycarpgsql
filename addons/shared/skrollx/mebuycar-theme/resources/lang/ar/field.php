<?php

return [
    'name'   => [
        'name' => 'اسم',
        'placeholder' => 'اسم *',
    ],
    'company_name'   => [
        'name' => 'اسم الشركة',
        'placeholder' => 'اسم الشركة *',
    ],
    'first_name'   => [
        'name' => 'الاسم الاول',
        'placeholder' => 'الاسم الاول *',
    ],
    'last_name'   => [
        'name' => 'الكنية',
        'placeholder' => 'الكنية *',
    ],
    'email'       => [
        'name' => 'البريد الإلكتروني',
        'placeholder' => 'البريد الإلكتروني *',
    ],
    'confirm_email'       => [
        'name' => '.تأكيد عنوان البريد الإلكتروني',
        'placeholder' => '.تأكيد عنوان البريد الإلكتروني *',
    ],
    'password'       => [
        'name' => 'كلمه السر',
        'placeholder' => 'كلمه السر *',
    ],
    'contact_no'    => [
        'name' => 'رقم الاتصال',
        'placeholder' => 'رقم الاتصال *',
    ],
    'subject'     => [
        'name' => 'موضوع',
        'placeholder' => 'موضوع *',
    ],
    'message'     => [
        'name' => 'رسالة',
        'placeholder' => 'رسالة *',
    ],
];
