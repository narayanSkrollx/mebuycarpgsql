<?php

return [
    'name'   => [
        'name' => 'Name',
        'placeholder' => 'Name *',
    ],
    'company_name'   => [
        'name' => 'Company Name',
        'placeholder' => 'Company Name *',
    ],
    'first_name'   => [
        'name' => 'First Name',
        'placeholder' => 'First Name *',
    ],
    'last_name'   => [
        'name' => 'Last Name',
        'placeholder' => 'Last Name *',
    ],
    'email'       => [
        'name' => 'Email',
        'placeholder' => 'Email *',
    ],
    'confirm_email'       => [
        'name' => 'Confirm Email',
        'placeholder' => 'Confirm Email *',
    ],
    'password'       => [
        'name' => 'Password',
        'placeholder' => 'Password *',
    ],
    'contact_no'    => [
        'name' => 'Contact Number',
        'placeholder' => 'Contact Number *',
    ],
    'subject'     => [
        'name' => 'Subject',
        'placeholder' => 'Subject *',
    ],
    'message'     => [
        'name' => 'Message',
        'placeholder' => 'Message *',
    ],
];
