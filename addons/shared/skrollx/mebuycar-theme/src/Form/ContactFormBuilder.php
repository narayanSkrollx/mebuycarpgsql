<?php namespace Skrollx\MebuycarTheme\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class ContactFormBuilder extends FormBuilder
{
    /**
     * The form handler.
     *
     * @var ContactFormHandler
     */
    protected $handler = ContactFormHandler::class;

    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [
        'name'    => [
            'label' => 'skrollx.theme.mebuycar::field.name.name',
            'placeholder' => 'skrollx.theme.mebuycar::field.name.placeholder',
            'type'     => 'anomaly.field_type.text',
            'required' => true,
        ],
        'email'   => [
            'label'    => 'skrollx.theme.mebuycar::field.email.name',
            'placeholder'    => 'skrollx.theme.mebuycar::field.email.placeholder',
            'type'     => 'anomaly.field_type.email',
            'required' => true,
        ],
        'contact_no'    => [
            'label'    => 'skrollx.theme.mebuycar::field.contact_no.name',
            'placeholder'    => 'skrollx.theme.mebuycar::field.contact_no.placeholder',
            'type'     => 'anomaly.field_type.text',
            'required' => true,
        ],
        'message' => [
            'label'    => 'skrollx.theme.mebuycar::field.message.name',
            'placeholder'    => 'skrollx.theme.mebuycar::field.message.placeholder',
            'type'     => 'anomaly.field_type.textarea',
            'required' => true,
        ],
    ];

     /**
     * The form actions.
     *
     * @var array
     */
    protected $actions = [
        'submit' => [
            'redirect' => false,
        ],
    ];

    /**
     * The form buttons.
     *
     * @var array
     */
    protected $buttons = [];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [
        'breadcrumb' => false,
        'to' => 'admin@mebuycar.com',
        'from' => 'noreply@mebuycar.com',
    ];
    
}
