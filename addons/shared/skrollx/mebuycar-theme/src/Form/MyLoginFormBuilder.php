<?php namespace Skrollx\MebuycarTheme\Form;

use Anomaly\Streams\Platform\Message\MessageBag;
use Anomaly\Streams\Platform\Ui\Form\FormBuilder;
use Anomaly\UserSecurityCheckExtension\Command\CheckUser;
use Anomaly\UsersModule\User\Contract\UserInterface;
use Anomaly\UsersModule\User\Login\LoginFormBuilder;
use Anomaly\UsersModule\User\UserAuthenticator;
use Anomaly\UsersModule\User\UserModel;
use Anomaly\UsersModule\User\UserSecurity;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LoginFormBuilder
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 */
class MyLoginFormBuilder extends LoginFormBuilder
{

    protected $fields = [
        'email' => [
            'label'    => 'anomaly.module.users::field.email.name',
            'placeholder'    => 'anomaly.module.users::field.email.name',
            'type'     => 'anomaly.field_type.email',
            'required' => true,
        ],
        'password'    => [
            'label'      => 'anomaly.module.users::field.password.name',
            'placeholder'      => 'anomaly.module.users::field.password.name',
            'type'       => 'anomaly.field_type.text',
            'required'   => true,
            'config'     => [
                'type' => 'password',
            ],
            'rules'      => [
                'valid_credentials',
            ],
            'validators' => [
                'valid_credentials' => [
                    'handler' => 'Anomaly\UsersModule\User\Validation\ValidateCredentials@handle',
                    'message' => 'anomaly.module.users::message.invalid_login',
                ],
            ],
        ],
        'remember_me' => [
            'label'  => false,
            'type'   => 'anomaly.field_type.boolean',
            'config' => [
                'mode'  => 'checkbox',
                'label' => 'anomaly.module.users::field.remember_me.name',
            ],
        ]
    ];
    /**
     * No model.
     *
     * @var bool
     */
    protected $model = false;

    protected $handler = 'Anomaly\UsersModule\User\Login\LoginFormHandler';
    /**
     * The user instance. This is set
     * after a successful login
     * has validated.
     *
     * @var null|UserInterface
     */
    protected $user = null;

    /**
     * The form actions.
     *
     * @var array
     */
    protected $actions = [
        'blue' => [
            'class' => 'site-btn',
            'text' => 'anomaly.module.users::button.login',
        ],
    ];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [
        'redirect'   => '/',
        'breadcrumb' => false,
    ];

    /**
     * Fired when the form is posting.
     *
     * @param UserSecurity $security
     */
    public function onPosted(UserSecurity $security)
    {
        // $this->setOption('redirect', '/admin');
    }

    /**
     * Get the user.
     *
     * @return UserInterface|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set the user.
     *
     * @param  UserInterface $user
     * @return $this
     */
    public function setUser(UserInterface $user)
    {
        $this->user = $user;

        return $this;
    }
}
