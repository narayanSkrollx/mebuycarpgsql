<?php namespace Skrollx\MebuycarTheme\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;
use Anomaly\UsersModule\User\Register\Command\AssociateActivationRoles;
use Anomaly\UsersModule\User\Register\Command\SetOptions;
use Anomaly\UsersModule\User\Register\RegisterFormBuilder;
use Skrollx\DealersModule\Dealer\DealerModel;

/**
 * Class MyRegisterFormBuilder
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 */
class MyRegisterFormBuilder extends RegisterFormBuilder
{

    /**
     * The form roles.
     *
     * @var array
     */
    protected $roles = [
        'dealer'
    ];

    /**
     * The form model.
     *
     * @var string
     */
    protected $model = 'Anomaly\UsersModule\User\UserModel';

    protected $handler = 'Skrollx\MebuycarTheme\Form\MyRegisterFormHandler';

    protected $rules = [
        'company_name' => [
            'required'
        ],
        'email' => [
            'required',
            'email'
        ],
        'confirm_email' => [
            'required',
            'email',
            'same:email'
        ],
        'contact_no' => [
            'required'
        ],
        'password' => [
            'required',
            'min:8'
        ]
    ];
    /**
     * The form fields.
     *
     * @var array
     */
    protected $fields = [
        'company_name' => [
            'instructions' => false,
            'type' => 'anomaly.field_type.text',
            'placeholder' => 'theme::field.company_name.name',
            'validators'   => [
                'company_name.required' => [
                    'message' => 'Company Name is required.',
                ],
            ],
        ],
        // 'first_name' => [
        //     'label' => false,
        //     'instructions' => false,
        //     'placeholder' => 'theme::field.first_name.name',
        //     'rules' => [
        //         'required'
        //     ],
        //     'required' => true,
        //     'validators'   => [
        //         'required' => [
        //             'message' => 'First Name is required.',
        //         ],
        //     ],
        // ],
        // 'last_name' => [
        //     'label' => false,
        //     'instructions' => false,
        //     'placeholder' => 'theme::field.last_name.name',
        //     'required' => true,
        //     'validators'   => [
        //         'required' => [
        //             'last_name.message' => 'Last name is required.',
        //         ],
        //     ],
        // ],
        'email'        => [
            'label' => false,
            'instructions' => false,
            'type' => 'anomaly.field_type.email',
            'placeholder' => 'theme::field.email.name',
            'validators'   => [
                'email.required' => [
                    'message' => 'Email is required.',
                ],
                'email.email' => [
                    'message' => 'Email must be a valid email address.',
                ],
                'email.unique' => [
                    'message' => 'Provided email is already registered. Please try with another email.',
                ]
            ],
        ],
        'confirm_email'        => [
            'label' => false,
            'instructions' => false,
            'type' => 'anomaly.field_type.email',
            'placeholder' => 'theme::field.confirm_email.name',
            'validators'   => [
                'confirm_email.required' => [
                    'message' => 'Confirm Email is required.',
                ],
                'confirm_email.email' => [
                    'message' => 'Confirm Email must be a valid email address.',
                ],
                'confirm_email.same' => [
                    'message' => 'Email and Confirm Email must match.',
                ],
            ],
        ],
        'password'     => [
            'label' => false,
            'instructions' => false,
            'placeholder' => 'theme::field.password.name',
            'validators'   => [
                'password.required' => [
                    'message' => 'Password is required.',
                ],
                'password.min' => [
                    'message' => 'Password is must be at least 8 characters.',
                ],
            ]
        ],
        'contact_no' => [
            'label' => false,
            'instructions' => false,
            'type' => 'anomaly.field_type.text',
            'placeholder' => 'theme::field.contact_no.name',
            'validators'   => [
                'contact_no.required' => [
                    'message' => 'Contact Number is required.',
                ],
            ],
        ],
    ];

    /**
     * The form actions.
     *
     * @var array
     */
    protected $actions = [
        'blue' => [
            'class' => 'btn-block site-btn',
            'text' => 'anomaly.module.users::button.register',
        ],
    ];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [
        'redirect'          => '/',
        'success_message'   => 'anomaly.module.users::success.user_registered',
        'pending_message'   => 'anomaly.module.users::message.pending_admin_activation',
        'confirm_message'   => 'anomaly.module.users::message.pending_email_activation',
        'activated_message' => 'anomaly.module.users::message.account_activated',
    ];

    public function onPost()
    {
        $this->setSkips(['company_name', 'contact_no', 'confirm_email']);
    }

    /**
     * Fired after the form is saved.
     */
    public function onSaving()
    {
        $entry = $this->getFormEntry();
        if($company_name = $this->getFormValue('company_name')){
            $entry->display_name = $company_name;
            // $name_parts = explode(' ', $company_name);
            // if(isset($name_parts[0])){
            //     $entry->first_name = $name_parts[0];
            // }
            // if(isset($name_parts[1])){
            //     $entry->last_name = $name_parts[1];
            // }
        }
        if($email = $this->getFormvalue('email')){
            $entry->username = $email;
        }
    }

    /**
     * Fired after the form is saved.
     */
    public function onSaved()
    {
        $this->dispatch(new AssociateActivationRoles($this));
        $user = $this->getFormEntry();
        session(['user_id' => $user->id]);
        $post = \Request::all();
        $dealer_data = [
            'company_name' => \Request::get('company_name'),
            'phone_no'     => \Request::get('contact_no'),
            'user_id'     => $user->id,
            'email'     => \Request::get('email'),
        ];

        $dealer = DealerModel::create($dealer_data);
        $dealer->created_by_id = $user->id;
        $dealer->save();

    }

    /**
     * Get the roles.
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set roles.
     *
     * @param $roles
     * @return $this
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }


}
