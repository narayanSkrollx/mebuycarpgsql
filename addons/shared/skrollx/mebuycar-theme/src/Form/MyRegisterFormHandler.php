<?php namespace Skrollx\MebuycarTheme\Form;

use Anomaly\UsersModule\User\UserModel;
use Anomaly\UsersModule\User\UserActivator;
use Anomaly\Streams\Platform\Traits\Eventable;
use Anomaly\Streams\Platform\Traits\Transmitter;
use Anomaly\UsersModule\User\Contract\UserInterface;
use Anomaly\UsersModule\User\Notification\UserHasRegistered;
use Anomaly\UsersModule\User\Register\Command\HandleAutomaticRegistration;
use Anomaly\UsersModule\User\Register\Command\HandleEmailRegistration;
use Anomaly\UsersModule\User\Register\Command\HandleManualRegistration;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Contracts\Config\Repository;

class MyRegisterFormHandler
{
    use Transmitter;
    use DispatchesJobs;

    /**
     * Handle the form.
     *
     * @param  Repository          $config
     * @param  MyRegisterFormBuilder $builder
     * @param  UserActivator       $activator
     * @throws \Exception
     */
    public function handle(Repository $config, MyRegisterFormBuilder $builder, UserActivator $activator)
    {
        if (!$builder->canSave()) {
            return;
        }

        $builder->saveForm(); // Save the new user.

        /* @var UserInterface $user */
        $user = $builder->getFormEntry();

        $activator->start($user);

        $this->dispatch(new HandleManualRegistration($builder));

        // $this->transmit(new UserHasRegistered($user));
    }
}
