<?php namespace Skrollx\MebuycarTheme;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;

class MebuycarThemeServiceProvider extends AddonServiceProvider
{

    protected $plugins = [];

    protected $commands = [];

    protected $schedules = [];

    protected $api = [];

    protected $routes = [];

    protected $middleware = [];

    protected $routeMiddleware = [];

    protected $listeners = [];

    protected $aliases = [];

    protected $bindings = [
        'login_form'    => \Skrollx\MebuycarTheme\Form\MyLoginFormBuilder::class,
        'register_form' => \Skrollx\MebuycarTheme\Form\MyRegisterFormBuilder::class,
        'contact_form'  => \Skrollx\MebuycarTheme\Form\ContactFormBuilder::class,
    ];

    protected $providers = [];

    protected $singletons = [];

    protected $overrides = [
        'streams::errors/404' => 'skrollx.theme.mebuycar::errors/404',
        'streams::errors/500' => 'skrollx.theme.mebuycar::errors/500',

        'anomaly.module.users::login' => 'skrollx.theme.mebuycar::core/users/login',
        'anomaly.module.users::register' => 'skrollx.theme.mebuycar::core/users/register',
        'anomaly.module.users::password/reset' => 'skrollx.theme.mebuycar::core/password/reset',
        'anomaly.module.users::password/forgot' => 'skrollx.theme.mebuycar::core/password/forgot',
    ];

    protected $mobile = [];

    public function register()
    {
    }

    public function map()
    {
    }

}
