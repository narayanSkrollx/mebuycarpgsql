
<?php include('header.php'); ?>

<!-- Homepage Slider -->
<div id="mainslider" class="mainslider">
	<div class="slider-container">
		<div class="mainslider-wrap">
			<div class="homeslider">
				<div class="slider-item">
					<img src="images/slider-img1.jpg">
					<div class="slider-caption">
						<h4>Latest Bid</h4>
						<h1>Land Cruiser Prado</h1>
						<p>Lorem ipsum dolor sit amet cons  ectetur adipiscing elit. </p>
					</div>
				</div>
				<div class="slider-item">
					<img src="images/slider-img2.jpg">
					<div class="slider-caption">
						<h4>Latest Bid</h4>
						<h1>Land Cruiser Prado</h1>
						<p>Lorem ipsum dolor sit amet cons  ectetur adipiscing elit. </p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<span class="slider-shape1">
		<img src="images/mainslider-shape1.png">
	</span>
	<span class="slider-shape2">
		<img src="images/mainslider-shape2.png">
	</span>
</div>

<!-- Site Section Start -->

<!-- How it Works Section -->
<section id="howitworks" class="howitworks">
	<div class="container">
		<h2 class="page-title">How it works</h2>
		<h5 class="tagline">Bid your car in 3 simple steps</h5>
		<div class="howitworks-col-wrap">
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="howitworks-column">
						<span class="step">Step 1</span>
						<img src="images/register-img.png">
						<h4>REGISTER</h4>
						<p>
							Lorem ipsum dolor sit amet,
							consectetur adipiscing.
						</p>
						<span class="shape first"></span>
						<span class="shape last"></span>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="howitworks-column">
						<span class="step">Step 2</span>
						<img src="images/hammer-img.png">
						<h4>START BIDDING</h4>
						<p>
						Lorem ipsum dolor sit amet,
						consectetur adipiscing.
						</p>
						<span class="shape first"></span>
						<span class="shape last"></span>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="howitworks-column">
						<span class="step">Step 3</span>
						<img src="images/auto.png">
						<h4>COMPLETE PURCHASE</h4>
						<p>
						Lorem ipsum dolor sit amet,
						consectetur adipiscing.
						</p>
						<span class="shape first"></span>
						<span class="shape last"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Two Section -->
<div class="two-section-wrap">

	<!-- Winners Table -->
	<section id="winners" class="winners">
		<div class="container">
			<div class="winners-overlay">
				<h2 class="page-title">Winners</h2>
				<div class="winner-table-wrap">
					<div class="winner-table">
						<table class="table">
							<thead>
								<tr>
									<th>Name of dealer</th>
									<th>Winning Bid Amount</th>
									<th>Plate Number</th>
									<th>Car Type</th>
									<th>Car Year</th>
									<th>Kilometers</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="name">John Doe</td>
									<td>36000 SR</td>
									<td>21332</td>
									<td>SUV</td>
									<td>2009</td>
									<td>280000</td>
								</tr>
								<tr>
									<td class="name">John Doe</td>
									<td>36000 SR</td>
									<td>21332</td>
									<td>SUV</td>
									<td>2009</td>
									<td>280000</td>
								</tr>
								<tr>
									<td class="name">John Doe</td>
									<td>36000 SR</td>
									<td>21332</td>
									<td>SUV</td>
									<td>2009</td>
									<td>280000</td>
								</tr>
								<tr>
									<td class="name">John Doe</td>
									<td>36000 SR</td>
									<td>21332</td>
									<td>SUV</td>
									<td>2009</td>
									<td>280000</td>
								</tr>
								<tr>
									<td class="name">John Doe</td>
									<td>36000 SR</td>
									<td>21332</td>
									<td>SUV</td>
									<td>2009</td>
									<td>280000</td>
								</tr>
								<tr>
									<td class="name">John Doe</td>
									<td>36000 SR</td>
									<td>21332</td>
									<td>SUV</td>
									<td>2009</td>
									<td>280000</td>
								</tr>
								<tr>
									<td class="name">John Doe</td>
									<td>36000 SR</td>
									<td>21332</td>
									<td>SUV</td>
									<td>2009</td>
									<td>280000</td>
								</tr>
								<tr>
									<td class="name">John Doe</td>
									<td>36000 SR</td>
									<td>21332</td>
									<td>SUV</td>
									<td>2009</td>
									<td>280000</td>
								</tr>
								<tr>
									<td class="name">John Doe</td>
									<td>36000 SR</td>
									<td>21332</td>
									<td>SUV</td>
									<td>2009</td>
									<td>280000</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<span class="shape first"></span>
				<span class="shape last"></span>
			</div>
		</div>
	</section>

	<!-- Why Samara Section -->
	<section id="whysamara" class="whysamara">
		<div class="container">
			<div class="inner-paddor">
				<h2 class="page-title">Why Samara</h2>
				<h5 class="tagline">
					Samara is Saudi’s first automobile brand with a long heritage of more than 
					90 years in the industry.
				</h5>
				<p>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
				</p>
				<div class="samara-support">
					<div class="row">
						<div class="col-md-4">
							<div class="sp-block">
								<img src="images/wide.png"><span>Wide Selection</span>
							</div>
						</div>
						<div class="col-md-4">
							<div class="sp-block">
								<img src="images/support.png"><span>Support Team</span>
							</div>
						</div>
						<div class="col-md-4">
							<div class="sp-block">
								<img src="images/money.png"><span>Save Money</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<!-- Samara Benefits Section -->
<section id="benefits" class="benefits">
	<div class="container">
			<div class="benefits-block-wrap">
				<h2 class="page-title">Benefits of selling with samara</h2>
				<div class="inner-container">
					<div class="row">
						<div class="col-md-6">
							<div class="benefits-block">
								<span class="bimg">
									<img src="images/csupp.png">
								</span>
								<h5>Customer Support</h5>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="benefits-block right">
								<span class="bimg">
									<img src="images/payment.png">
								</span>
								<h5>Secure Payment</h5>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="benefits-block">
								<span class="bimg">
									<img src="images/fees.png">
								</span>
									<h5>Low Fees</h5>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="benefits-block right">
								<span class="bimg">
									<img src="images/globe.png">
								</span>
								<h5>Live Auction</h5>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
							</div>
						</div>
					</div>
				</div>
				<span class="shape first"></span>
				<span class="shape last"></span>
			</div>
		</div>
	</div>
</section>


<!-- Samara Clients Section -->
<section id="clients" class="clients">
	<div class="container">
		<div class="inner-container">
			<h2 class="page-title">Partners</h2>
			<div class="clients-slider">
				<div class="slider-item client1">
					<img src="images/client1.png">
				</div>
				<div class="slider-item client2">
					<img src="images/client2.png">
				</div>
				<div class="slider-item client3">
					<img src="images/client3.png">
				</div>
				<div class="slider-item client4">
					<img src="images/client4.png">
				</div>
				<div class="slider-item client5">
					<img src="images/client5.png">
				</div>
			</div>
		</div>
	</div>
</section>



<?php include('footer.php'); ?>