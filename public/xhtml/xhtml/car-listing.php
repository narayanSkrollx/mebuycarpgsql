
<?php include('header.php'); ?>


<!-- Site Breadcrumb -->
<div id="breadcrumb" class="site-breadcrumb"> 
  <div class="main-breadcrumb-wrap">
    <div class="container">
      <div class="inner-container">
        <div class="site-breadcrumb-wrap">
          <div class="row">
            <div class="col-md-6">
              <h3 class="banner-title">
                Cars Listing
              </h3>
            </div>
            <div class="col-md-6">
              <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Search</a></li>
              </ol>
            </div>
          </div>
          <!-- <span class="shape first"></span>
          <span class="shape last"></span> -->
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Site Content -->

<section class="site-section section">
  <div class="container">
    <div class="inner-container">

      <!-- Search Form -->
      <form class="form search-form clearfix">
        <div class="form-group">
          <div class="select-appearance">
            <select class="form-control selectpicker">
              <option>Search Make</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="select-appearance">
            <select class="form-control selectpicker">
              <option>Search Years</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="select-appearance">
            <select class="form-control selectpicker">
              <option>Select Body Type</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="select-appearance">
            <select class="form-control selectpicker">
              <option>Search Mileage</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="select-appearance">
            <select class="form-control selectpicker">
              <option>Type Here</option>
              <option>2</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn search-btn">Search</button>
        </div>
      </form>

      <!-- Total Cars Found -->
      <div class="total-cars-block">
        <h3 class="nocars">
          155 Cars Found
        </h3>
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <form class="form sort-form">
              <div class="form-group">
                <div class="select-appearance">
                  <select class="form-control selectpicker">
                    <option>Sort by: Recent First</option>
                    <option>Sort by: Recent Last</option>
                </select>
                </div>
              </div>
              <div class="form-group">
                <div class="select-appearance">
                  <select class="form-control selectpicker">
                    <option>Per Pages: 10</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
          <div class="col-md-6 col-sm-6">
            <div class="car-pagination">
              <ul class="pagination">
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">4</a></li>
                <li class="page-item"><a class="page-link" href="#">5</a></li>
                <li class="page-item"><a class="page-link" href="#">6</a></li>
              </ul>
            </div>                  
          </div>
        </div>
      </div>

    <!-- Car Details Block -->
    <div class="car-details-block-wrap">

      <div class="car-details-block">
        <div class="row">
          <div class="col-md-5">
            <figure class="thumbnail-img">
              <span class="shape">
                <img src="images/car1.png">
              </span>
            </figure>
          </div>
          <div class="col-md-4">
            <div class="car-details">
              <h3 class="car-heading">
                Nissan Patrol 2018
              </h3>
              <ul class="car-details-list">
                <li><span>Mileage</span>10,000 KM</li>
                <li><span>Car Color</span>Silver</li>
                <li><span>Transmission</span>Automatic</li>
                <li><span>Engine Size</span>2.00</li>
                <li><span>Assident History</span>Yes</li>
                <li><span>Specs</span>GCC</li>
              </ul>
            </div>
          </div>
          <div class="col-md-3">
            <div class="car-details-right">
              <h4>Deal Offer</h4>
              <span class="deal-time">
                2  days 5 : 54 : 25 left
              </span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="car-details-actions">
              <ul>
                <li><a href="">Quick Look: 10 Photos</a></li>
                <li><a href="">Save this car</a></li>
                <li><a href="">View Details</a></li>
                <li><a href="">Bid Now</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="car-details-block">
        <div class="row">
            <div class="col-md-5">
              <figure class="thumbnail-img">
                <span class="shape">
                  <img src="images/car3.png">
                </span>
              </figure>
            </div>
            <div class="col-md-4">
              <div class="car-details">
                <h3 class="car-heading">
                  Nissan Patrol 2018
                </h3>
                <ul class="car-details-list">
                  <li><span>Mileage</span>10,000 KM</li>
                  <li><span>Car Color</span>Silver</li>
                  <li><span>Transmission</span>Automatic</li>
                  <li><span>Engine Size</span>2.00</li>
                  <li><span>Assident History</span>Yes</li>
                  <li><span>Specs</span>GCC</li>
                </ul>
              </div>
            </div>
            <div class="col-md-3">
              <div class="car-details-right">
                <h4>Deal Offer</h4>
                <span class="deal-time">
                  2  days 5 : 54 : 25 left
                </span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="car-details-actions">
                <ul>
                  <li><a href="">Quick Look: 10 Photos</a></li>
                  <li><a href="">Save this car</a></li>
                  <li><a href="">View Details</a></li>
                  <li><a href="">Bid Now</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div class="car-details-block">
          <div class="row">
          <div class="col-md-5">
            <figure class="thumbnail-img">
              <span class="shape">
                <img src="images/car2.png">
              </span>
            </figure>
          </div>
          <div class="col-md-4">
            <div class="car-details">
              <h3 class="car-heading">
                Nissan Patrol 2018
              </h3>
              <ul class="car-details-list">
                <li><span>Mileage</span>10,000 KM</li>
                <li><span>Car Color</span>Silver</li>
                <li><span>Transmission</span>Automatic</li>
                <li><span>Engine Size</span>2.00</li>
                <li><span>Assident History</span>Yes</li>
                <li><span>Specs</span>GCC</li>
              </ul>
            </div>
          </div>
          <div class="col-md-3">
            <div class="car-details-right">
              <h4>Deal Offer</h4>
              <span class="deal-time">
                2  days 5 : 54 : 25 left
              </span>
            </div>
          </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="car-details-actions">
                <ul>
                  <li><a href="">Quick Look: 10 Photos</a></li>
                  <li><a href="">Save this car</a></li>
                  <li><a href="">View Details</a></li>
                  <li><a href="">Bid Now</a></li>
                </ul>
              </div>
            </div>
          </div>
      </div>

      <div class="car-details-block">
        <div class="row">
          <div class="col-md-5">
            <figure class="thumbnail-img">
              <span class="shape">
                <img src="images/car4.png">
              </span>
            </figure>
          </div>
          <div class="col-md-4">
            <div class="car-details">
              <h3 class="car-heading">
                Nissan Patrol 2018
              </h3>
              <ul class="car-details-list">
                <li><span>Mileage</span>10,000 KM</li>
                <li><span>Car Color</span>Silver</li>
                <li><span>Transmission</span>Automatic</li>
                <li><span>Engine Size</span>2.00</li>
                <li><span>Assident History</span>Yes</li>
                <li><span>Specs</span>GCC</li>
              </ul>
            </div>
          </div>
          <div class="col-md-3">
            <div class="car-details-right">
              <h4>Deal Offer</h4>
              <span class="deal-time">
                2  days 5 : 54 : 25 left
              </span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="car-details-actions">
              <ul>
                <li><a href="">Quick Look: 10 Photos</a></li>
                <li><a href="">Save this car</a></li>
                <li><a href="">View Details</a></li>
                <li><a href="">Bid Now</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="car-pagination foot">
    <ul class="pagination">
    <li class="page-item ">
    <a class="page-link" href="#">
      <img src="images/pagination-prev.png">
    </a>
    </li>
    <li class="page-item active"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">4</a></li>
    <li class="page-item"><a class="page-link" href="#">5</a></li>
    <li class="page-item"><a class="page-link" href="#">6</a></li>
    <li class="page-item">
      <a class="page-link" href="#">
        <img src="images/pagination-next.png">
      </a>
    </li>
    </ul>
    </div>   

    </div>
  </div>
</section>

  
 
 <?php include('footer.php'); ?>