<?php include('header.php'); ?>


<!-- Site Breadcrumb -->
<div id="breadcrumb" class="site-breadcrumb"> 
  <div class="main-breadcrumb-wrap">
    <div class="container">
      <div class="inner-container">
        <div class="site-breadcrumb-wrap">
          <div class="row">
            <div class="col-md-6">
              <h3 class="banner-title">
                Cars Details
              </h3>
            </div>
            <div class="col-md-6">
              <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Car</a></li>
              </ol>
            </div>
          </div>
          <!-- <span class="shape first"></span>
          <span class="shape last"></span> -->
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Site Content -->

<section class="site-section car-view-section">
  <div class="container">
    <div class="inner-container clearfix">
      <div class="car-view-block-wrap">
        <div class="row">
          <div class="col-md-6">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <div class="item active">
                  <img src="images/view-car1.png" alt="view-car1">
                </div>
                <div class="item">
                  <img src="images/view-car1.png" alt="view-car1">
                </div>
                <div class="item">
                  <img src="images/view-car1.png" alt="view-car1">
                </div>
                <div class="item">
                  <img src="images/view-car1.png" alt="view-car1">
                </div>
              </div>

              <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active">
                  <img src="images/view-car1.png" alt="view-car1">
                </li>
                <li data-target="#carousel-example-generic" data-slide-to="1">
                  <img src="images/view-car1.png" alt="view-car1">
                </li>
                <li data-target="#carousel-example-generic" data-slide-to="2">
                  <img src="images/view-car1.png" alt="view-car1">
                </li>
                <li data-target="#carousel-example-generic" data-slide-to="3">
                  <img src="images/view-car1.png" alt="view-car1">
                </li>
              </ol>

            </div>
          </div>
          <div class="col-md-6">
            <div class="car-view-details">
              <h3 class="block-title">
                Renault Duster 2018
              </h3>
              <p class="car-number">
                CAR NUMBER:  #232344
              </p>
              <p class="desc">
                We can close the Nissan Patrol 2018 if you increase your bid to AED 10,000
              </p>
              <div class="seller-details">
                <p>Minimum bid AED <span>3,000</span></p>
                <p>Seller already rejected 3000 AED before. Try bidding more !</p>
              </div>
              <div class="seller-price-details">
                <ul>
                  <li><span>Seller's Price : </span>AED 8,000.00</li>
                  <li>2 days 5 : 30 : 25 left</li>
                </ul>
              </div>
              <div class="biding-price">
                <h4>Your Bidding Price</h4>
                <div class="bidnow">
                  <input type="text" class="form-control" name="bidprice" placeholder="AED">
                  <button type="submit" class="btn site-btn">Bid Now</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="note">
        <h5>
          <span>Note:</span> Please note that you will be charged an admin fee of 229 AED and a mandatory odometer check with authorities of 49 AED.
        </h5>
      </div>

      <div class="info-tabs">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#vechileinfo" aria-controls="vechileinfo" role="tab" data-toggle="tab">Vechile Information</a></li>
            <li role="presentation"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">Description</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="vechileinfo">
              <div class="infotabs-content">
                <table class="table">
                  <tbody>
                    <tr>
                      <th>Make</th>
                      <td>Renault Duster</td>
                    </tr>
                    <tr>
                      <th>Model</th>
                      <td>E-Class</td>
                    </tr>
                    <tr>
                      <th>Exact Model</th>
                      <td>E 430</td>
                    </tr>
                    <tr>
                      <th>Transmission</th>
                      <td>Automatic</td>
                    </tr>
                    <tr>
                      <th>Interior Trim</th>
                      <td>Leather</td>
                    </tr>
                    <tr>
                      <th>Year</th>
                      <td>1999</td>
                    </tr>
                    <tr>
                      <th>Specs</th>
                      <td>Japan</td>
                    </tr>
                    <tr>
                      <th>Engine Cylinders</th>
                      <td>8</td>
                    </tr>
                    <tr>
                      <th>Odometer Reading (km)</th>
                      <td>203716 km</td>
                    </tr>
                    <tr>
                      <th>Paint</th>
                      <td>Portion Repaint</td>
                    </tr>
                    <tr>
                      <th>Accident History</th>
                      <td>Yes</td>
                    </tr>
                    <tr>
                      <th>Vehicle History Report</th>
                      <td>Accident reported, Personal car</td>
                    </tr>
                    <tr>
                      <th>Service History</th>
                      <td>No Service History</td>
                    </tr>
                    <tr>
                      <th>Body</th>
                      <td>Sedan</td>
                    </tr>
                    <tr>
                      <th>Drive</th>
                      <td>2WD</td>
                    </tr>
                    <tr>
                      <th>Car Color</th>
                      <td>Blue</td>
                    </tr>
                    <tr>
                      <th>Fuel Type</th>
                      <td>Petrol</td>
                    </tr>
                    <tr>
                      <th>Odometer Reading (km)</th>
                      <td>203716</td>
                    </tr>
                    <tr>
                      <th>Car Number</th>
                      <td>2451959</td>
                    </tr>
                    <tr>
                      <th>Engine Size</th>
                      <td>4.3</td>
                    </tr>
                    <tr>
                      <th>Structural / Chassis Damage</th>
                      <td>No</td>
                    </tr>
                    <tr>
                      <th>Chassis Repaired</th>
                      <td>No</td>
                    </tr>
                    <tr>
                      <th>Car Registered In</th>
                      <td>Dubai</td>
                    </tr>
                    <tr>
                      <th>Bank Loan?</th>
                      <td>No</td>
                    </tr>
                    <tr>
                      <th>Navigation System</th>
                      <td>No</td>
                    </tr>
                    <tr>
                      <th>Number Of Keys</th>
                      <td>1</td>
                    </tr>
                    <tr>
                      <th>Roof</th>
                      <td>Sunroof</td>
                    </tr>
                    <tr>
                      <th>Rim Tipe</th>
                      <td>Alloy</td>
                    </tr>
                    <tr>
                      <th>Rim Condition</th>
                      <td>Scratched</td>
                    </tr>
                    <tr>
                      <th>Seats Color</th>
                      <td>Black</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="description">
              <div class="infotabs-content">
                <table class="table">
                  <tbody>
                    <tr>
                      <th>Make</th>
                      <td>Renault Duster</td>
                    </tr>
                    <tr>
                      <th>Model</th>
                      <td>E-Class</td>
                    </tr>
                    <tr>
                      <th>Exact Model</th>
                      <td>E 430</td>
                    </tr>
                    <tr>
                      <th>Transmission</th>
                      <td>Automatic</td>
                    </tr>
                    <tr>
                      <th>Interior Trim</th>
                      <td>Leather</td>
                    </tr>
                    <tr>
                      <th>Year</th>
                      <td>1999</td>
                    </tr>
                    <tr>
                      <th>Specs</th>
                      <td>Japan</td>
                    </tr>
                    <tr>
                      <th>Engine Cylinders</th>
                      <td>8</td>
                    </tr>
                    <tr>
                      <th>Odometer Reading (km)</th>
                      <td>203716 km</td>
                    </tr>
                    <tr>
                      <th>Paint</th>
                      <td>Portion Repaint</td>
                    </tr>
                    <tr>
                      <th>Accident History</th>
                      <td>Yes</td>
                    </tr>
                    <tr>
                      <th>Vehicle History Report</th>
                      <td>Accident reported, Personal car</td>
                    </tr>
                    <tr>
                      <th>Service History</th>
                      <td>No Service History</td>
                    </tr>
                    <tr>
                      <th>Body</th>
                      <td>Sedan</td>
                    </tr>
                    <tr>
                      <th>Drive</th>
                      <td>2WD</td>
                    </tr>
                    <tr>
                      <th>Car Color</th>
                      <td>Blue</td>
                    </tr>
                    <tr>
                      <th>Fuel Type</th>
                      <td>Petrol</td>
                    </tr>
                    <tr>
                      <th>Odometer Reading (km)</th>
                      <td>203716</td>
                    </tr>
                    <tr>
                      <th>Car Number</th>
                      <td>2451959</td>
                    </tr>
                    <tr>
                      <th>Engine Size</th>
                      <td>4.3</td>
                    </tr>
                    <tr>
                      <th>Structural / Chassis Damage</th>
                      <td>No</td>
                    </tr>
                    <tr>
                      <th>Chassis Repaired</th>
                      <td>No</td>
                    </tr>
                    <tr>
                      <th>Car Registered In</th>
                      <td>Dubai</td>
                    </tr>
                    <tr>
                      <th>Bank Loan?</th>
                      <td>No</td>
                    </tr>
                    <tr>
                      <th>Navigation System</th>
                      <td>No</td>
                    </tr>
                    <tr>
                      <th>Number Of Keys</th>
                      <td>1</td>
                    </tr>
                    <tr>
                      <th>Roof</th>
                      <td>Sunroof</td>
                    </tr>
                    <tr>
                      <th>Rim Tipe</th>
                      <td>Alloy</td>
                    </tr>
                    <tr>
                      <th>Rim Condition</th>
                      <td>Scratched</td>
                    </tr>
                    <tr>
                      <th>Seats Color</th>
                      <td>Black</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
      </div>

    </div>
  </div>
</section>
      

 <?php include('footer.php'); ?>