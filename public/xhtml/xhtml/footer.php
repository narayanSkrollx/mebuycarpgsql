<!-- Footer -->
<footer id="footer" class="site-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="footer-block">
					<cite><img src="images/footer-logo.png"></cite>
					<p class="footer-desc">
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
					</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="footer-block">
					<p class="footer-title">
						Need Help
					</p>
					<p>Contact us via Phone or Email</p>
					<ul class="contacts">
						<li><img src="images/contact-img.png">+966 561 45878</li>
						<li class="email"><img src="images/email-img.png">info@samara.com</li>
					</ul>
					<a href="#" class="pdflink">
						Company Profile (PDF)
					</a>
				</div>
			</div>
			<div class="col-md-2">
				<div class="footer-block">
					<p class="footer-title">
						Follow Us
					</p>
					<ul class="social">
						<li><a href="#"><img src="images/gmail-img.png"></a></li>
						<li><a href="#"><img src="images/linkedin-img.png"></a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3">
				<div class="footer-block">
					<p class="footer-title">
						Newsletter Signup
					</p>
					<form class="form form-newsletter">
						<div class="form-group">
							<input type="text" class="form-control" name="email" placeholder="Enter your email address">
						</div>
						<div class="form-group">
							<button type="submit" class="btn site-btn">
								SUBMIT
							</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-12">
				<p class="copyright">
					© Copyright 2018. Samara Trading. All rights reserved. Site by GO-Gulf
				</p>
			</div>
		</div>
	</div>
</footer>



<!-- Linking Scripts -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/jquery.bxslider.js"></script>
<script type="text/javascript" src="js/main.js"></script>

</body>
</html>