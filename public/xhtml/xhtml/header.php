<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" initial-scale=1>
	<title>Homepage | Samara</title>

	<!-- Linking Favicons -->
	<link rel="shortcut icon" href="images/favicon/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="images/favicon/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-touch-icon-152x152.png" />
	<link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon-180x180.png" />


	<!-- Linking Styles -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-select.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
	<link rel="stylesheet" type="text/css" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">


</head>
<body>

<!-- Header -->
<header id="header" class="site-header">
	<div class="container">
		<div class="inner-container">
			<div class="topmenu">
				<span>
					<img src="images/header-bg.png">
					<ul>
						<li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
						<li><a href="#">العربية</a></li>
					</ul>
				</span>
			</div>
			<div class="main-header">
				<div class="row">
					<div class="col-md-3">
						<div class="site-logo">
							<a href="#">
								<img src="images/site-logo.png">
							</a>
						</div>
					</div>
					<div class="col-md-9">
						<div class="site-navigation">
							<ul>
								<li><a href="#">About Us</a></li>
								<li><a href="#">How it works</a></li>
								<li><a href="#">Contact</a></li>
								<li><a href="#">Login</a></li>
								<li class="register"><a href="#">Register</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>


<!-- Search Form (Hidden Toggle form) -->
<div class="srch-form" id="get_search">
	<form class="searchform" id="searchform">
		<div class="form-group">
			<input type="text" class="form-control" name="searchtxt" placeholder="Search Samaracar">
			<button type="submit" class="btn site-btn"><i class="fa fa-search"></i></button>
		</div>
	</form>					
	<span class="close"><i class="fa fa-times-circle"></i></span>
</div>
