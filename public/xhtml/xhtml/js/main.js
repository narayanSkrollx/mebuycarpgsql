$('.homeslider').bxSlider({
    pager: false,
    speed: 2000
});

$('.clients-slider').bxSlider({
	speed: 1500,
    pause: 5000,
    mode: 'horizontal',
    auto: true,
    infiniteLoop: true,
    slideWidth: 153,
    slideMargin: 78,
    pager: false,
    controls:false,
    minSlides: 5,
    maxSlides: 5,
    moveSlides: 1
});

$('.search').click(function(){

    $('.srch-form').toggleClass('active');

});


$('.close').click(function(){

  $('.srch-form').removeClass('active');

});


