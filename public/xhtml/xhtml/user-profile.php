<?php include('header.php'); ?>


<!-- Site Breadcrumb -->
<div id="breadcrumb" class="site-breadcrumb"> 
  <div class="main-breadcrumb-wrap">
    <div class="container">
      <div class="inner-container">
        <div class="site-breadcrumb-wrap">
          <div class="row">
            <div class="col-md-6">
              <h3 class="banner-title">
                Account
              </h3>
            </div>
            <div class="col-md-6">
              <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Account</a></li>
              </ol>
            </div>
          </div>
          <!-- <span class="shape first"></span>
          <span class="shape last"></span> -->
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Site Content -->

<section class="site-section car-view-section user-profile">
  <div class="container">
    <div class="inner-container clearfix">
      <div class="user-profile-block">
        <div class="row">
          <div class="col-md-4">
            <div class="page-navigation">
              <ul>
                <li class="active"><a href="#">Contact info</a></li>
                <li><a href="#">Notification Preferences</a></li>
                <li><a href="#">Address</a></li>
                <li><a href="#">Billing</a></li>
                <li><a href="#">Deposit</a></li>
                <li><a href="#">Invoice</a></li>
                <li><a href="#">Bids</a></li>
                <li><a href="#">Change Password</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-8">
            <div class="page-details-block">
              <h3 class="block-title">
                Contact info
              </h3>
              <div class="page-inner-block">
                <h4 class="inner-block-title">Personal Info <a href="#" class="editinfo">Edit</a></h4>
                <ul>
                  <li>First Name: <span>John</span></li>
                  <li>Last Name: <span>Doe</span></li>
                  <li>Email: <span>johndoe@gmail.com</span></li>
                  <li>Traffic File code: <span>Not Specified</span></li>
                </ul>
              </div>
              <div class="page-inner-block">
                <h4 class="inner-block-title">Location <a href="#" class="editinfo">Edit</a></h4>
                <ul>
                  <li>First Name: <span>John</span></li>
                  <li>Last Name: <span>Doe</span></li>
                  <li>Email: <span>johndoe@gmail.com</span></li>
                  <li>Traffic File code: <span>Not Specified</span></li>
                </ul>
              </div>
              <div class="page-inner-block">
                <h4 class="inner-block-title">Documents <a href="#" class="editinfo">Edit</a></h4>
                <ul>
                  <li>First Name: <span>John</span></li>
                  <li>Last Name: <span>Doe</span></li>
                  <li>Email: <span>johndoe@gmail.com</span></li>
                  <li>Traffic File code: <span>Not Specified</span></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
      

 <?php include('footer.php'); ?>