<?php

return [
    'reset_password'  => 'لقد تم إعادة تعين كلمة المرور.',
    'activate_user'   => 'لقد تم تنشيط حسابك.',
    'user_registered' => 'لقد تم تسجيل حسابك.',
    'logged_in' => 'تم تسجيل دخولك بنجاح.',
];
