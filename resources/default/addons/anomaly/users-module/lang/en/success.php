<?php

return [
    'reset_password'  => 'Your password has been reset. Please login now.',
    'activate_user'   => 'Your account has been activated.',
    'user_registered' => 'Your account has been registered.',
    'logged_in' => 'You are logged in successfully.',
];
