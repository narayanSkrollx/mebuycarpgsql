<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Locale Hint
    |--------------------------------------------------------------------------
    |
    | Define where to look for an i18n locale.
    |
    | true, false, "domain" or "uri"
    |
    | If false, you must handle setting the locale yourself.
    | If true, both "domain" and "uri" are enabled and will be detected.
    | If "domain", streams will check your sub-domain for an i18n locale key
    | If "uri", streams will check your first URI segment for an i18n locale key
    |
    */

    'hint' => true,

    /*
    |--------------------------------------------------------------------------
    | Enabled Locales
    |--------------------------------------------------------------------------
    |
    | Define an array of locales enabled for translatable input.
    |
    */

    'enabled' => explode(',', env('ENABLED_LOCALES', 'en,ar')),

    /*
    |--------------------------------------------------------------------------
    | Default
    |--------------------------------------------------------------------------
    |
    | The default locale for CONTENT.
    |
    */

    'default' => env('DEFAULT_LOCALE', env('LOCALE', 'en')),

    /*
    |--------------------------------------------------------------------------
    | Supported Locales
    |--------------------------------------------------------------------------
    |
    | In order to enable a locale or translate anything
    | the i18n locale key MUST be in this array.
    |
    */

    'supported' => [
        'en'    => [
            'direction' => 'ltr'
        ],
        'ar'    => [
            'direction' => 'rtl'
        ]
    ]
];
